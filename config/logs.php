<?php

return [
    'api' => [
        'base' => 'https://logs.tf',
        'json' => '/json/',
        // zip logs/log_" + logID + ".log.zip
    ]
];