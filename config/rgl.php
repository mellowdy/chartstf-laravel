<?php

return [
    // config for bad league scraping
    'base_url'    => 'https://rgl.gg',
    // ?s=1&r=24
    'season_page' => '/Public/LeagueTable.aspx',
    // ?m=1003&r=24
    'match_page'  => '/Public/Match.aspx',
    // ?t=2682&r=24
    'team_page'   => '/Public/Team.aspx',
    // ?p=76561198040782513&r=24
    'player_page' => '/Public/PlayerProfile.aspx',
    'players'     => [
        // these steam id's will be ignored cause they aren't people who played (most) of the matches
        'ignored' => [
            76561198069527288,
            76561198057522072,
            76561198053047207,
            76561198251245566,
            76561198061560390,
            76561198053763983,
            76561198071737757
        ]
    ]
];