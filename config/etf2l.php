<?php

return [
    'api' => [
        'base'   => 'https://api.etf2l.org',
        'player' => '/player/',
        'team'   => '/team/'
    ],
    'web' => [
        'base'    => 'https://etf2l.org',
        'matches' => '/matches/',
        'archive' => '/etf2l/archives',
        'awards'  => '/etf2l/archives/'
    ]
];