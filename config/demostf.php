<?php

return [
    'api' => [
        'base'  => 'https://api.demos.tf',
        'demos' => '/demos/'
    ]
];