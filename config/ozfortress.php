<?php

return [
    'web' => [
        'base'    => 'https://ozfortress.com',
        'player'  => '/users/steam_id/',
        'leagues' => '/leagues/',
        'matches' => '/matches'
    ]
];