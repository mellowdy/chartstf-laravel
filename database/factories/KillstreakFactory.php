<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Killstreak;
use App\Models\Logs,id;
use App\Models\Users,id;

class KillstreakFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Killstreak::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'name' => $this->faker->name,
            'steam_id' => Users,id::factory(),
            'streak' => $this->faker->numberBetween(-10000, 10000),
            'tick' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
