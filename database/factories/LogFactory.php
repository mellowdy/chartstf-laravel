<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Log;
use App\Models\Users,id;

class LogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Log::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->dateTime(),
            'title' => $this->faker->sentence(4),
            'map' => $this->faker->word,
            'duration' => $this->faker->numberBetween(-10000, 10000),
            'started_at' => $this->faker->dateTime(),
            'stopped_at' => $this->faker->dateTime(),
            'supplemental' => $this->faker->boolean,
            'has_real_damage' => $this->faker->boolean,
            'has_weapon_damage' => $this->faker->boolean,
            'has_accuracy' => $this->faker->boolean,
            'has_medkit_pickups' => $this->faker->boolean,
            'has_medkit_health' => $this->faker->boolean,
            'has_headshot_kills' => $this->faker->boolean,
            'has_headshot_hits' => $this->faker->boolean,
            'has_backstabs' => $this->faker->boolean,
            'has_point_captures' => $this->faker->boolean,
            'has_sentries_built' => $this->faker->boolean,
            'has_damage_taken' => $this->faker->boolean,
            'has_airshots' => $this->faker->boolean,
            'has_heals_received' => $this->faker->boolean,
            'has_intel_captures' => $this->faker->boolean,
            'scoring_attack_defense' => $this->faker->boolean,
            'uploader_steam_id' => Users,id::factory(),
            'uploader_name' => $this->faker->word,
            'uploader_info' => $this->faker->word,
            'red_score' => $this->faker->numberBetween(-10000, 10000),
            'red_kills' => $this->faker->numberBetween(-10000, 10000),
            'red_deaths' => $this->faker->numberBetween(-10000, 10000),
            'red_damage' => $this->faker->numberBetween(-10000, 10000),
            'red_first_caps' => $this->faker->numberBetween(-10000, 10000),
            'red_caps' => $this->faker->numberBetween(-10000, 10000),
            'red_charges' => $this->faker->numberBetween(-10000, 10000),
            'red_drops' => $this->faker->numberBetween(-10000, 10000),
            'red_team_name' => $this->faker->word,
            'red_team_name_league' => $this->faker->word,
            'blu_score' => $this->faker->numberBetween(-10000, 10000),
            'blu_kills' => $this->faker->numberBetween(-10000, 10000),
            'blu_deaths' => $this->faker->numberBetween(-10000, 10000),
            'blu_damage' => $this->faker->numberBetween(-10000, 10000),
            'blu_first_caps' => $this->faker->numberBetween(-10000, 10000),
            'blu_caps' => $this->faker->numberBetween(-10000, 10000),
            'blu_charges' => $this->faker->numberBetween(-10000, 10000),
            'blu_drops' => $this->faker->numberBetween(-10000, 10000),
            'blu_team_name' => $this->faker->word,
            'blu_team_name_league' => $this->faker->word,
        ];
    }
}
