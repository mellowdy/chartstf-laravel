<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Logs,id;
use App\Models\PlayerStatline;
use App\Models\Users,id;

class PlayerStatlineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PlayerStatline::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'steam_id' => Users,id::factory(),
            'name' => $this->faker->name,
            'team' => $this->faker->word,
            'class_played' => $this->faker->word,
            'team_league' => $this->faker->word,
            'kills' => $this->faker->numberBetween(-10000, 10000),
            'kills_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'assists' => $this->faker->numberBetween(-10000, 10000),
            'assists_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths' => $this->faker->numberBetween(-10000, 10000),
            'deaths_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'suicides' => $this->faker->numberBetween(-10000, 10000),
            'suicides_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'damage' => $this->faker->numberBetween(-10000, 10000),
            'damage_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'damage_real' => $this->faker->numberBetween(-10000, 10000),
            'damage_real_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'damage_taken' => $this->faker->numberBetween(-10000, 10000),
            'damage_taken_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'heals_received' => $this->faker->numberBetween(-10000, 10000),
            'heals_received_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'longest_killstreak' => $this->faker->numberBetween(-10000, 10000),
            'airshots' => $this->faker->numberBetween(-10000, 10000),
            'airshots_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'medkit_pickup' => $this->faker->numberBetween(-10000, 10000),
            'medkit_pickup_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'medkit_health' => $this->faker->numberBetween(-10000, 10000),
            'medkit_health_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'backstabs' => $this->faker->numberBetween(-10000, 10000),
            'backstabs_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'headshot_kills' => $this->faker->numberBetween(-10000, 10000),
            'headshot_kills_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'headshots' => $this->faker->numberBetween(-10000, 10000),
            'headshots_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'sentries' => $this->faker->numberBetween(-10000, 10000),
            'sentries_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'dispensers' => $this->faker->numberBetween(-10000, 10000),
            'dispensers_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'teleporters' => $this->faker->numberBetween(-10000, 10000),
            'teleporters_per_minute' => $this->faker->numberBetween(-10000, 10000),
            'extinguishes' => $this->faker->numberBetween(-10000, 10000),
            'extinguishes_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'feigns' => $this->faker->numberBetween(-10000, 10000),
            'feigns_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'point_captures' => $this->faker->numberBetween(-10000, 10000),
            'point_captures_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'intel_captures' => $this->faker->numberBetween(-10000, 10000),
            'intel_captures_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'charges' => $this->faker->numberBetween(-10000, 10000),
            'charges_uber' => $this->faker->numberBetween(-10000, 10000),
            'charges_kritzkrieg' => $this->faker->numberBetween(-10000, 10000),
            'charges_vaccinator' => $this->faker->numberBetween(-10000, 10000),
            'charges_quickfix' => $this->faker->numberBetween(-10000, 10000),
            'charges_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'charges_uber_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'charges_kritzkrieg_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'charges_vaccinator_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'charges_quickfix_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'drops' => $this->faker->numberBetween(-10000, 10000),
            'drops_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'advantages_lost' => $this->faker->numberBetween(-10000, 10000),
            'advantages_lost_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'biggest_advantage_lost' => $this->faker->numberBetween(-10000, 10000),
            'biggest_advantage_lost_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_with_95_uber' => $this->faker->numberBetween(-10000, 10000),
            'deaths_within_20s_after_uber' => $this->faker->numberBetween(-10000, 10000),
            'average_time_before_healing' => $this->faker->randomFloat(0, 0, 9999999999.),
            'average_time_to_build' => $this->faker->randomFloat(0, 0, 9999999999.),
            'average_time_before_using' => $this->faker->randomFloat(0, 0, 9999999999.),
            'average_charge_length' => $this->faker->randomFloat(0, 0, 9999999999.),
            'time_as_scout' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_scout' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_scout' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_scout' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_scout' => $this->faker->numberBetween(-10000, 10000),
            'time_as_soldier' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_soldier' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_soldier' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_soldier' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_soldier' => $this->faker->numberBetween(-10000, 10000),
            'time_as_pyro' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_pyro' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_pyro' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_pyro' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_pyro' => $this->faker->numberBetween(-10000, 10000),
            'time_as_demoman' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_demoman' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_demoman' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_demoman' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_demoman' => $this->faker->numberBetween(-10000, 10000),
            'time_as_heavyweapons' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_heavyweapons' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_heavyweapons' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_heavyweapons' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_heavyweapons' => $this->faker->numberBetween(-10000, 10000),
            'time_as_engineer' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_engineer' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_engineer' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_engineer' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_engineer' => $this->faker->numberBetween(-10000, 10000),
            'time_as_medic' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_medic' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_medic' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_medic' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_medic' => $this->faker->numberBetween(-10000, 10000),
            'time_as_sniper' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_sniper' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_sniper' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_sniper' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_sniper' => $this->faker->numberBetween(-10000, 10000),
            'time_as_spy' => $this->faker->numberBetween(-10000, 10000),
            'kills_as_spy' => $this->faker->numberBetween(-10000, 10000),
            'assists_as_spy' => $this->faker->numberBetween(-10000, 10000),
            'deaths_as_spy' => $this->faker->numberBetween(-10000, 10000),
            'damage_as_spy' => $this->faker->numberBetween(-10000, 10000),
            'scouts_killed' => $this->faker->numberBetween(-10000, 10000),
            'scouts_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'scouts_assisted' => $this->faker->numberBetween(-10000, 10000),
            'scouts_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_scouts' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_scouts_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'soldiers_killed' => $this->faker->numberBetween(-10000, 10000),
            'soldiers_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'soldiers_assisted' => $this->faker->numberBetween(-10000, 10000),
            'soldiers_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_soldiers' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_soldiers_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'pyros_killed' => $this->faker->numberBetween(-10000, 10000),
            'pyros_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'pyros_assisted' => $this->faker->numberBetween(-10000, 10000),
            'pyros_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_pyros' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_pyros_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'demomen_killed' => $this->faker->numberBetween(-10000, 10000),
            'demomen_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'demomen_assisted' => $this->faker->numberBetween(-10000, 10000),
            'demomen_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_demomen' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_demomen_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'heavies_killed' => $this->faker->numberBetween(-10000, 10000),
            'heavies_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'heavies_assisted' => $this->faker->numberBetween(-10000, 10000),
            'heavies_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_heavies' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_heavies_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'engineers_killed' => $this->faker->numberBetween(-10000, 10000),
            'engineers_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'engineers_assisted' => $this->faker->numberBetween(-10000, 10000),
            'engineers_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_engineers' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_engineers_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'medics_killed' => $this->faker->numberBetween(-10000, 10000),
            'medics_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'medics_assisted' => $this->faker->numberBetween(-10000, 10000),
            'medics_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_medics' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_medics_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'snipers_killed' => $this->faker->numberBetween(-10000, 10000),
            'snipers_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'snipers_assisted' => $this->faker->numberBetween(-10000, 10000),
            'snipers_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_snipers' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_snipers_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'spies_killed' => $this->faker->numberBetween(-10000, 10000),
            'spies_killed_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'spies_assisted' => $this->faker->numberBetween(-10000, 10000),
            'spies_assisted_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'deaths_to_spies' => $this->faker->numberBetween(-10000, 10000),
            'deaths_to_spies_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
        ];
    }
}
