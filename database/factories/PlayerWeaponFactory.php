<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Logs,id;
use App\Models\PlayerWeapon;
use App\Models\Users,id;

class PlayerWeaponFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PlayerWeapon::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'steam_id' => Users,id::factory(),
            'tf_class' => $this->faker->word,
            'kills' => $this->faker->numberBetween(-10000, 10000),
            'damage' => $this->faker->numberBetween(-10000, 10000),
            'damage_per_minute' => $this->faker->randomFloat(0, 0, 9999999999.),
            'shots' => $this->faker->numberBetween(-10000, 10000),
            'hits' => $this->faker->numberBetween(-10000, 10000),
            'accuracy' => $this->faker->randomFloat(0, 0, 9999999999.),
        ];
    }
}
