<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Logs,id;
use App\Models\RoundPlayer;
use App\Models\Rounds,id;
use App\Models\Users,id;

class RoundPlayerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RoundPlayer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'round_id' => Rounds,id::factory(),
            'steam_id' => Users,id::factory(),
            'kills' => $this->faker->numberBetween(-10000, 10000),
            'damage' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
