<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Logs,id;
use App\Models\RoundEvent;
use App\Models\Rounds,id;
use App\Models\Users,id;

class RoundEventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RoundEvent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'round_id' => Rounds,id::factory(),
            'steam_id' => Users,id::factory(),
            'killer_steam_id' => Users,id::factory(),
            'tick' => $this->faker->numberBetween(-10000, 10000),
            'type' => $this->faker->word,
            'team' => $this->faker->word,
            'point' => $this->faker->numberBetween(-10000, 10000),
            'medigun' => $this->faker->word,
        ];
    }
}
