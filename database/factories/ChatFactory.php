<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Chat;
use App\Models\Logs,id;
use App\Models\Users,id;

class ChatFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Chat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'name' => $this->faker->name,
            'steam_id' => Users,id::factory(),
            'message' => $this->faker->text,
        ];
    }
}
