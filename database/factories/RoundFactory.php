<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Logs,id;
use App\Models\Round;

class RoundFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Round::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'log_id' => Logs,id::factory(),
            'started_at' => $this->faker->dateTime(),
            'duration' => $this->faker->numberBetween(-10000, 10000),
            'first_cap' => $this->faker->word,
            'winner' => $this->faker->word,
            'red_score' => $this->faker->numberBetween(-10000, 10000),
            'red_kills' => $this->faker->numberBetween(-10000, 10000),
            'red_damage' => $this->faker->numberBetween(-10000, 10000),
            'red_charges' => $this->faker->numberBetween(-10000, 10000),
            'blu_score' => $this->faker->numberBetween(-10000, 10000),
            'blu_kills' => $this->faker->numberBetween(-10000, 10000),
            'blu_damage' => $this->faker->numberBetween(-10000, 10000),
            'blu_charges' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
