<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rounds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs');
            $table->dateTime('started_at')->nullable();
            $table->dateTime('stopped_at')->nullable();
            $table->integer('duration');
            $table->string('first_cap')->nullable();
            $table->string('winner')->nullable()->comment('Winner of the round (can be empty if no one got a round for example)');
            $table->integer('red_score')->default(0);
            $table->integer('red_kills')->default(0);
            $table->integer('red_damage')->default(0);
            $table->integer('red_charges')->default(0);
            $table->integer('blu_score')->default(0);
            $table->integer('blu_kills')->default(0);
            $table->integer('blu_damage')->default(0);
            $table->integer('blu_charges')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rounds', function(Blueprint $table) {
            $table->dropForeign(['log_id']);
        });

        Schema::dropIfExists('rounds');
    }
}
