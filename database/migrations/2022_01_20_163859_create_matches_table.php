<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index()->comment('Current team name');
            $table->json('past_names')->nullable()->comment('Previous names the team played as');
            $table->string('team_picture', 600)->nullable()->comment('Picture to use?');
            $table->string('league_team_id')->nullable()->comment('ID of the team in that league?');
            $table->string('team_url')->nullable()->comment('Link to team page (league)');
            $table->string('league')->nullable()->comment('Which league?');
            $table->string('gamemode')->nullable()->comment('Which gamemode do they play?');
            $table->string('tag')->index()->nullable();
            $table->string('country')->nullable();
            $table->unique(['league_team_id', 'league']);
            $table->timestamps();
        });

        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('home_team_name')->index();
            $table->unsignedBigInteger('home_team_id')->nullable();
            $table->foreign('home_team_id')->references('id')->on('teams');
            $table->float('home_team_score')->nullable();
            $table->string('away_team_name')->index();
            $table->unsignedBigInteger('away_team_id')->nullable();
            $table->foreign('away_team_id')->references('id')->on('teams');
            $table->float('away_team_score')->nullable();
            $table->string('league_match_id')->index();
            $table->string('league_match_url')->nullable();
            $table->json('maps_played')->nullable();
            $table->string('division')->nullable();
            $table->string('week')->nullable();
            $table->dateTimeTz('scheduled_date');
            $table->dateTimeTz('results_submitted_date')->nullable();
            $table->unsignedBigInteger('collection_id');
            $table->foreign('collection_id')->references('id')->on('collections');
            $table->timestamps();
        });

        Schema::create('competition_participations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('steam_id');
            $table->foreign('steam_id')->references('id')->on('users');
            $table->unsignedBigInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->boolean('unrostered')->default(0);
            $table->unsignedBigInteger('match_id');
            $table->foreign('match_id')->references('id')->on('matches');
            $table->unsignedBigInteger('collection_id');
            $table->foreign('collection_id')->references('id')->on('collections');
            $table->unique(['match_id', 'steam_id', 'team_id', 'collection_id'], 'unique_match_part');
            $table->timestamps();
        });

        Schema::create('log_match', function (Blueprint $table) {
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs');
            $table->unsignedBigInteger('match_id');
            $table->foreign('match_id')->references('id')->on('matches');
            $table->unique(['log_id', 'match_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_match', function (Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['match_id']);
        });

        Schema::table('competition_participations', function (Blueprint $table) {
            $table->dropForeign(['collection_id']);
            $table->dropForeign(['match_id']);
            $table->dropForeign(['steam_id']);
            $table->dropForeign(['team_id']);
        });

        Schema::table('matches', function (Blueprint $table) {
            $table->dropForeign(['collection_id']);
            $table->dropForeign(['home_team_id']);
            $table->dropForeign(['away_team_id']);
        });

        Schema::dropIfExists('log_match');
        Schema::dropIfExists('competition_participations');
        Schema::dropIfExists('matches');
        Schema::dropIfExists('teams');
    }
}
