<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->string('name')
                  ->unique();
            $table->string('description', 400)
                  ->nullable();
            $table->string('is_private')
                  ->default(0);
            $table->unsignedBigInteger('steam_id');
            $table->foreign('steam_id')
                  ->references('id')
                  ->on('users');
            $table->string('slug')
                  ->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropForeign(['steam_id']);
        });
        Schema::dropIfExists('collections');
    }
}
