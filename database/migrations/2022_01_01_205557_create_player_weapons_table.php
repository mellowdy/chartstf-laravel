<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerWeaponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_weapons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')
                  ->references('id')
                  ->on('logs');
            $table->unsignedBigInteger('steam_id');
            $table->foreign('steam_id')
                  ->references('id')
                  ->on('users');
            $table->unsignedBigInteger('player_statline_id')
                  ->nullable();
            $table->foreign('player_statline_id')
                  ->references('id')
                  ->on('player_statlines')
                  ->nullOnDelete();
            $table->string('name')->index();
            $table->string('tf_class')->index();
            $table->integer('kills');
            $table->integer('damage')
                  ->nullable();
            $table->float('damage_per_minute')
                  ->nullable();
            $table->integer('shots')
                  ->nullable();
            $table->integer('hits')
                  ->nullable();
            $table->float('accuracy')
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_weapons', function (Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['steam_id']);
            $table->dropForeign(['player_statline_id']);
        });

        Schema::dropIfExists('player_weapons');
    }
}
