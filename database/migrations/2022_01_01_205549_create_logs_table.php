<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date')->index();
            $table->string('title')->index();
            $table->string('map')->index();
            $table->integer('duration')
                  ->nullable()
                  ->comment('Time in seconds');
            $table->integer('duration_real')
                  ->nullable()
                  ->comment('Corrected time in seconds (round time) to adjust for pauses');
            $table->dateTime('started_at')
                  ->nullable()
                  ->comment('Match started when?');
            $table->dateTime('stopped_at')
                  ->nullable()
                  ->comment('Match ended when?');
            $table->boolean('supplemental')->default(0);
            $table->boolean('has_real_damage')->default(0);
            $table->boolean('has_weapon_damage')->default(0);
            $table->boolean('has_accuracy')->default(0);
            $table->boolean('has_medkit_pickups')->default(0);
            $table->boolean('has_medkit_health')->default(0);
            $table->boolean('has_headshot_kills')->default(0);
            $table->boolean('has_headshot_hits')->default(0);
            $table->boolean('has_backstabs')->default(0);
            $table->boolean('has_point_captures')->default(0);
            $table->boolean('has_sentries_built')->default(0);
            $table->boolean('has_damage_taken')->default(0);
            $table->boolean('has_airshots')->default(0);
            $table->boolean('has_heals_received')->default(0);
            $table->boolean('has_intel_captures')->default(0);
            $table->boolean('has_zip_data')->default(0);
            $table->boolean('scoring_attack_defense')->default(0);
            $table->unsignedBigInteger('uploader_steam_id');
            $table->foreign('uploader_steam_id')->references('id')->on('users');
            $table->string('uploader_name')->index();
            $table->string('uploader_info')->nullable();
            $table->integer('red_score')->nullable();
            $table->integer('red_kills')->nullable();
            $table->integer('red_deaths')->nullable();
            $table->integer('red_damage')->nullable();
            $table->integer('red_first_caps')->nullable();
            $table->integer('red_caps')->nullable();
            $table->integer('red_charges')->nullable();
            $table->integer('red_drops')->nullable();
            $table->string('red_team_name')
                  ->index()
                  ->nullable();
            $table->string('red_team_name_league')
                  ->index()
                  ->nullable();
            $table->integer('blu_score')->nullable();
            $table->integer('blu_kills')->nullable();
            $table->integer('blu_deaths')->nullable();
            $table->integer('blu_damage')->nullable();
            $table->integer('blu_first_caps')->nullable();
            $table->integer('blu_caps')->nullable();
            $table->integer('blu_charges')->nullable();
            $table->integer('blu_drops')->nullable();
            $table->string('blu_team_name')
                  ->index()
                  ->nullable();
            $table->string('blu_team_name_league')
                  ->index()
                  ->nullable();
            $table->string('pug_service')
                  ->index()
                  ->nullable();
            $table->string('server_provider')
                  ->index()
                  ->nullable();
            $table->string('league')
                  ->index()
                  ->nullable();
            $table->string('match_url')
                  ->nullable();
            $table->string('stream')
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
