<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamemodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamemodes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->timestamps();
        });

        \App\Models\Gamemode::create(['name' => 'Attack/Defend']);
        \App\Models\Gamemode::create(['name' => 'Capture The Flag']);
        \App\Models\Gamemode::create(['name' => 'Pass Time']);
        \App\Models\Gamemode::create(['name' => 'Territorial Control']);
        \App\Models\Gamemode::create(['name' => 'Player Destruction']);
        \App\Models\Gamemode::create(['name' => 'Payload Race']);
        \App\Models\Gamemode::create(['name' => 'Payload']);
        \App\Models\Gamemode::create(['name' => 'King Of The Hill']);
        \App\Models\Gamemode::create(['name' => 'Control Points']);

        Schema::table('logs', function (Blueprint $table) {
            $table->unsignedBigInteger('gamemode_id')
                  ->nullable();
            $table->foreign('gamemode_id')
                  ->references('id')
                  ->on('gamemodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->dropForeign(['gamemode_id']);
            $table->dropColumn(['gamemode_id']);
        });
        Schema::dropIfExists('gamemodes');
    }
}
