<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoundPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('round_players', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs');
            $table->unsignedBigInteger('round_id');
            $table->foreign('round_id')->references('id')->on('rounds');
            $table->unsignedBigInteger('steam_id')->nullable();
            $table->foreign('steam_id')->references('id')->on('users');
            $table->integer('kills');
            $table->integer('damage');
            $table->string('team')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('round_players', function(Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['round_id']);
            $table->dropForeign(['steam_id']);
        });
        Schema::dropIfExists('round_players');
    }
}
