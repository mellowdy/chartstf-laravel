<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demos', function (Blueprint $table) {
            $table->id();
            $table->string('download_url', 600)->nullable();
            $table->string('server', 200)->nullable();
            $table->integer('duration')->nullable();
            $table->string('demo_provider')
                  ->default('demos.tf')
                  ->nullable();
            $table->string('demo_type')->index()->default('SourceTV Demo');
            $table->string('map')->index()->nullable();
            $table->dateTime('date')->index()->nullable();
            $table->string('red_name')->nullable();
            $table->string('blu_name')->nullable();
            $table->integer('player_count')->nullable();
            $table->unsignedBigInteger('demostf_id')->index()->nullable();
            $table->timestamps();
        });

        Schema::create('demo_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('demo_id');
            $table->foreign('demo_id')->references('id')->on('demos');
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('demo_log', function (Blueprint $table) {
            $table->dropForeign(['demo_id']);
            $table->dropForeign(['log_id']);
        });
        Schema::dropIfExists('demo_log');
        Schema::dropIfExists('demos');
    }
}
