<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEtf2lIntegrationToChartstf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('etf2l_id')
                  ->index()
                  ->comment('ID of the user on ETF2L')
                  ->nullable();
        });

        Schema::table('logs', function (Blueprint $table) {
            $table->string('match_page')
                  ->nullable();
            $table->unsignedBigInteger('etf2l_match_id')
                  ->nullable()
                  ->comment('Number of the ETF2L match')
                  ->index();
        });

        Schema::table('player_statlines', function (Blueprint $table) {
            $table->unsignedBigInteger('etf2l_player_id')
                  ->index()
                  ->nullable();
            $table->string('played_for')
                  ->nullable()
                  ->index();
            $table->unsignedBigInteger('etf2l_team_id')
                  ->index()
                  ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['etf2l_id']);
        });

        Schema::table('logs', function (Blueprint $table) {
            $table->dropColumn(['match_page', 'etf2l_match_id']);
        });

        Schema::table('player_statlines', function (Blueprint $table) {
            $table->dropColumn(['played_for', 'etf2l_player_id', 'etf2l_team_id']);
        });
    }
}
