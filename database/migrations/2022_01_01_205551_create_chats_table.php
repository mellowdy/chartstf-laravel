<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs');
            $table->string('name');
            $table->unsignedBigInteger('steam_id')->nullable();
            $table->foreign('steam_id')->references('id')->on('users');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chats', function(Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['steam_id']);
        });

        Schema::dropIfExists('chats');
    }
}
