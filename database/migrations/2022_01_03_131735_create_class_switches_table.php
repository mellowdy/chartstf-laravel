<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassSwitchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_switches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs')->cascadeOnDelete();
            $table->unsignedBigInteger('steam_id');
            $table->foreign('steam_id')->references('id')->on('users');
            $table->integer('time');
            $table->dateTime('when')->nullable();
            $table->string('name');
            $table->string('team');
            $table->string('tf_class');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class_switches', function (Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['steam_id']);
        });

        Schema::dropIfExists('class_switches');
    }
}
