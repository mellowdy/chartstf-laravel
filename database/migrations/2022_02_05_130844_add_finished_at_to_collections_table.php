<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinishedAtToCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dateTime('finished_at')
                  ->index()
                  ->nullable();
        });

        $collections = \App\Models\Collection::with(['logs' => function ($q) {
            $q->select('id', 'stopped_at')->orderByDesc('stopped_at');
        }])->get();

        foreach ($collections as $collection) {
            $firstLog = $collection->logs->first();
            if ($firstLog instanceof \App\Models\Log) {
                $collection->update(['finished_at' => $firstLog->stopped_at->endOfDay()]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropColumn('finished_at');
        });
    }
}
