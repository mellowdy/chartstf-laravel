<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')
                  ->primary();
            $table->string('name')
                  ->index();
            $table->string('alias')
                  ->index()
                  ->nullable()
                  ->comment('Preferred user alias');
            $table->string('steam_profile_picture_small', 400)
                  ->nullable();
            $table->string('steam_profile_picture_medium', 400)
                  ->nullable();
            $table->string('steam_profile_picture_big', 400)
                  ->nullable();
            $table->string('steamid2')->index()->nullable();
            $table->string('steamid3')->index()->nullable();
            $table->string('steamid64')->index()->nullable();
            $table->boolean('banned')
                  ->default(0);
            $table->string('email')
                  ->nullable()
                  ->unique();
            $table->timestamp('email_verified_at')
                  ->nullable();
            $table->string('password')
                  ->nullable();
            $table->rememberToken();
            $table->foreignId('current_team_id')
                  ->nullable();
            $table->string('profile_photo_path', 2048)
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
