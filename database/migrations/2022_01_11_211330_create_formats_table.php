<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formats', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->timestamps();
        });

        \App\Models\Format::create(['name' => 'Highlander']);
        \App\Models\Format::create(['name' => '6v6']);
        \App\Models\Format::create(['name' => '4v4']);
        \App\Models\Format::create(['name' => 'Ultiduo']);
        \App\Models\Format::create(['name' => 'Prolander']);
        \App\Models\Format::create(['name' => 'MGE']);
        \App\Models\Format::create(['name' => 'Custom']);

        Schema::table('logs', function (Blueprint $table) {
            $table->unsignedBigInteger('format_id')
                  ->nullable();
            $table->foreign('format_id')
                  ->references('id')
                  ->on('formats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->dropForeign(['format_id']);
            $table->dropColumn(['format_id']);
        });

        Schema::dropIfExists('formats');
    }
}
