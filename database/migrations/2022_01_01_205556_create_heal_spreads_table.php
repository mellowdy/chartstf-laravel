<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealSpreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heal_spreads', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')->references('id')->on('logs');
            $table->unsignedBigInteger('healer_steam_id');
            $table->foreign('healer_steam_id')->references('id')->on('users');
            $table->unsignedBigInteger('target_steam_id');
            $table->foreign('target_steam_id')->references('id')->on('users');
            $table->float('heal_amount')->default(0);
            $table->string('tf_class')->nullable()->default('Medic');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('heal_spreads', function(Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['healer_steam_id']);
            $table->dropForeign(['target_steam_id']);
        });
        Schema::dropIfExists('heal_spreads');
    }
}
