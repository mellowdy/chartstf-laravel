<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpawnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spawns', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')
                  ->references('id')
                  ->on('logs')
                  ->cascadeOnDelete();
            $table->unsignedBigInteger('steam_id')->nullable();
            $table->foreign('steam_id')
                  ->references('id')
                  ->on('users')
                  ->nullOnDelete();
            $table->string('name');
            $table->dateTime('spawned_at')->nullable();
            $table->integer('time')
                  ->comment('How many seconds since start of the game');
            $table->string('tf_class')->nullable();
            $table->string('team');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spawns', function (Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['steam_id']);
        });
        Schema::dropIfExists('spawns');
    }
}
