<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vods', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('league_match_id');
            $table->foreign('league_match_id')->references('id')->on('matches');
            $table->string('title')->nullable();
            $table->string('season')->nullable();
            $table->string('url', 500);
            $table->string('provider')->nullable();
            $table->string('match_page')->nullable();
            $table->string('channel')->nullable();
            $table->string('channel_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vods', function(Blueprint $table) {
            $table->dropForeign(['league_match_id']);
        });

        Schema::dropIfExists('vods');
    }
}
