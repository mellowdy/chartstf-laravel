<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenges', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->foreign('log_id')
                  ->references('id')
                  ->on('logs')
                  ->cascadeOnDelete();
            $table->unsignedBigInteger('steam_id');
            $table->foreign('steam_id')
                  ->references('id')
                  ->on('users');
            $table->string('team');
            $table->string('name');
            $table->unsignedBigInteger('target_steam_id');
            $table->foreign('target_steam_id')
                  ->references('id')
                  ->on('users');
            $table->integer('time')
                  ->nullable();
            $table->string('target_name');
            $table->string('target_team');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revenges', function (Blueprint $table) {
            $table->dropForeign(['log_id']);
            $table->dropForeign(['steam_id']);
            $table->dropForeign(['target_steam_id']);
        });

        Schema::dropIfExists('revenges');
    }
}
