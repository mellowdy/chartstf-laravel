<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @param UrlGenerator $url
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (env('APP_ENV') !== 'local' && !Str::contains(env('APP_ENV'), 'dev') && !Str::contains(env('APP_ENV'), 'arne')) {
            $url->forceScheme('https');

            \Illuminate\Pagination\AbstractPaginator::currentPathResolver(function () {
                /** @var UrlGenerator $url */
                $url = app('url');
                return $url->current();
            });
        }
    }
}
