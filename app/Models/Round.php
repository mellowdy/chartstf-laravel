<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $log_id
 * @property Carbon $started_at
 * @property int $duration
 * @property string $first_cap
 * @property string $winner
 * @property int $red_score
 * @property int $red_kills
 * @property int $red_damage
 * @property int $red_charges
 * @property int $blu_score
 * @property int $blu_kills
 * @property int $blu_damage
 * @property int $blu_charges
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Round extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'       => 'integer',
        'log_id'   => 'integer',
        'steam_id' => 'string'
    ];

    protected $dates = ['started_at', 'stopped_at'];

    /**
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    public function players(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\RoundPlayer::class);
    }

    public function events(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\RoundEvent::class);
    }
}
