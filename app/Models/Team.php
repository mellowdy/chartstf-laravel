<?php

namespace App\Models;

use App\Models\Filters\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory, Filterable;

    protected $guarded = ['id'];

    public function competitionParticipations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\CompetitionParticipation::class);
    }

    public function players(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough(\App\Models\User::class, \App\Models\CompetitionParticipation::class, 'team_id', 'id', 'id', 'steam_id');
    }
}
