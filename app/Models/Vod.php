<?php

namespace App\Models;

use App\Models\Filters\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vod extends Model
{
    use HasFactory, Filterable;

    protected $guarded = ['id'];
}
