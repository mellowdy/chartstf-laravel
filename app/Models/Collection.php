<?php

namespace App\Models;

use App\Models\Filters\Traits\Filterable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $is_private
 * @property int $steam_id
 * @property string $slug
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Collection extends Model
{
    use HasFactory, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'steam_id'   => 'string',
        'is_private' => 'boolean',
        'featured'   => 'boolean'
    ];

    protected $dates = [
        'finished_at'
    ];

    /**
     * @return BelongsTo
     */
    public function steam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function logs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Log::class)->withTimestamps();
    }

    public function competitionParticipations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\CompetitionParticipation::class);
    }
}
