<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $log_id
 * @property int $round_id
 * @property int $steam_id
 * @property int $killer_steam_id
 * @property int $tick
 * @property string $type
 * @property string $team
 * @property int $point
 * @property string $medigun
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class RoundEvent extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'              => 'integer',
        'log_id'          => 'integer',
        'round_id'        => 'integer',
        'steam_id'        => 'string',
        'killer_steam_id' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    /**
     * @return BelongsTo
     */
    public function round(): BelongsTo
    {
        return $this->belongsTo(Round::class);
    }

    /**
     * @return BelongsTo
     */
    public function steam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function killerSteam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
