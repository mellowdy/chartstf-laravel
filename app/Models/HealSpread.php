<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $log_id
 * @property int $healer_steam_id
 * @property int $target_steam_id
 * @property float $heal_amount
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class HealSpread extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'              => 'integer',
        'log_id'          => 'integer',
        'healer_steam_id' => 'string',
        'target_steam_id' => 'string',
        'heal_amount'     => 'float',
    ];

    /**
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    /**
     * @return BelongsTo
     */
    public function healerSteam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function targetSteam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
