<?php

namespace App\Models\Filters;

use Illuminate\Database\Eloquent\Relations\HasMany;

class CollectionFilter extends QueryFilter
{
    public function name($value): void
    {
        $this->builder->where('name', 'like', '%' . $value . '%');
    }

    public function gamemode($value): void
    {
        $this->builder->where('gamemode', $value);
    }

    public function competition($value): void
    {
        $this->builder->where('league', $value);
    }

    public function league($value): void
    {
        $this->builder->where('league', $value);
    }

    public function format($value): void
    {
        $this->builder->where('gamemode', $value);
    }

    public function division($value): void
    {
        $this->builder->where('division', $value);
    }

    public function from($value): void
    {
        $this->builder->whereHas('logs', fn (\Illuminate\Database\Eloquent\Builder $query) => $query->where('date', '>=', $value));
    }

    public function to($value): void
    {
        $this->builder->whereHas('logs', fn (\Illuminate\Database\Eloquent\Builder $query) => $query->where('date', '<=', $value));
    }
}