<?php

namespace App\Models\Filters;

class UserFilter extends QueryFilter
{
    public function name($value)
    {
        $this->builder->where('name', 'like', '%' . $value . '%');
    }
}