<?php

namespace App\Models\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class QueryFilter
{
    /**
     * The request object.
     *
     * @var Request
     */
    protected Request $request;
    /**
     * The builder instance.
     *
     * @var Builder
     */
    protected Builder $builder;

    /**
     * Create a new QueryFilters instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters to the builder.
     *
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {
            if (!method_exists($this, Str::camel($name)) || $value == "") {
                continue;
            }
            if (is_array($value) || strlen($value)) {
                $this->{Str::camel($name)}($value);
            } else {
                $this->{Str::camel($name)}();
            }
        }

        if (!$this->request->has('order')) {
            $this->defaultOrder();
        }
        return $this->builder;
    }

    /**
     * Get all request filters data.
     *
     * @return array
     */
    public function filters(): array
    {
        return $this->request->all();
    }

    public function defaultOrder(): void
    {
    }
}
