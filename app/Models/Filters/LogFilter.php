<?php

namespace App\Models\Filters;

use App\Models\Weapon;

class LogFilter extends QueryFilter
{
    public function sort($value)
    {
        $column = $value;

        if ($column == 'newest') {
            $this->builder->orderBy('date', 'desc');
        }

        if ($column == 'oldest') {
            $this->builder->orderBy('date');
        }

        if ($column == 'a_z') {
            $this->builder->orderBy('title');
        }

        if ($column == 'z_a') {
            $this->builder->orderBy('title', 'desc');
        }
    }

    public function title($value)
    {
        $this->builder->where('title', 'like', '%' . $value . '%');
    }

    public function map($value)
    {
        $this->builder->where('map', 'like', '%' . $value . '%');
    }

    public function weapon($value)
    {
        $this->builder->whereHas('weapons', function ($q) use ($value) {
            $weapon = Weapon::where('name', $value)->first();

            logger()->info('Weapon', [
                'weapon' => $weapon
            ]);
            if ($weapon instanceof Weapon) {
                $q->whereIn('name', $weapon->tf_names);
            }
        });
    }

    public function chat($value)
    {
        $this->builder->whereHas('chats', function ($q) use ($value) {
            $q->where('message', 'like', '%' . $value . '%');
        });
    }

    public function steamIds($value)
    {
        if (is_array($value)) {
            $this->builder->whereHas('statlines', function ($q) use ($value) {
                $q->whereIn('steam_id', $value);
            });
        }

        if (is_string($value)) {
            $this->builder->whereHas('statlines', function ($q) use ($value) {
                $q->where('steam_id', $value);
            });
        }
    }

    public function format($value)
    {
        switch ($value) {
            case '2v2':
            case 'BBall':
                $this->builder->has('statlines', '=', 4);
                break;
            case 'Prolander':
                $this->builder->has('statlines', '=', 14);
                break;
            case '4v4':
                $this->builder->has('statlines', '=', 8);
                break;
            case '6v6':
                $this->builder->has('statlines', '=', 12);
                break;
            case 'Highlander':
                $this->builder->has('statlines', '=', 18);
                break;
        }
    }

    public function searchQuery($value)
    {
        $this->builder->whereHas('statlines', function ($q) use ($value) {
            $q->where(function ($q) use ($value) {
                $q->where('name', 'like', $value . '%')
                  ->orWhereHas('steam', function ($q) use ($value) {
                      $q->where('name', 'like', $value . '%');
                  });
            });
        });
    }
}