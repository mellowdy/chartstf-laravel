<?php

namespace App\Models\Filters;

class TeamFilter extends QueryFilter
{
    public function id($value)
    {
        $this->builder->where('id', $value);
    }

    public function name($value)
    {
        $this->builder->where('name', 'like', '%' . $value . '%')
                      ->orWhere('tag', 'like', '%' . $value . '%');
    }

    public function league($value)
    {
        $this->builder->where('league', 'like', '%' . $value . '%');
    }

    public function gamemode($value)
    {
        $this->builder->where('gamemode', 'like', '%' . $value . '%');
    }

    public function format($value)
    {
        $this->builder->where('gamemode', 'like', '%' . $value . '%');
    }
}