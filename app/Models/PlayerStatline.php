<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property int $log_id
 * @property string $steam_id
 * @property string $name
 * @property string $team
 * @property string $class_played
 * @property string $team_league
 * @property int $kills
 * @property float $kills_per_minute
 * @property float $kills_per_death
 * @property float $kills_assists_per_death
 * @property float $assists_per_death
 * @property int $assists
 * @property float $assists_per_minute
 * @property int $deaths
 * @property float $deaths_per_minute
 * @property int $suicides
 * @property float $suicides_per_minute
 * @property int $damage
 * @property float $damage_per_minute
 * @property int $damage_real
 * @property float $damage_real_per_minute
 * @property int $damage_taken
 * @property float $damage_taken_per_minute
 * @property int $heals_received
 * @property float $heals_received_per_minute
 * @property int $longest_killstreak
 * @property int $airshots
 * @property float $airshots_per_minute
 * @property int $medkit_pickup
 * @property float $medkit_pickup_per_minute
 * @property int $medkit_health
 * @property float $medkit_health_per_minute
 * @property int $backstabs
 * @property float $backstabs_per_minute
 * @property int $headshot_kills
 * @property float $headshot_kills_per_minute
 * @property int $headshots
 * @property float $headshots_per_minute
 * @property int $sentries
 * @property float $sentries_per_minute
 * @property int $dispensers
 * @property float $dispensers_per_minute
 * @property int $teleporters
 * @property int $teleporters_per_minute
 * @property int $extinguishes
 * @property float $extinguishes_per_minute
 * @property int $feigns
 * @property float $feigns_per_minute
 * @property int $point_captures
 * @property float $point_captures_per_minute
 * @property int $intel_captures
 * @property float $intel_captures_per_minute
 * @property int $charges
 * @property int $charges_uber
 * @property int $charges_kritzkrieg
 * @property int $charges_vaccinator
 * @property int $charges_quickfix
 * @property float $charges_per_minute
 * @property float $charges_uber_per_minute
 * @property float $charges_kritzkrieg_per_minute
 * @property float $charges_vaccinator_per_minute
 * @property float $charges_quickfix_per_minute
 * @property int $drops
 * @property float $drops_per_minute
 * @property int $advantages_lost
 * @property float $advantages_lost_per_minute
 * @property int $biggest_advantage_lost
 * @property float $biggest_advantage_lost_per_minute
 * @property int $deaths_with_95_uber
 * @property int $deaths_within_20s_after_uber
 * @property float $average_time_before_healing
 * @property float $average_time_to_build
 * @property float $average_time_before_using
 * @property float $average_charge_length
 * @property int $time_as_scout
 * @property int $kills_as_scout
 * @property int $assists_as_scout
 * @property int $deaths_as_scout
 * @property int $damage_as_scout
 * @property int $time_as_soldier
 * @property int $kills_as_soldier
 * @property int $assists_as_soldier
 * @property int $deaths_as_soldier
 * @property int $damage_as_soldier
 * @property int $time_as_pyro
 * @property int $kills_as_pyro
 * @property int $assists_as_pyro
 * @property int $deaths_as_pyro
 * @property int $damage_as_pyro
 * @property int $time_as_demoman
 * @property int $kills_as_demoman
 * @property int $assists_as_demoman
 * @property int $deaths_as_demoman
 * @property int $damage_as_demoman
 * @property int $time_as_heavyweapons
 * @property int $kills_as_heavyweapons
 * @property int $assists_as_heavyweapons
 * @property int $deaths_as_heavyweapons
 * @property int $damage_as_heavyweapons
 * @property int $time_as_engineer
 * @property int $kills_as_engineer
 * @property int $assists_as_engineer
 * @property int $deaths_as_engineer
 * @property int $damage_as_engineer
 * @property int $time_as_medic
 * @property int $kills_as_medic
 * @property int $assists_as_medic
 * @property int $deaths_as_medic
 * @property int $damage_as_medic
 * @property int $time_as_sniper
 * @property int $kills_as_sniper
 * @property int $assists_as_sniper
 * @property int $deaths_as_sniper
 * @property int $damage_as_sniper
 * @property int $time_as_spy
 * @property int $kills_as_spy
 * @property int $assists_as_spy
 * @property int $deaths_as_spy
 * @property int $damage_as_spy
 * @property int $scouts_killed
 * @property float $scouts_killed_per_minute
 * @property int $scouts_assisted
 * @property float $scouts_assisted_per_minute
 * @property int $deaths_to_scouts
 * @property float $deaths_to_scouts_per_minute
 * @property int $soldiers_killed
 * @property float $soldiers_killed_per_minute
 * @property int $soldiers_assisted
 * @property float $soldiers_assisted_per_minute
 * @property int $deaths_to_soldiers
 * @property float $deaths_to_soldiers_per_minute
 * @property int $pyros_killed
 * @property float $pyros_killed_per_minute
 * @property int $pyros_assisted
 * @property float $pyros_assisted_per_minute
 * @property int $deaths_to_pyros
 * @property float $deaths_to_pyros_per_minute
 * @property int $demomen_killed
 * @property float $demomen_killed_per_minute
 * @property int $demomen_assisted
 * @property float $demomen_assisted_per_minute
 * @property int $deaths_to_demomen
 * @property float $deaths_to_demomen_per_minute
 * @property int $heavies_killed
 * @property float $heavies_killed_per_minute
 * @property int $heavies_assisted
 * @property float $heavies_assisted_per_minute
 * @property int $deaths_to_heavies
 * @property float $deaths_to_heavies_per_minute
 * @property int $engineers_killed
 * @property float $engineers_killed_per_minute
 * @property int $engineers_assisted
 * @property float $engineers_assisted_per_minute
 * @property int $deaths_to_engineers
 * @property float $deaths_to_engineers_per_minute
 * @property int $medics_killed
 * @property float $medics_killed_per_minute
 * @property int $medics_assisted
 * @property float $medics_assisted_per_minute
 * @property int $deaths_to_medics
 * @property float $deaths_to_medics_per_minute
 * @property int $snipers_killed
 * @property float $snipers_killed_per_minute
 * @property int $snipers_assisted
 * @property float $snipers_assisted_per_minute
 * @property int $deaths_to_snipers
 * @property float $deaths_to_snipers_per_minute
 * @property int $spies_killed
 * @property float $spies_killed_per_minute
 * @property int $spies_assisted
 * @property float $spies_assisted_per_minute
 * @property int $deaths_to_spies
 * @property float $deaths_to_spies_per_minute
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class PlayerStatline extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                                => 'integer',
        'log_id'                            => 'integer',
        'steam_id'                          => 'string',
        'prec_enabled'                      => 'boolean',
        'kills_per_minute'                  => 'float',
        'assists_per_minute'                => 'float',
        'deaths_per_minute'                 => 'float',
        'suicides_per_minute'               => 'float',
        'damage_per_minute'                 => 'float',
        'damage_real_per_minute'            => 'float',
        'damage_taken_per_minute'           => 'float',
        'heals_received_per_minute'         => 'float',
        'airshots_per_minute'               => 'float',
        'medkit_pickup_per_minute'          => 'float',
        'medkit_health_per_minute'          => 'float',
        'backstabs_per_minute'              => 'float',
        'headshot_kills_per_minute'         => 'float',
        'headshots_per_minute'              => 'float',
        'kills_per_death'                   => 'float',
        'kills_assists_per_death'           => 'float',
        'assists_per_death'                 => 'float',
        'sentries_per_minute'               => 'float',
        'dispensers_per_minute'             => 'float',
        'extinguishes_per_minute'           => 'float',
        'feigns_per_minute'                 => 'float',
        'point_captures_per_minute'         => 'float',
        'intel_captures_per_minute'         => 'float',
        'charges_per_minute'                => 'float',
        'charges_uber_per_minute'           => 'float',
        'charges_kritzkrieg_per_minute'     => 'float',
        'charges_vaccinator_per_minute'     => 'float',
        'charges_quickfix_per_minute'       => 'float',
        'drops_per_minute'                  => 'float',
        'advantages_lost_per_minute'        => 'float',
        'biggest_advantage_lost_per_minute' => 'float',
        'average_time_before_healing'       => 'float',
        'average_time_to_build'             => 'float',
        'average_time_before_using'         => 'float',
        'average_charge_length'             => 'float',
        'scouts_killed_per_minute'          => 'float',
        'scouts_assisted_per_minute'        => 'float',
        'deaths_to_scouts_per_minute'       => 'float',
        'soldiers_killed_per_minute'        => 'float',
        'soldiers_assisted_per_minute'      => 'float',
        'deaths_to_soldiers_per_minute'     => 'float',
        'pyros_killed_per_minute'           => 'float',
        'pyros_assisted_per_minute'         => 'float',
        'deaths_to_pyros_per_minute'        => 'float',
        'demomen_killed_per_minute'         => 'float',
        'demomen_assisted_per_minute'       => 'float',
        'deaths_to_demomen_per_minute'      => 'float',
        'heavies_killed_per_minute'         => 'float',
        'heavies_assisted_per_minute'       => 'float',
        'deaths_to_heavies_per_minute'      => 'float',
        'engineers_killed_per_minute'       => 'float',
        'engineers_assisted_per_minute'     => 'float',
        'deaths_to_engineers_per_minute'    => 'float',
        'medics_killed_per_minute'          => 'float',
        'medics_assisted_per_minute'        => 'float',
        'deaths_to_medics_per_minute'       => 'float',
        'snipers_killed_per_minute'         => 'float',
        'snipers_assisted_per_minute'       => 'float',
        'deaths_to_snipers_per_minute'      => 'float',
        'spies_killed_per_minute'           => 'float',
        'spies_assisted_per_minute'         => 'float',
        'deaths_to_spies_per_minute'        => 'float',
        'classes_played'                    => 'array'
    ];

    /**
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    /**
     * @return BelongsTo
     */
    public function steam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function weapons(): HasMany
    {
        return $this->hasMany(\App\Models\PlayerWeapon::class);
    }
}
