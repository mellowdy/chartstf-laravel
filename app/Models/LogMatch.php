<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class LogMatch extends Pivot
{
    protected $table = 'log_match';
}
