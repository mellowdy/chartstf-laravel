<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $log_id
 * @property int $steam_id
 * @property string $tf_class
 * @property int $kills
 * @property int $damage
 * @property float $damage_per_minute
 * @property int $shots
 * @property int $hits
 * @property float $accuracy
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class PlayerWeapon extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'log_id'            => 'integer',
        'steam_id'          => 'string',
        'damage_per_minute' => 'float',
        'accuracy'          => 'float',
    ];

    /**
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    /**
     * @return BelongsTo
     */
    public function steam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function playerStatline(): BelongsTo
    {
        return $this->belongsTo(\App\Models\PlayerStatline::class);
    }
}
