<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $log_id
 * @property string $name
 * @property int $steam_id
 * @property int $streak
 * @property int $tick
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Killstreak extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'       => 'integer',
        'log_id'   => 'integer',
        'steam_id' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function log(): BelongsTo
    {
        return $this->belongsTo(Log::class);
    }

    /**
     * @return BelongsTo
     */
    public function steam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
