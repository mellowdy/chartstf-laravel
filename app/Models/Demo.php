<?php

namespace App\Models;

use App\Models\Filters\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    use HasFactory, Filterable;

    protected $guarded = ['id'];

    protected $dates = [
        'date'
    ];

    public function logs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Log::class)->withTimestamps();
    }
}
