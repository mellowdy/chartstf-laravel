<?php

namespace App\Models;

use App\Models\Filters\Traits\Filterable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property Carbon $date
 * @property string $title
 * @property string $map
 * @property int $duration
 * @property int $duration_real
 * @property Carbon $started_at
 * @property Carbon $stopped_at
 * @property bool $supplemental
 * @property bool $has_real_damage
 * @property bool $has_weapon_damage
 * @property bool $has_accuracy
 * @property bool $has_medkit_pickups
 * @property bool $has_medkit_health
 * @property bool $has_headshot_kills
 * @property bool $has_headshot_hits
 * @property bool $has_backstabs
 * @property bool $has_point_captures
 * @property bool $has_sentries_built
 * @property bool $has_damage_taken
 * @property bool $has_airshots
 * @property bool $has_heals_received
 * @property bool $has_intel_captures
 * @property bool $scoring_attack_defense
 * @property int $uploader_steam_id
 * @property string $uploader_name
 * @property string $uploader_info
 * @property int $red_score
 * @property int $red_kills
 * @property int $red_deaths
 * @property int $red_damage
 * @property int $red_first_caps
 * @property int $red_caps
 * @property int $red_charges
 * @property int $red_drops
 * @property string $red_team_name
 * @property string $red_team_name_league
 * @property int $blu_score
 * @property int $blu_kills
 * @property int $blu_deaths
 * @property int $blu_damage
 * @property int $blu_first_caps
 * @property int $blu_caps
 * @property int $blu_charges
 * @property int $blu_drops
 * @property string $blu_team_name
 * @property string $blu_team_name_league
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Log extends Model
{
    use HasFactory, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                     => 'integer',
        'started_at'             => 'datetime',
        'stopped_at'             => 'datetime',
        'supplemental'           => 'boolean',
        'has_real_damage'        => 'boolean',
        'has_weapon_damage'      => 'boolean',
        'has_accuracy'           => 'boolean',
        'has_medkit_pickups'     => 'boolean',
        'has_medkit_health'      => 'boolean',
        'has_headshot_kills'     => 'boolean',
        'has_headshot_hits'      => 'boolean',
        'has_backstabs'          => 'boolean',
        'has_point_captures'     => 'boolean',
        'has_sentries_built'     => 'boolean',
        'has_damage_taken'       => 'boolean',
        'has_airshots'           => 'boolean',
        'has_heals_received'     => 'boolean',
        'has_intel_captures'     => 'boolean',
        'has_zip_data'           => 'boolean',
        'scoring_attack_defense' => 'boolean',
        'uploader_steam_id'      => 'integer'
    ];

    protected $dates = ['date'];

    /**
     * @return BelongsTo
     */
    public function uploaderSteam(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function rounds(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Round::class)->orderBy('started_at');
    }

    public function collections(): BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Collection::class)->withTimestamps();
    }

    public function spawns(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Spawn::class);
    }

    public function switches(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\ClassSwitch::class);
    }

    public function weapons(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\PlayerWeapon::class);
    }

    public function statlines(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\PlayerStatline::class);
    }


    public function roundEvents(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\RoundEvent::class);
    }

    public function roundPlayers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\RoundPlayer::class);
    }

    public function dominations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Domination::class);
    }

    public function revenges(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Revenge::class);
    }

    public function disconnects(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Disconnect::class);
    }

    public function healSpreads(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\HealSpread::class)
                    ->orderBy('heal_amount', 'desc');
    }

    public function killstreaks(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Killstreak::class);
    }

    public function chats(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Chat::class);
    }

    public function format(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Format::class);
    }

    public function gamemode(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Gamemode::class);
    }

    public function leagueMatches(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\Models\LeagueMatch::class, 'log_match', 'log_id', 'match_id')->withTimestamps();
    }

    public function demos(): BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Demo::class)->withTimestamps();
    }
}
