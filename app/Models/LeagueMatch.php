<?php

namespace App\Models;

use App\Models\Filters\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeagueMatch extends Model
{
    use HasFactory, Filterable;

    protected $guarded = ['id'];

    protected $table = 'matches';

    protected $casts = [
        'maps_played' => 'array'
    ];

    protected $dates = [
        'scheduled_date',
        'results_submitted_date'
    ];

    public function awayTeam(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Team::class, 'away_team_id');
    }

    public function homeTeam(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Team::class, 'home_team_id');
    }

    public function collection(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Collection::class);
    }

    public function logs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Log::class, 'log_match', 'match_id', 'log_id')->withTimestamps();
    }

    public function competitionParticipations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\CompetitionParticipation::class, 'match_id');
    }

    public function vods(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Vod::class, 'league_match_id');
    }
}
