<?php

namespace App\Enums;

enum Formats: int
{
    case HIGHLANDER = 1;
    case SIXES = 2;
    case FOURS = 3;
    case ULTIDUO = 4;
    case PROLANDER = 5;
    case MGE = 6;
    case CUSTOM = 7;
}