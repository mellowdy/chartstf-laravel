<?php

namespace App\Enums;

enum GameModes: int
{
    case ATK_DEF = 1;
    case CTF = 2;
    case PASS_TIME = 3;
    case TERRITORIAL_CONTROL = 4;
    case PLAYER_DESTRUCTION = 5;
    case PAYLOAD_RACE = 6;
    case PAYLOAD = 7;
    case KING_OF_THE_HILL = 8;
    case CONTROL_POINTS = 9;
}