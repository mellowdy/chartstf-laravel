<?php

namespace App\Exports\Sheets;

use App\Models\Collection;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Sheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MedicSheet implements FromView, ShouldAutoSize, WithTitle, WithStyles
{
    protected Collection $collection;
    protected \Illuminate\Support\Collection $data;

    public function __construct(Collection $collection, \Illuminate\Support\Collection $data)
    {
        $this->collection = $collection;
        $this->data       = $data;
    }

    public function view(): View
    {
        return view('exports.medic', [
            'data' => $this->data->where('most_played_class', 'medic')->values()
        ]);
    }

    public function title(): string
    {
        return 'Medics';
    }

    public function styles(Worksheet $sheet): array
    {
        return [
            1 => ['font' => ['bold' => true]]
        ];
    }
}