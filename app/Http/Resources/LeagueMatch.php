<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LeagueMatch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'home_team_score' => $this->home_team_score,
            'away_team_score' => $this->away_team_score,
            'home_team_name'  => $this->home_team_name,
            'away_team_name'  => $this->away_team_name,
            'home_team_id'    => $this->home_team_id,
            'away_team_id'    => $this->away_team_id,
            'vods'            => $this->whenLoaded('vods', function () {
                return $this->vods;
            })
        ];
    }
}
