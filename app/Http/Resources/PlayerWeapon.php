<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlayerWeapon extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'              => $this->name,
            'tf_class'          => $this->tf_class,
            'kills'             => $this->kills,
            'damage'            => $this->damage,
            'damage_per_minute' => $this->damage_per_minute,
            'shots'             => $this->shots,
            'hits'              => $this->hits,
            'accuracy'          => $this->accuracy
        ];
    }
}
