<?php

namespace App\Http\Resources;

use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

class ClassSwitch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'steam_id'        => $this->steam_id,
            'time_in_seconds' => $this->time,
            'time_h'          => CarbonInterval::seconds($this->time)->cascade()->format('%H:%I:%S'),
            'when'            => !empty($this->when) ? $this->when->format('H:i:s') : null,
            'in_game_name'    => $this->name,
            'common_name'     => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'img'             => $this->whenLoaded('steam', function () {
                return $this->steam->steam_profile_picture_medium;
            }),
            'team'            => $this->team,
            'tf_class'        => $this->tf_class
        ];
    }
}
