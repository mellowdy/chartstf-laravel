<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Chat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'in_game_name' => $this->name,
            'common_name'  => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'img'          => $this->whenLoaded('steam', function () {
                return $this->steam->steam_profile_picture_medium;
            }),
            'steam_id'     => $this->steam_id,
            'message'      => $this->message
        ];
    }
}
