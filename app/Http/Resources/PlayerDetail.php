<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlayerDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                           => $this->id,
            'name'                         => $this->name,
            'steam_profile_picture_medium' => $this->steam_profile_picture_medium,
            'steam_profile_picture_big'    => $this->steam_profile_picture_big,
            'steam_profile_picture_small'  => $this->steam_profile_picture_small,
            /*'matches'                      => $this->whenLoaded('leagueMatches', function () {
                return $this->leagueMatches;
            })*/
        ];
    }
}
