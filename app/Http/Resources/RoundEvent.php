<?php

namespace App\Http\Resources;

use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

class RoundEvent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'            => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'img'             => $this->whenLoaded('steam', function () {
                return $this->steam->steam_profile_picture_medium;
            }),
            'target_name'     => $this->whenLoaded('killerSteam', function () {
                return $this->killerSteam->name;
            }),
            'target_img'      => $this->whenLoaded('killerSteam', function () {
                return $this->killerSteam->steam_profile_picture_medium;
            }),
            'type'            => $this->type,
            'duration'        => $this->tick,
            'tick'            => $this->tick * 66,
            'time_h'          => $this->tick >= 3600
                ? CarbonInterval::seconds($this->tick)->cascade()->format('%H:%I:%S')
                : CarbonInterval::seconds($this->tick)->cascade()->format('%I:%S'),
            'steam_id'        => $this->steam_id,
            'killer_steam_id' => $this->killer_steam_id,
            'team'            => $this->team,
            'point'           => $this->point,
            'medigun'         => $this->medigun
        ];
    }
}
