<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Team extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'team_picture'   => $this->team_picture,
            'league_team_id' => $this->league_team_id,
            'team_url'       => $this->team_url,
            'league'         => $this->league,
            'gamemode'       => $this->gamemode,
            'tag'            => $this->tag,
            'country'        => $this->country,
            'players'        => $this->whenLoaded('players', function () {
                return new PlayerCollection($this->players);
            }),
            'matches'        => $this->whenLoaded('leagueMatches', function () {
                return new LeagueMatchCollection($this->leagueMatches);
            })
        ];
    }
}
