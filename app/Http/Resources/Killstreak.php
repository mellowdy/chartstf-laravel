<?php

namespace App\Http\Resources;

use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

class Killstreak extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'in_game_name' => $this->name,
            'common_name'  => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'img'          => $this->whenLoaded('steam', function () {
                return $this->steam->steam_profile_picture_medium;
            }),
            'steam_id'     => $this->steam_id,
            'streak'       => $this->streak,
            // The 'tick' value isn't actually the tick.
            // It is the amount of seconds in the game that has passed.
            // To convert that into ticks, we need to multiply by the server tick rate,
            // which should be 66 for competitive servers.
            'tick'         => $this->tick * 66,
            'time_h'       => CarbonInterval::seconds($this->tick)->cascade()->format('%H:%I:%S'),
        ];
    }
}
