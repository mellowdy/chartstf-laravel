<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Log extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'title'    => $this->title,
            'map'      => $this->map,
            'date'     => !empty($this->date) ? $this->date->format('Y-m-d H:i:s') : null,
            'date_h'   => !empty($this->date) ? $this->date->diffForHumans() : null,
            'date_l'   => !empty($this->date) ? $this->date->format('M d, Y g:i A') : null,
            'duration' => $this->duration,
            'gamemode' => match ($this->statlines_count) {
                4 => 'BBall',
                2 => '2 players',
                8 => '4v4',
                12 => '6v6',
                18 => 'Highlander',
                14 => 'Prolander',
                default => 'Custom'
            }
        ];
    }
}
