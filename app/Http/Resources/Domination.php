<?php

namespace App\Http\Resources;

use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

class Domination extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'steam_id'            => $this->steam_id,
            'team'                => $this->team,
            'in_game_name'        => $this->name,
            'common_name'         => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'target_steam_id'     => $this->target_steam_id,
            'time_in_seconds'     => $this->time,
            'time_h'              => CarbonInterval::seconds($this->time)->cascade()->format('%H:%I:%S'),
            'target_team'         => $this->target_team,
            'target_in_game_name' => $this->target_name,
            'target_common_name'  => $this->whenLoaded('targetSteam', function () {
                return $this->targetSteam->name;
            })
        ];
    }
}
