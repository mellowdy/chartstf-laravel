<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoundPlayer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'common_name' => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'steam_id'    => $this->steam_id,
            'kills'       => $this->kills,
            'damage'      => $this->damage,
            'team'        => $this->team,
        ];
    }
}
