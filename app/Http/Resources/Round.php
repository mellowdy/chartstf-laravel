<?php

namespace App\Http\Resources;

use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

class Round extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'winner'       => $this->winner,
            'red_score'    => $this->red_score,
            'red_kills'    => $this->red_kills,
            'red_damage'   => $this->red_damage,
            'red_charges'  => $this->red_charges,
            'blu_score'    => $this->blu_score,
            'blu_kills'    => $this->blu_kills,
            'blu_damage'   => $this->blu_damage,
            'blu_charges'  => $this->blu_charges,
            'first_cap'    => $this->first_cap,
            'duration'     => $this->duration,
            'duration_h'   => CarbonInterval::seconds($this->duration)->cascade()->format('%H:%I:%S'),
            'started_at'   => !empty($this->started_at) ? $this->started_at->format('H:i:s') : null,
            'started_at_h' => !empty($this->started_at) ? $this->started_at->diffForHumans() : null,
            'stopped_at'   => !empty($this->stopped_at) ? $this->stopped_at->format('H:i:s') : null,
            'stopped_at_h' => !empty($this->stopped_at) ? $this->stopped_at->diffForHumans() : null,
            'players'      => $this->whenLoaded('players', function () {
                return new RoundPlayerCollection($this->players);
            }),
            'events'       => $this->whenLoaded('events', function () {
                return new RoundEventCollection($this->events);
            })
        ];
    }
}
