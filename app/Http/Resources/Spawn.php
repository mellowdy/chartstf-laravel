<?php

namespace App\Http\Resources;

use Carbon\CarbonInterval;
use Illuminate\Http\Resources\Json\JsonResource;

class Spawn extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'in_game_name'    => $this->name,
            'steam_id'        => $this->steam_id,
            'common_name'     => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'img'             => $this->whenLoaded('steam', function () {
                return $this->steam->steam_profile_picture_medium;
            }),
            'spawned_at'      => !empty($this->spawned_at) ? $this->spawned_at->format('H:i:s') : null,
            'time_in_seconds' => $this->time,
            'time_h'          => CarbonInterval::seconds($this->time)->cascade()->format('%H:%I:%S'),
            'tf_class'        => $this->tf_class,
            'team'            => $this->team
        ];
    }
}
