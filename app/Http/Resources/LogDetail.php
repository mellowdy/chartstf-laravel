<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LogDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'title'          => $this->title,
            'map'            => $this->map,
            'blu_score'      => $this->blu_score,
            'red_score'      => $this->red_score,
            'date'           => !empty($this->date) ? $this->date->format('Y-m-d H:i:s') : null,
            'date_h'         => !empty($this->date) ? $this->date->diffForHumans() : null,
            'date_l'         => !empty($this->date) ? $this->date->format('l d, Y g:i A') : null,
            'duration'       => $this->duration,
            'duration_real'  => $this->duration_real,
            'gamemode'       => match ($this->statlines_count) {
                4 => 'BBall',
                2 => '2 players',
                8 => '4v4',
                12 => '6v6',
                18 => 'Highlander',
                14 => 'Prolander',
                default => 'Custom'
            },
            'has_zip_data'   => $this->has_zip_data,
            'has_accuracy'   => $this->has_accuracy,
            'spawns'         => $this->whenLoaded('spawns', function () {
                return new SpawnCollection($this->spawns);
            }),
            'heal_spreads'   => $this->whenLoaded('healSpreads', function () {
                return new HealSpreadCollection($this->healSpreads);
            }),
            'revenges'       => $this->whenLoaded('revenges', function () {
                return new RevengeCollection($this->revenges);
            }),
            'dominations'    => $this->whenLoaded('dominations', function () {
                return new DominationCollection($this->dominations);
            }),
            'killstreaks'    => $this->whenLoaded('killstreaks', function () {
                return new KillstreakCollection($this->killstreaks);
            }),
            'statlines'      => $this->whenLoaded('statlines', function () {
                return new PlayerStatlineCollection($this->statlines);
            }),
            'rounds'         => $this->whenLoaded('rounds', function () {
                return new RoundCollection($this->rounds);
            }),
            'class_switches' => $this->whenLoaded('switches', function () {
                return new ClassSwitchCollection($this->switches);
            }),
            'disconnects'    => $this->whenLoaded('disconnects', function () {
                return new DisconnectCollection($this->disconnects);
            }),
            'chat'           => $this->whenLoaded('chats', function () {
                return new ChatCollection($this->chats);
            }),
            'league_matches' => $this->whenLoaded('leagueMatches', function() {
                return new LeagueMatchCollection($this->leagueMatches);
            })
        ];
    }
}
