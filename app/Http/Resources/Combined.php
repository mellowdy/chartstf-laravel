<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Combined extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'slug'       => $this->slug,
            'is_private' => $this->is_private,
            'steam_id'   => $this->steam_id,
            'created_by' => $this->whenLoaded('steam', function () {
                return $this->steam->name;
            }),
            'gamemode'   => $this->gamemode,
            'date'       => !empty($this->finished_at) ? $this->finished_at->format('Y-m-d H:i:s') : null,
            'date_h'     => !empty($this->finished_at) ? $this->finished_at->diffForHumans() : null,
            'date_l'     => !empty($this->finished_at) ? $this->finished_at->format('M d, Y g:i A') : null,
            'logs'       => $this->whenLoaded('logs', function () {
                return $this->logs->map(fn ($log) => [
                    'id'     => $log->id,
                    'title'  => $log->title,
                    'map'    => $log->map,
                    'date'   => !empty($log->date) ? $log->date->format('Y-m-d H:i:s') : null,
                    'date_h' => !empty($log->date) ? $log->date->diffForHumans() : null,
                    'date_l' => !empty($log->date) ? $log->date->format('M d, Y g:i A') : null,
                    'home'   => $log->leagueMatches->count() > 0 ? $log->leagueMatches[0]->home_team_name : null,
                    'away'   => $log->leagueMatches->count() > 0 ? $log->leagueMatches[0]->away_team_name : null,
                    'vod'    => $log->leagueMatches[0]->vods[0] ?? null,
                    'demos'  => $log->demos->map(fn ($demo) => [
                        'download_url' => $demo->download_url,
                        'demostf_url'  => 'https://demos.tf/' . $demo->demostf_id,
                        'map'          => $demo->map,
                        'date'         => !empty($demo->date) ? $demo->date->format('M d, Y g:i A') : null
                    ])
                ]);
            })
        ];
    }
}
