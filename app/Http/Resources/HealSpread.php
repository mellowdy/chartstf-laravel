<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HealSpread extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'heal_amount'     => $this->heal_amount,
            'tf_class'        => $this->tf_class,
            'healer_steam_id' => $this->healer_steam_id,
            'target_steam_id' => $this->target_steam_id,
            'healer_name'     => $this->whenLoaded('healerSteam', function () {
                return $this->healerSteam->name;
            }),
            'target_name'     => $this->whenLoaded('targetSteam', function () {
                return $this->targetSteam->name;
            })
        ];
    }
}
