<?php

namespace App\Http\Controllers;

use App\Exports\CombinedExport;
use App\Http\Requests\StoreCollectionRequest;
use App\Http\Resources\LogCollection;
use App\Jobs\AddLogsToCollection;
use App\Models\Collection;
use App\Models\Filters\CollectionFilter;
use App\Models\Format;
use App\Models\Gamemode;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Illuminate\Http\Request;
use App\Http\Resources\CombinedCollection;
use App\Http\Resources\Combined as CollectionResource;
use Illuminate\Support\Facades\DB;

class CollectionController extends Controller
{
    protected LogsRepo $logsRepo;

    public function __construct(LogsRepo $logsRepo)
    {
        $this->logsRepo = $logsRepo;
    }

    public function index(Request $request): \Inertia\Response|\Inertia\ResponseFactory
    {
        $collections = $this->getCollections($request);
        return inertia('LogCollections', [
            'collections' => $collections,
            'formats'     => Format::select('id', 'name')->orderBy('name')->get(),
            'leagues'     => ['ETF2L', 'RGL', 'ozfortress', 'UGC', 'Cup'],
            'divisions'   => Collection::select('division')->orderBy('division')->distinct()->get()->pluck('division')
        ]);
    }

    private function getCollections(Request $request): CombinedCollection
    {
        return new CombinedCollection(
            Collection::with(['steam' => function ($q) {
                $q->select('id', 'name', 'steam_profile_picture_medium');
            }])
                      ->filter(new CollectionFilter($request))
                      ->where('is_private', 0)
                      ->orderByDesc('finished_at')
                      ->orderBy('name', 'desc')
                      ->paginate($request->get('limit', 50))
                      ->withQueryString()
        );
    }

    public function create()
    {
        return inertia('CreateCollection');
    }

    /**
     * @throws \Exception
     */
    public function store(StoreCollectionRequest $request): \Illuminate\Http\RedirectResponse
    {
        $collection = Collection::create(array_merge($request->only('name', 'gamemode', 'league'), ['is_private' => 1]));

        AddLogsToCollection::dispatch((object)$request->all(), $collection->id);

        return redirect()->to(route('collections'));
        //return redirect()->to(route('collection.detail', ['collection' => $collection->slug]));
    }

    /**
     * @param Collection $collection
     * @return \Illuminate\Http\RedirectResponse|\Inertia\Response|\Inertia\ResponseFactory
     */
    public function show(Collection $collection)
    {
        $collection->load(['logs' => function ($q) {
            $q->select('id', 'map', 'title', 'duration', 'date');
        }, 'logs.leagueMatches'   => function ($q) {
            $q->select('home_team_name', 'id', 'away_team_name');
        }, 'logs.leagueMatches.vods', 'logs.demos']);

        if ($collection->logs->count() == 0 || $collection->is_private) {
            return redirect()->to(route('collections'));
        }

        $healSpreads = collect(DB::select(
            "
            SELECT 
    SUM(heal_spreads.heal_amount) AS total_heals,
    CAST(MAX(heal_spreads.healer_steam_id) AS CHAR) AS healer_steam_id,
    CAST(MAX(heal_spreads.target_steam_id) AS CHAR) AS target_steam_id,
    MAX(healers.name) as healer_name,
    MAX(healers.steam_profile_picture_medium) as healer_img,
    MAX(heal_targets.name) as target_name,
    MAX(heal_targets.steam_profile_picture_medium) as target_img,
    SUM(heal_spreads.heal_amount) / SUM(logs.duration_real / 60) as total_heals_per_minute
FROM
    heal_spreads
LEFT JOIN users as healers ON healers.id = heal_spreads.healer_steam_id
LEFT JOIN users as heal_targets ON heal_targets.id = heal_spreads.target_steam_id
LEFT JOIN logs ON heal_spreads.log_id = logs.id
WHERE heal_spreads.log_id in (" . implode(', ', $collection->logs->pluck('id')->toArray()) . ")
GROUP BY heal_spreads.healer_steam_id, heal_spreads.target_steam_id
ORDER BY SUM(heal_spreads.heal_amount) desc;
           "
        ))->map(fn ($spread) => [
            'total_heals'            => round($spread->total_heals, 2),
            'healer_steam_id'        => $spread->healer_steam_id,
            'target_steam_id'        => $spread->target_steam_id,
            'healer_name'            => $spread->healer_name,
            'target_name'            => $spread->target_name,
            'healer_img'             => $spread->healer_img,
            'target_img'             => $spread->target_img,
            'total_heals_per_minute' => round($spread->total_heals_per_minute, 2)
        ]);

        $weapons = collect(DB::select("
        SELECT 
    MAX(users.steam_profile_picture_medium) as img,
    MAX(users.name) AS common_name,
    CAST(player_weapons.steam_id AS CHAR) AS steam_id,
    player_weapons.name AS weapon_name,
    player_weapons.tf_class AS class_played,
    SUM(player_weapons.kills) AS kills,
    SUM(player_weapons.damage) AS damage,
    SUM(player_weapons.damage_per_minute) AS damage_per_minute,
    SUM(player_weapons.shots) AS shots,
    SUM(player_weapons.hits) AS hits,
    AVG(player_weapons.accuracy) AS accuracy
FROM
    player_weapons
        LEFT JOIN
    users ON users.id = player_weapons.steam_id
        LEFT JOIN
    logs ON logs.id = player_weapons.log_id
        WHERE player_weapons.log_id in (" . implode(', ', $collection->logs->pluck('id')->toArray()) . ")
GROUP BY player_weapons.steam_id , player_weapons.tf_class , player_weapons.name
ORDER BY users.name;
        "))->map(fn ($weapon) => [
            'common_name'       => $weapon->common_name,
            'steam_id'          => $weapon->steam_id,
            'img'               => $weapon->img,
            'weapon_name'       => $weapon->weapon_name,
            'class_played'      => $weapon->class_played,
            'kills'             => (int)$weapon->kills,
            'damage'            => (int)$weapon->damage,
            'damage_per_minute' => (int)$weapon->damage_per_minute,
            'shots'             => (int)$weapon->shots,
            'hits'              => (int)$weapon->hits,
            'accuracy'          => round($weapon->accuracy, 2)
        ]);

        $data = collect(DB::select("
                SELECT
    MAX(users.steam_profile_picture_medium) as img,
    MAX(users.name) AS common_name,
    CAST(player_statlines.steam_id AS CHAR) AS steam_id,
    COUNT(player_statlines.id) AS total_logs,
    sec_to_time(SUM(logs.duration_real)) as time_played,
    SUM(logs.duration_real) as time_played_in_seconds,
    SUM(player_statlines.kills) AS total_kills,
    SUM(player_statlines.deaths) AS total_deaths,
    SUM(player_statlines.assists) AS total_assists,
    MAX(player_statlines.class_played) as most_played_class,
    GROUP_CONCAT(DISTINCT player_statlines.class_played SEPARATOR ', ') as classes_played,
    MAX(longest_killstreak) as longest_killstreak,
    SUM(player_statlines.kills) / (SUM(logs.duration_real) / 60) as average_kills_per_minute,
    SUM(player_statlines.deaths) / (SUM(logs.duration_real) / 60) as average_deaths_per_minute,
    SUM(player_statlines.assists) / (SUM(logs.duration_real) / 60) as average_assists_per_minute,
    SUM(player_statlines.kills) / SUM(player_statlines.deaths) as average_kills_per_death,
    (SUM(player_statlines.kills) + SUM(player_statlines.assists)) / SUM(player_statlines.deaths) as average_kills_assists_per_death,
    SUM(player_statlines.damage) as total_damage,
    SUM(player_statlines.damage) / SUM(logs.duration_real / 60) as average_damage_per_minute,
    SUM(player_statlines.damage_taken) as total_damage_taken,
    SUM(player_statlines.damage_taken) / SUM(logs.duration_real / 60) as average_damage_taken_per_minute,
    SUM(player_statlines.sentries_built) as total_sentries_built,
    SUM(player_statlines.sentries_built) / SUM(logs.duration_real / 60) as average_sentries_built,
    SUM(player_statlines.dispensers_built) as total_dispensers_built,
    SUM(player_statlines.dispensers_built) / SUM(logs.duration_real / 60) as average_dispensers_built,
    SUM(player_statlines.teleporters_built) as total_teleporters_built,
    SUM(player_statlines.teleporters_built) / SUM(logs.duration_real / 60) as average_teleporters_built,
    SUM(player_statlines.charges) as total_charges,
    SUM(player_statlines.charges_uber) as total_charges_uber,
    SUM(player_statlines.charges_kritzkrieg) as total_charges_kritzkrieg,
    SUM(player_statlines.charges_vaccinator) as total_charges_vaccinator,
    SUM(player_statlines.charges_quickfix) as total_charges_quickfix,
    SUM(player_statlines.drops) as total_drops,
    SUM(player_statlines.drops) / SUM(logs.duration_real / 60) as average_drops,
    SUM(player_statlines.heals_received) as total_heals_received,
    SUM(player_statlines.heals_received) / SUM(logs.duration_real / 60) as average_heals_received_per_minute,
    SUM(player_statlines.backstabs) as total_backstabs,
    SUM(player_statlines.backstabs) / SUM(logs.duration_real / 60) as average_backstabs_per_minute,
    SUM(player_statlines.headshots) as total_headshots,
    SUM(player_statlines.headshots) / SUM(logs.duration_real / 60) as average_headshots_per_minute,
    SUM(player_statlines.airshots) as total_airshots,
    SUM(player_statlines.airshots) / SUM(logs.duration_real / 60) as average_airshots_per_minute,
    SUM(player_statlines.medkit_health) as total_medkit_health,
    SUM(player_statlines.medkit_pickup) as total_medkit_pickup,
    SUM(player_statlines.feigns) as total_feigns,
    SUM(player_statlines.feigns_per_minute) / SUM(logs.duration_real / 60) as average_feigns_per_minute,
    SUM(player_statlines.sentries_destroyed) as total_sentries_destroyed,
    SUM(player_statlines.sentries_destroyed) / SUM(logs.duration_real / 60) as average_sentries_destroyed_per_minute,
    SUM(player_statlines.dispensers_destroyed) as total_dispensers_destroyed,
    SUM(player_statlines.dispensers_destroyed) / SUM(logs.duration_real / 60) as average_dispensers_destroyed_per_minute,
    SUM(player_statlines.teleporters_destroyed) as total_teleporters_destroyed,
    SUM(player_statlines.teleporters_destroyed) / SUM(logs.duration_real / 60) as average_teleporters_destroyed_per_minute,
    SUM(player_statlines.extinguishes) as total_extinguishes,
    SUM(player_statlines.extinguishes) / SUM(logs.duration_real / 60) as average_extinguishes_per_minute,
    SUM(player_statlines.point_captures) as total_point_captures,
    SUM(player_statlines.advantages_lost) as total_advantages_lost,
    MAX(player_statlines.biggest_advantage_lost) as biggest_advantage_lost,
    AVG(player_statlines.average_charge_length) as average_charge_length,
    AVG(player_statlines.average_time_to_build) as average_time_to_build,
    AVG(player_statlines.average_time_before_using) as average_time_before_using,
    AVG(player_statlines.average_time_before_healing) as average_time_before_healing,
    sec_to_time(SUM(player_statlines.time_as_scout)) as total_time_on_scout,
    sec_to_time(SUM(player_statlines.time_as_soldier)) as total_time_on_soldier,
    sec_to_time(SUM(player_statlines.time_as_pyro)) as total_time_on_pyro,
    sec_to_time(SUM(player_statlines.time_as_demoman)) as total_time_on_demoman,
    sec_to_time(SUM(player_statlines.time_as_heavyweapons)) as total_time_on_heavyweapons,
    sec_to_time(SUM(player_statlines.time_as_engineer)) as total_time_on_engineer,
    sec_to_time(SUM(player_statlines.time_as_medic)) as total_time_on_medic,
    sec_to_time(SUM(player_statlines.time_as_sniper)) as total_time_on_sniper,
    sec_to_time(SUM(player_statlines.time_as_spy)) as total_time_on_spy,
    SUM(player_statlines.kills_as_scout) as total_kills_on_scout,
    SUM(player_statlines.kills_as_soldier) as total_kills_on_soldier,
    SUM(player_statlines.kills_as_pyro) as total_kills_on_pyro,
    SUM(player_statlines.kills_as_demoman) as total_kills_on_demoman,
    SUM(player_statlines.kills_as_heavyweapons) as total_kills_on_heavyweapons,
    SUM(player_statlines.kills_as_engineer) as total_kills_on_engineer,
    SUM(player_statlines.kills_as_medic) as total_kills_on_medic,
    SUM(player_statlines.kills_as_sniper) as total_kills_on_sniper,
    SUM(player_statlines.kills_as_spy) as total_kills_on_spy,
    SUM(player_statlines.assists_as_scout) as total_assists_on_scout,
    SUM(player_statlines.assists_as_soldier) as total_assists_on_soldier,
    SUM(player_statlines.assists_as_pyro) as total_assists_on_pyro,
    SUM(player_statlines.assists_as_demoman) as total_assists_on_demoman,
    SUM(player_statlines.assists_as_heavyweapons) as total_assists_on_heavyweapons,
    SUM(player_statlines.assists_as_engineer) as total_assists_on_engineer,
    SUM(player_statlines.assists_as_medic) as total_assists_on_medic,
    SUM(player_statlines.assists_as_sniper) as total_assists_on_sniper,
    SUM(player_statlines.assists_as_spy) as total_assists_on_spy,
    SUM(player_statlines.deaths_as_scout) as total_deaths_on_scout,
    SUM(player_statlines.deaths_as_soldier) as total_deaths_on_soldier,
    SUM(player_statlines.deaths_as_pyro) as total_deaths_on_pyro,
    SUM(player_statlines.deaths_as_demoman) as total_deaths_on_demoman,
    SUM(player_statlines.deaths_as_heavyweapons) as total_deaths_on_heavyweapons,
    SUM(player_statlines.deaths_as_engineer) as total_deaths_on_engineer,
    SUM(player_statlines.deaths_as_medic) as total_deaths_on_medic,
    SUM(player_statlines.deaths_as_sniper) as total_deaths_on_sniper,
    SUM(player_statlines.deaths_as_spy) as total_deaths_on_spy,
    SUM(player_statlines.damage_as_scout) as total_damage_on_scout,
    SUM(player_statlines.damage_as_soldier) as total_damage_on_soldier,
    SUM(player_statlines.damage_as_pyro) as total_damage_on_pyro,
    SUM(player_statlines.damage_as_demoman) as total_damage_on_demoman,
    SUM(player_statlines.damage_as_heavyweapons) as total_damage_on_heavyweapons,
    SUM(player_statlines.damage_as_engineer) as total_damage_on_engineer,
    SUM(player_statlines.damage_as_medic) as total_damage_on_medic,
    SUM(player_statlines.damage_as_sniper) as total_damage_on_sniper,
    SUM(player_statlines.damage_as_spy) as total_damage_on_spy,
    SUM(player_statlines.scouts_killed) / SUM(logs.duration_real / 60) as total_scouts_killed_per_minute,
    SUM(player_statlines.soldiers_killed) / SUM(logs.duration_real / 60) as total_soldiers_killed_per_minute,
    SUM(player_statlines.pyros_killed) / SUM(logs.duration_real / 60) as total_pyros_killed_per_minute,
    SUM(player_statlines.demomen_killed) / SUM(logs.duration_real / 60) as total_demomen_killed_per_minute,
    SUM(player_statlines.heavies_killed) / SUM(logs.duration_real / 60) as total_heavies_killed_per_minute,
    SUM(player_statlines.engineers_killed) / SUM(logs.duration_real / 60) as total_engineers_killed_per_minute,
    SUM(player_statlines.medics_killed) / SUM(logs.duration_real / 60) as total_medics_killed_per_minute,
    SUM(player_statlines.snipers_killed) / SUM(logs.duration_real / 60) as total_snipers_killed_per_minute,
    SUM(player_statlines.spies_killed) / SUM(logs.duration_real / 60) as total_spies_killed_per_minute,
    SUM(player_statlines.scouts_killed) as total_scouts_killed,
    SUM(player_statlines.soldiers_killed) as total_soldiers_killed,
    SUM(player_statlines.pyros_killed) as total_pyros_killed,
    SUM(player_statlines.demomen_killed) as total_demomen_killed,
    SUM(player_statlines.heavies_killed) as total_heavies_killed,
    SUM(player_statlines.engineers_killed) as total_engineers_killed,
    SUM(player_statlines.medics_killed) as total_medics_killed,
    SUM(player_statlines.snipers_killed) as total_snipers_killed,
    SUM(player_statlines.spies_killed) as total_spies_killed,
    SUM(player_statlines.deaths_to_scouts) as total_deaths_to_scouts,
    SUM(player_statlines.deaths_to_soldiers) as total_deaths_to_soldiers,
    SUM(player_statlines.deaths_to_pyros) as total_deaths_to_pyros,
    SUM(player_statlines.deaths_to_demomen) as total_deaths_to_demomen,
    SUM(player_statlines.deaths_to_heavies) as total_deaths_to_heavies,
    SUM(player_statlines.deaths_to_engineers) as total_deaths_to_engineers,
    SUM(player_statlines.deaths_to_medics) as total_deaths_to_medics,
    SUM(player_statlines.deaths_to_snipers) as total_deaths_to_snipers,
    SUM(player_statlines.deaths_to_spies) as total_deaths_to_spies,
    SUM(player_statlines.dominations) as total_dominations,
    SUM(player_statlines.revenges) as total_revenges
FROM
    player_statlines
        LEFT JOIN users ON users.id = player_statlines.steam_id
        LEFT JOIN logs ON logs.id = player_statlines.log_id
WHERE player_statlines.log_id in (" . implode(', ', $collection->logs->pluck('id')->toArray()) . ")
GROUP BY player_statlines.steam_id, player_statlines.class_played
ORDER BY SUM(case when player_statlines.team = 'Blue' and blu_score > red_score then 1 else 0 end) desc;
            "))->map(fn ($statline) => [
            'total_logs'                               => $statline->total_logs,
            'img'                                      => $statline->img,
            'common_name'                              => $statline->common_name,
            'lowercased_name'                          => strtolower($statline->common_name),
            'steam_id'                                 => $statline->steam_id,
            'most_played_class'                        => $statline->most_played_class,
            'time_played'                              => $statline->time_played,
            'total_kills'                              => (int)$statline->total_kills,
            'total_deaths'                             => (int)$statline->total_deaths,
            'total_assists'                            => (int)$statline->total_assists,
            'classes_played'                           => explode(',', $statline->classes_played),
            'longest_killstreak'                       => (int)$statline->longest_killstreak,
            'average_kills_per_minute'                 => round($statline->average_kills_per_minute, 2),
            'average_deaths_per_minute'                => round($statline->average_deaths_per_minute, 2),
            'average_assists_per_minute'               => round($statline->average_assists_per_minute, 2),
            'average_kills_per_death'                  => round($statline->average_kills_per_death, 2),
            'average_kills_assists_per_death'          => round($statline->average_kills_assists_per_death, 2),
            'total_damage'                             => (int)$statline->total_damage,
            'average_damage_per_minute'                => round($statline->average_damage_per_minute, 2),
            'total_damage_taken'                       => (int)$statline->total_damage_taken,
            'average_damage_taken_per_minute'          => round($statline->average_damage_taken_per_minute, 2),
            'total_sentries_built'                     => (int)$statline->total_sentries_built,
            'average_sentries_built'                   => round($statline->average_sentries_built, 2),
            'total_dispensers_built'                   => (int)$statline->total_dispensers_built,
            'average_dispensers_built'                 => round($statline->average_dispensers_built, 2),
            'total_teleporters_built'                  => (int)$statline->total_teleporters_built,
            'average_teleporters_built'                => round($statline->average_teleporters_built, 2),
            'total_charges'                            => (int)$statline->total_charges,
            'total_charges_uber'                       => (int)$statline->total_charges_uber,
            'total_charges_kritzkrieg'                 => (int)$statline->total_charges_kritzkrieg,
            'total_charges_vaccinator'                 => (int)$statline->total_charges_vaccinator,
            'total_charges_quickfix'                   => (int)$statline->total_charges_quickfix,
            'total_drops'                              => (int)$statline->total_drops,
            'average_drops'                            => round($statline->average_drops, 2),
            'total_heals_received'                     => (int)$statline->total_heals_received,
            'time_played_in_seconds'                   => (int)$statline->time_played_in_seconds,
            'average_heals_received_per_minute'        => round($statline->average_heals_received_per_minute, 2),
            'total_backstabs'                          => (int)$statline->total_backstabs,
            'average_backstabs_per_minute'             => round($statline->average_backstabs_per_minute, 2),
            'total_headshots'                          => (int)$statline->total_headshots,
            'average_headshots_per_minute'             => round($statline->average_headshots_per_minute, 2),
            'total_airshots'                           => (int)$statline->total_airshots,
            'average_airshots_per_minute'              => round($statline->average_airshots_per_minute, 2),
            'total_medkit_health'                      => (int)$statline->total_medkit_health,
            'total_medkit_pickup'                      => (int)$statline->total_medkit_pickup,
            'total_feigns'                             => (int)$statline->total_feigns,
            'average_feigns_per_minute'                => round($statline->average_feigns_per_minute, 2),
            'total_sentries_destroyed'                 => (int)$statline->total_sentries_destroyed,
            'average_sentries_destroyed_per_minute'    => round($statline->average_sentries_destroyed_per_minute, 2),
            'total_dispensers_destroyed'               => (int)$statline->total_dispensers_destroyed,
            'average_dispensers_destroyed_per_minute'  => round($statline->average_dispensers_destroyed_per_minute, 2),
            'total_teleporters_destroyed'              => (int)$statline->total_teleporters_destroyed,
            'average_teleporters_destroyed_per_minute' => round($statline->average_teleporters_destroyed_per_minute, 2),
            'total_extinguishes'                       => (int)$statline->total_extinguishes,
            'average_extinguishes_per_minute'          => round($statline->average_extinguishes_per_minute, 2),
            'total_point_captures'                     => (int)$statline->total_point_captures,
            'total_advantages_lost'                    => (int)$statline->total_advantages_lost,
            'biggest_advantage_lost'                   => (int)$statline->biggest_advantage_lost,
            'average_charge_length'                    => round($statline->average_charge_length, 2),
            'average_time_to_build'                    => round($statline->average_time_to_build, 2),
            'average_time_before_using'                => round($statline->average_time_before_using, 2),
            'average_time_before_healing'              => round($statline->average_time_before_healing, 2),
            'total_time_on_scout'                      => $statline->total_time_on_scout,
            'total_time_on_soldier'                    => $statline->total_time_on_soldier,
            'total_time_on_pyro'                       => $statline->total_time_on_pyro,
            'total_time_on_demoman'                    => $statline->total_time_on_demoman,
            'total_time_on_heavyweapons'               => $statline->total_time_on_heavyweapons,
            'total_time_on_engineer'                   => $statline->total_time_on_engineer,
            'total_time_on_medic'                      => $statline->total_time_on_medic,
            'total_time_on_sniper'                     => $statline->total_time_on_sniper,
            'total_time_on_spy'                        => $statline->total_time_on_spy,
            'total_kills_on_scout'                     => round($statline->total_kills_on_scout, 2),
            'total_kills_on_soldier'                   => round($statline->total_kills_on_soldier, 2),
            'total_kills_on_pyro'                      => round($statline->total_kills_on_pyro, 2),
            'total_kills_on_demoman'                   => round($statline->total_kills_on_demoman, 2),
            'total_kills_on_heavyweapons'              => round($statline->total_kills_on_heavyweapons, 2),
            'total_kills_on_engineer'                  => round($statline->total_kills_on_engineer, 2),
            'total_kills_on_medic'                     => round($statline->total_kills_on_medic, 2),
            'total_kills_on_sniper'                    => round($statline->total_kills_on_sniper, 2),
            'total_kills_on_spy'                       => round($statline->total_kills_on_spy, 2),
            'total_assists_on_scout'                   => round($statline->total_assists_on_scout, 2),
            'total_assists_on_soldier'                 => round($statline->total_assists_on_soldier, 2),
            'total_assists_on_pyro'                    => round($statline->total_assists_on_pyro, 2),
            'total_assists_on_demoman'                 => round($statline->total_assists_on_demoman, 2),
            'total_assists_on_heavyweapons'            => round($statline->total_assists_on_heavyweapons, 2),
            'total_assists_on_engineer'                => round($statline->total_assists_on_engineer, 2),
            'total_assists_on_medic'                   => round($statline->total_assists_on_medic, 2),
            'total_assists_on_sniper'                  => round($statline->total_assists_on_sniper, 2),
            'total_assists_on_spy'                     => round($statline->total_assists_on_spy, 2),
            'total_deaths_on_scout'                    => round($statline->total_deaths_on_scout, 2),
            'total_deaths_on_soldier'                  => round($statline->total_deaths_on_soldier, 2),
            'total_deaths_on_pyro'                     => round($statline->total_deaths_on_pyro, 2),
            'total_deaths_on_demoman'                  => round($statline->total_deaths_on_demoman, 2),
            'total_deaths_on_heavyweapons'             => round($statline->total_deaths_on_heavyweapons, 2),
            'total_deaths_on_engineer'                 => round($statline->total_deaths_on_engineer, 2),
            'total_deaths_on_medic'                    => round($statline->total_deaths_on_medic, 2),
            'total_deaths_on_sniper'                   => round($statline->total_deaths_on_sniper, 2),
            'total_deaths_on_spy'                      => round($statline->total_deaths_on_spy, 2),
            'total_damage_on_scout'                    => round($statline->total_damage_on_scout, 2),
            'total_damage_on_soldier'                  => round($statline->total_damage_on_soldier, 2),
            'total_damage_on_pyro'                     => round($statline->total_damage_on_pyro, 2),
            'total_damage_on_demoman'                  => round($statline->total_damage_on_demoman, 2),
            'total_damage_on_heavyweapons'             => round($statline->total_damage_on_heavyweapons, 2),
            'total_damage_on_engineer'                 => round($statline->total_damage_on_engineer, 2),
            'total_damage_on_medic'                    => round($statline->total_damage_on_medic, 2),
            'total_damage_on_sniper'                   => round($statline->total_damage_on_sniper, 2),
            'total_damage_on_spy'                      => round($statline->total_damage_on_spy, 2),
            'total_scouts_killed'                      => round($statline->total_scouts_killed, 2),
            'total_soldiers_killed'                    => round($statline->total_soldiers_killed, 2),
            'total_pyros_killed'                       => round($statline->total_pyros_killed, 2),
            'total_demomen_killed'                     => round($statline->total_demomen_killed, 2),
            'total_heavies_killed'                     => round($statline->total_heavies_killed, 2),
            'total_engineers_killed'                   => round($statline->total_engineers_killed, 2),
            'total_medics_killed'                      => round($statline->total_medics_killed, 2),
            'total_snipers_killed'                     => round($statline->total_snipers_killed, 2),
            'total_spies_killed'                       => round($statline->total_spies_killed, 2),
            'total_scouts_killed_per_minute'           => round($statline->total_scouts_killed_per_minute, 2),
            'total_soldiers_killed_per_minute'         => round($statline->total_soldiers_killed_per_minute, 2),
            'total_pyros_killed_per_minute'            => round($statline->total_pyros_killed_per_minute, 2),
            'total_demomen_killed_per_minute'          => round($statline->total_demomen_killed_per_minute, 2),
            'total_heavies_killed_per_minute'          => round($statline->total_heavies_killed_per_minute, 2),
            'total_engineers_killed_per_minute'        => round($statline->total_engineers_killed_per_minute, 2),
            'total_medics_killed_per_minute'           => round($statline->total_medics_killed_per_minute, 2),
            'total_snipers_killed_per_minute'          => round($statline->total_snipers_killed_per_minute, 2),
            'total_spies_killed_per_minute'            => round($statline->total_spies_killed_per_minute, 2),
            'total_deaths_to_scouts'                   => round($statline->total_deaths_to_scouts, 2),
            'total_deaths_to_soldiers'                 => round($statline->total_deaths_to_soldiers, 2),
            'total_deaths_to_pyros'                    => round($statline->total_deaths_to_pyros, 2),
            'total_deaths_to_demomen'                  => round($statline->total_deaths_to_demomen, 2),
            'total_deaths_to_heavies'                  => round($statline->total_deaths_to_heavies, 2),
            'total_deaths_to_engineers'                => round($statline->total_deaths_to_engineers, 2),
            'total_deaths_to_medics'                   => round($statline->total_deaths_to_medics, 2),
            'total_deaths_to_snipers'                  => round($statline->total_deaths_to_snipers, 2),
            'total_deaths_to_spies'                    => round($statline->total_deaths_to_spies, 2),
            'total_dominations'                        => round($statline->total_dominations, 2),
            'total_revenges'                           => round($statline->total_revenges, 2),
        ]);

        return inertia('CollectionDetail', [
            'collection'     => new CollectionResource($collection),
            'data'           => $data,
            'weapons'        => $weapons,
            'heal_spreads'   => $healSpreads,
            'played_classes' => $data->pluck('most_played_class')
        ]);
    }

    public function export(Collection $collection): \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return (new CombinedExport($collection))->download($collection->slug . '.xlsx');
    }
}
