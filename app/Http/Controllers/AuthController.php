<?php

namespace App\Http\Controllers;

use App\Repositories\Leagues\Euro\ETF2LRepo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Invisnik\LaravelSteamAuth\SteamAuth;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use xPaw\Steam\SteamID;

class AuthController extends Controller
{
    /**
     * The SteamAuth instance.
     *
     * @var SteamAuth
     */
    protected $steam;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectURL = '/';

    /**
     * AuthController constructor.
     *
     * @param SteamAuth $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    /**
     * Redirect the user to the authentication page
     *
     * @return RedirectResponse
     */
    public function redirectToSteam(): RedirectResponse
    {
        return $this->steam->redirect();
    }

    /**
     * Get user info and log in
     *
     * @return RedirectResponse|Redirector
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                $user = $this->findOrNewUser($info);

                Auth::login($user, true);

                return redirect($this->redirectURL); // redirect to site
            }
        }
        return $this->redirectToSteam();
    }

    /**
     * Getting user by info or created if not exists
     *
     * @param $info
     * @return User
     */
    protected function findOrNewUser($info): User
    {
        $user = User::where('id', $info->steamID64)
                    ->first();

        $steamId = new SteamID($info->steamID64);

        $parameters = [
            'id'                           => $info->steamID64,
            'steamid2'                     => $steamId->RenderSteam2(),
            'steamid3'                     => $steamId->RenderSteam3(),
            'steamid64'                    => $info->steamID64,
            'name'                         => $info->personaname,
            'password'                     => bcrypt($info->steamID64),
            'steam_profile_picture_small'  => $info->avatar,
            'steam_profile_picture_medium' => $info->avatarmedium,
            'steam_profile_picture_big'    => $info->avatarfull,
        ];

        $etf2l = (new ETF2LRepo())->getUserInfo($info->steamID64);

        if (isset($etf2l->player->name)) {
            $parameters['name'] = $etf2l->player->name;
        }

        if ($user instanceof User) {
            return $user;
        }

        return User::create(
            $parameters
        );
    }
}