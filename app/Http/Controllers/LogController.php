<?php

namespace App\Http\Controllers;

use App\Http\Resources\LogDetail;
use App\Models\Filters\LogFilter;
use App\Models\Log;
use App\Models\Vod;
use App\Models\Weapon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Resources\LogCollection;
use App\Http\Resources\Log as LogResource;

class LogController extends Controller
{
    public function index(Request $request)
    {
        $logs = $this->getLogs($request);
        return inertia('Logs', [
            'logs'    => $logs,
            'weapons' => Weapon::select('name')
                               ->orderBy('name')
                               ->get()
        ]);
    }

    public function show(Log $log): \Inertia\Response|\Inertia\ResponseFactory
    {
        $log->load(
            [
                'demos'                     => function ($q) {
                    $q->select('demostf_id', 'map', 'download_url', 'server', 'red_name', 'blu_name');
                },
                'switches'                  => function ($q) {
                    $q->select('id', 'log_id', 'steam_id', 'time', 'when', 'name', 'team', 'tf_class');
                },
                'switches.steam'            => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'healSpreads'               => function ($q) {
                    $q->select('id', 'log_id', 'healer_steam_id', 'target_steam_id', 'heal_amount', 'tf_class');
                },
                'healSpreads.healerSteam'   => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'healSpreads.targetSteam'   => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'spawns'                    => function ($q) {
                    $q->select('id', 'log_id', 'steam_id', 'name', 'spawned_at', 'time', 'tf_class', 'team');
                },
                'spawns.steam'              => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'disconnects'               => function ($q) {
                    $q->select('steam_id', 'log_id', 'name', 'team', 'time', 'when', 'reason');
                },
                'disconnects.steam'         => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'dominations'               => function ($q) {
                    $q->select('id', 'log_id', 'steam_id', 'team', 'name', 'target_steam_id', 'time', 'target_team', 'target_name');
                },
                'dominations.steam'         => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'dominations.targetSteam'   => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'revenges'                  => function ($q) {
                    $q->select('id', 'log_id', 'steam_id', 'team', 'name', 'target_steam_id', 'time', 'target_team', 'target_name');
                },
                'revenges.steam'            => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'revenges.targetSteam'      => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'statlines.steam'           => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'statlines',
                'statlines.weapons'         => function ($q) {
                    $q->select('id', 'log_id', 'steam_id', 'player_statline_id', 'name', 'tf_class', 'kills', 'damage', 'damage_per_minute',
                               'shots', 'hits', 'accuracy');
                },
                'statlines.weapons.steam'   => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'killstreaks'               => function ($q) {
                    $q->select('id', 'log_id', 'name', 'steam_id', 'streak', 'tick');
                },
                'killstreaks.steam'         => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'chats'                     => function ($q) {
                    $q->select('id', 'log_id', 'name', 'steam_id', 'message');
                },
                'chats.steam'               => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'rounds'                    => function ($q) {
                    $q->select('id', 'log_id', 'started_at', 'stopped_at', 'duration', 'first_cap', 'winner', 'red_score',
                               'red_kills', 'red_damage', 'red_charges', 'blu_score', 'blu_kills', 'blu_damage', 'blu_charges');
                },
                'rounds.players'            => function ($q) {
                    $q->select('id', 'log_id', 'round_id', 'steam_id', 'kills', 'damage', 'team')
                      ->orderBy('kills', 'desc');
                },
                'rounds.events'             => function ($q) {
                    $q->select('id', 'log_id', 'steam_id', 'round_id', 'killer_steam_id', 'tick', 'type', 'team', 'point', 'medigun');
                },
                'rounds.players.steam'      => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'rounds.events.steam'       => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'rounds.events.killerSteam' => function ($q) {
                    $q->select('id', 'name', 'steam_profile_picture_medium');
                },
                'leagueMatches'
            ]);

        $vods = $log->leagueMatches->count() > 0
            ? Vod::select('provider', 'url', 'channel', 'title')
                 ->whereIn('league_match_id', $log->leagueMatches->pluck('id')->toArray())
                 ->get()
            : [];

        return inertia('LogDetail', [
            'log'   => new LogDetail($log),
            'vods'  => $vods,
            'demos' => $log->demos->map(fn ($demo) => [
                'demostf_id'  => $demo->demostf_id,
                'demostf_url' => 'https://demos.tf/' . $demo->demostf_id,
                'map'         => $demo->map,
                'server'      => $demo->server,
                'title'       => $demo->red_name . ' vs. ' . $demo->blu_name
            ])
        ]);
    }

    private function getLogs(Request $request): LogCollection
    {
        $logs = Log::select('id', 'title', 'map', 'date', 'duration')
                   ->withCount('statlines')
                   ->filter(new LogFilter($request))
                   ->orderBy('date', 'desc')
                   ->paginate($request->get('limit', 50))
                   ->withQueryString();

        return new LogCollection($logs);
    }

}
