<?php

namespace App\Http\Controllers;

use App\Http\Resources\TeamCollection;
use App\Models\Filters\TeamFilter;
use App\Models\Format;
use App\Models\LeagueMatch;
use App\Models\Team;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Resources\Team as TeamResource;

class TeamController extends Controller
{
    public function index(Request $request): \Inertia\Response|\Inertia\ResponseFactory
    {
        $teams = $this->getTeams($request);
        return inertia('Teams', [
            'teams'   => $teams,
            'formats' => [
                [
                    'id'   => 1,
                    'name' => 'Highlander'
                ],
                [
                    'id'   => 2,
                    'name' => '6on6'
                ]
            ],
        ]);
    }

    public function show(Team $team)
    {
        $matches = LeagueMatch::with('collection')
                              ->where(fn (Builder $q) => $q->where('home_team_id', $team->id)->orWhere('away_team_id', $team->id))
                              ->orderByDesc('scheduled_date')
                              ->get();
        $team->load(['players' => function ($q) {
            $q->distinct()->orderBy('name');
        }]);
        return inertia('TeamDetail', [
            'team'        => new TeamResource($team),
            'match_count' => $matches->count(),
            'matches'     => $matches->mapToGroups(fn ($match) => [$match->collection->name => [
                'id'               => $match->id,
                'name'             => $match->name,
                'home_team_score'  => $match->home_team_score,
                'away_team_score'  => $match->away_team_score,
                'home_team_name'   => $match->home_team_name,
                'away_team_name'   => $match->away_team_name,
                'home_team_id'     => $match->home_team_id,
                'away_team_id'     => $match->away_team_id,
                'maps_played'      => implode(', ', $match->maps_played),
                'division'         => $match->division,
                'week'             => $match->week,
                'league_match_url' => $match->league_match_url,
                'scheduled_date'   => !empty($match->scheduled_date) ? $match->scheduled_date->diffForHumans() : null,
                'won'              => $this->determineWinner($match, $team)
            ]])
        ]);
    }

    private function determineWinner(LeagueMatch $leagueMatch, Team $team): string
    {
        return match (true) {
            $team->id === $leagueMatch->home_team_id => match (true) {
                $leagueMatch->home_team_score > $leagueMatch->away_team_score => 'Won',
                $leagueMatch->home_team_score < $leagueMatch->away_team_score => 'Lost',
                default => 'Tie'
            },
            $team->id === $leagueMatch->away_team_id => match (true) {
                $leagueMatch->home_team_score < $leagueMatch->away_team_score => 'Won',
                $leagueMatch->home_team_score > $leagueMatch->away_team_score => 'Lost',
                default => 'Tie'
            },
            default => '?'
        };
    }

    private function getTeams(Request $request): TeamCollection
    {
        return new TeamCollection(
            Team::filter(new TeamFilter($request))
                ->orderBy('name', 'asc')
                ->paginate($request->get('limit', 50))
                ->withQueryString()
        );
    }
}
