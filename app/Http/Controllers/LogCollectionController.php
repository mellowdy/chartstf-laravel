<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogCollectionController extends Controller
{
    public function index()
    {
        return inertia('LogCollections');
    }
}
