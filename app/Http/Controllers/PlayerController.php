<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlayerCollection;
use App\Http\Resources\PlayerDetail;
use App\Models\Demo;
use App\Models\Filters\UserFilter;
use App\Models\Log;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlayerController extends Controller
{
    public function index(Request $request): \Inertia\Response|\Inertia\ResponseFactory
    {
        $players = $this->getPlayers($request);
        return inertia('Players', [
            'players' => $players,
        ]);
    }

    private function getPlayers(Request $request): PlayerCollection
    {
        $players = User::select('id', 'name', 'steam_profile_picture_medium', 'steamid2', 'steamid3')
                       ->filter(new UserFilter($request))
                       ->orderBy('name')
                       ->paginate($request->get('limit', 50))
                       ->withQueryString();

        return new PlayerCollection($players);
    }

    public function show(User $user)
    {
        $user->load(['leagueMatches' => function (HasManyThrough $q) {
            $q->orderByDesc('scheduled_date');
        }, 'leagueMatches.collection:id,name', 'leagueMatches.homeTeam:id,name,tag', 'leagueMatches.awayTeam:id,name,tag']);

        $demos = Demo::select('map', 'server', 'date', 'demostf_id', 'blu_name', 'red_name')
                     ->whereHas('logs.statlines', function ($q) use ($user) {
                         $q->where('steam_id', $user->id);
                     })
                     ->orderByDesc('date')
                     ->get()
                     ->map(fn ($demo) => [
                         'map'         => $demo->map,
                         'date'        => !empty($demo->date) ? $demo->date->format('M d, Y g:i A') : null,
                         'server'      => $demo->server,
                         'demostf_id'  => $demo->demostf_id,
                         'demostf_url' => 'https://demos.tf/' . $demo->demostf_id,
                         'title'       => $demo->blu_name . ' vs. ' . $demo->red_name
                     ]);

        return inertia('PlayerDetail', [
            'player'              => new PlayerDetail($user),
            'performance_metrics' => DB::select(file_get_contents(app_path('Queries' . DIRECTORY_SEPARATOR . 'player-metrics.sql')), [
                'id' => $user->id
            ])[0] ?? null,
            'demos'               => $demos,
            'logs'                => Log::select('id', 'map', 'title', 'date')
                                        ->whereHas('statlines', function ($q) use ($user) {
                                            $q->where('steam_id', $user->id);
                                        })
                                        ->take(10)
                                        ->orderByDesc('date')
                                        ->get()
                                        ->map(fn ($item) => [
                                            'id'     => $item->id,
                                            'url'    => "https://logs.tf/" . $item->id,
                                            'map'    => $item->map,
                                            'title'  => $item->title,
                                            'date_h' => $item->date->diffForHumans()
                                        ]),
            'matches'             => $user->leagueMatches->mapToGroups(fn ($match) => [$match->collection->name => [
                'id'               => $match->id,
                'name'             => $match->name,
                'home_team_score'  => $match->home_team_score,
                'away_team_score'  => $match->away_team_score,
                'home_team_tag'    => optional($match->homeTeam)->tag,
                'away_team_tag'    => optional($match->awayTeam)->tag,
                'home_team_name'   => $match->home_team_name,
                'away_team_name'   => $match->away_team_name,
                'home_team_id'     => $match->home_team_id,
                'away_team_id'     => $match->away_team_id,
                'maps_played'      => implode(', ', $match->maps_played),
                'division'         => $match->division,
                'week'             => $match->week,
                'league_match_url' => $match->league_match_url,
                'scheduled_date'   => !empty($match->scheduled_date) ? $match->scheduled_date->diffForHumans() : null
            ]]),
            'teams'               => Team::select('id', 'name', 'league', 'gamemode', 'team_picture')
                                         ->whereHas('competitionParticipations', function (Builder $q) use ($user) {
                                             $q->where('steam_id', $user->id);
                                         })
                                         ->orderBy('name')
                                         ->get()
        ]);
    }
}
