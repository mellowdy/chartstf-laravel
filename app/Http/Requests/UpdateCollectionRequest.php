<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|min:3|max:190',
            'logs'       => 'required|array|min:1|max:100',
            'league'     => 'in:ETF2L,RGL,ozfortress|nullable',
            'is_private' => 'boolean',
            'gamemode'   => 'required|in:Highlander,6v6,4v4,2v2,BBall,Prolander'
        ];
    }
}
