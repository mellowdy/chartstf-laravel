<?php

namespace App\Observers;

use App\Models\Collection;
use Illuminate\Support\Str;

class CollectionObserver
{
    /**
     * Handle the Collection "created" event.
     *
     * @param Collection $collection
     * @return void
     */
    public function creating(Collection $collection)
    {
        $collection->slug = Str::slug($collection->name, '-');
        if (auth()->check()) {
            $collection->steam_id = auth()->user()->id;
        }
    }

    /**
     * Handle the Collection "updated" event.
     *
     * @param Collection $collection
     * @return void
     */
    public function updating(Collection $collection)
    {
        $collection->slug = Str::slug($collection->name, '-');
    }
}
