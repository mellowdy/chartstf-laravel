<?php

namespace App\Imports;

use App\Models\LeagueMatch;
use App\Models\Vod;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use Symfony\Component\Console\Output\ConsoleOutput;

class KritzkastImport implements OnEachRow, WithHeadingRow
{
    use Importable;

    protected ConsoleOutput $consoleOutput;

    public function __construct()
    {
        $this->consoleOutput = new ConsoleOutput();
    }

    public function onRow(Row $row)
    {
        if ($row['League'] === 'ETF2L') {
            $matchId = Str::replace(['https://etf2l.org/matches/', 'http://etf2l.org/matches/', '/'], ['', '', ''], $row['Match']);
            if (is_numeric($matchId)) {
                $this->consoleOutput->writeln("Match ID: " . $matchId);
                $match = LeagueMatch::where('league_match_id', $matchId)->first();

                if ($match instanceof LeagueMatch) {
                    $provider = match (true) {
                        Str::contains(strtolower($row['Link']), 'youtu') => 'YouTube',
                        Str::contains(strtolower($row['Link']), 'twitch') => 'Twitch'
                    };

                    Vod::updateOrCreate(
                        [
                            'league_match_id' => $match->id,
                        ],
                        [
                            'provider'    => $provider,
                            'url'         => $row['Link'],
                            'match_page'  => $row['Match'],
                            'season'      => $row['Season'],
                            'title'       => $row['Teams'],
                            'channel'     => 'KritzKast',
                            'channel_url' => 'https://twitch.tv/kritzkast'
                        ]
                    );
                }
            }
        }
    }
}
