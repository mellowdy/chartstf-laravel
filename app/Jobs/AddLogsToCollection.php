<?php

namespace App\Jobs;

use App\Models\Collection;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddLogsToCollection implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public int $timeout = 3600;

    /**
     * @var LogsRepo $logsRepo
     */
    protected LogsRepo $logsRepo;

    protected object $request;

    protected Collection $collection;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(object $request, $id)
    {
        $this->logsRepo   = new LogsRepo();
        $this->collection = Collection::find($id);
        $this->request    = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $logs = collect(preg_split('/\r\n|\r|\n/', $this->request->logs))
            ->unique()
            ->values()
            ->toArray();

        $this->logsRepo->setNameResolution($this->collection->league);

        foreach ($logs as $log) {
            $log = $this->logsRepo->parseLogByUrl($log);
            try {
                $this->collection->logs()->attach($log);
            } catch (\Exception $ex) {
                logger()->error('An error occurred: ' . $ex->getMessage());
            }
        }

        $this->collection->update(['is_private' => $this->request->is_private]);
    }
}
