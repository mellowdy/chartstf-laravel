<?php

namespace App\Services\LogRetrieval;

use App\Models\User;
use App\Repositories\Leagues\Euro\ETF2LRepo;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class ETF2LLogRetrievalService
{
    protected ConsoleOutput $consoleOutput;
    protected LogsRepo $logsRepo;
    protected ETF2LRepo $etf2lRepo;

    // To protect against me getting IP banned for spamming the API's & pages. :)
    protected int $delayBetweenRequests = 3;
    protected bool $rateLimiting = true;

    protected array $notFound = [];

    public function __construct()
    {
        $this->consoleOutput        = new ConsoleOutput();
        $this->etf2lRepo            = new ETF2LRepo();
        $this->logsRepo             = new LogsRepo();
        $this->delayBetweenRequests = 3;
        $this->rateLimiting         = true;
        $this->notFound             = [];
    }

    private function waitAWhile()
    {
        if ($this->rateLimiting) {
            sleep($this->delayBetweenRequests);
        }
    }

    public function getNotFoundLogs(): array
    {
        return $this->notFound;
    }

    public function setNotFoundLogs(array $notFound = [])
    {
        $this->notFound = $notFound;
    }

    /**
     * @throws \Exception
     */
    public function getLogsFromCompetition(int $competitionId, $division = 'Premiership')
    {
        $matches = $this->etf2lRepo->scrapeMatchesFromCompetition($competitionId, $division);

        $this->waitAWhile();

        $this->consoleOutput->writeln("These are the Match URL's for this competition: " . collect($matches)->pluck('match_url')->join(', '));

        $allLogs = [];

        foreach ($matches as $match) {
            $this->consoleOutput->writeln("Fetching detailed match information from match " . $match->match_url . ", played on " . $match->match_date);

            $matchInformation = $this->etf2lRepo->scrapeMatchInformation($match->match_id);
            $this->waitAWhile();

            if (!Str::contains($match->match_date, 'Not Scheduled')) {
                $matchDate = Carbon::createFromFormat('d-m-Y H:i:s', $match->match_date)->format('Y-m-d H:i:s');

                $this->consoleOutput->writeln("Found match for teams " . collect($matchInformation->teams)->pluck('team_name')->join(' vs. ') . ". Maps played: " . collect($matchInformation->maps_played)->join(', '));
                $this->consoleOutput->writeln("Match date: " . $matchDate);
                $this->consoleOutput->writeln("Proceeding to get Steam ID for ETF2L users.");
                $this->consoleOutput->writeln("Participating players in this competition are: ");

                $steamIds = [];
                $steamId = null;
                foreach ($matchInformation->players as $player) {

                    // Check if we already have a user with that ID in our system.
                    // That way, we do not need to spam the ETF2L API for this information (since we already have it).
                    $user = User::where('etf2l_id', $player->player_id)->first();

                    // If we do not have it, then we will unfortunately need to fetch it from ETF2L API.
                    if (!$user instanceof User) {
                        // While the current function says 'steam id', player ID's work too.
                        $etf2lInfo = $this->etf2lRepo->getUserInfo($player->player_id);

                        $this->waitAWhile();

                        //$steamInfo = $this->logsRepo->getSteamInfoBySteamId([]);

                        $user = User::updateOrCreate(
                            [
                                'id' => $etf2lInfo->player->steam->id64,
                            ],
                            [
                                'id'                           => $etf2lInfo->player->steam->id64,
                                'etf2l_id'                     => $etf2lInfo->player->id,
                                'steamid2'                     => $etf2lInfo->player->steam->id,
                                'steamid3'                     => '[' . $etf2lInfo->player->steam->id3 . ']',
                                'steamid64'                    => $etf2lInfo->player->steam->id64,
                                'name'                         => $etf2lInfo->player->name,
                                'password'                     => bcrypt($etf2lInfo->player->steam->id64),
                                'steam_profile_picture_small'  => $etf2lInfo->player->steam->avatar,
                                'steam_profile_picture_medium' => $etf2lInfo->player->steam->avatar,
                                'steam_profile_picture_big'    => $etf2lInfo->player->steam->avatar,
                            ]
                        );
                        $steamId = $etf2lInfo->player->steam->id64;
                    } else {
                        $steamId = $user->id;
                    }

                    $steamIds[] = $steamId;
                    $this->consoleOutput->writeln("- " . $user->name . "($steamId)");
                }

                $this->consoleOutput->writeln("Successfully retrieved the Steam ID's of the players through the ETF2L API: " . implode(',', $steamIds));

                $this->consoleOutput->writeln("Beginning log search for this match...");

                foreach ($matchInformation->maps_played as $mapName) {
                    $this->consoleOutput->writeln("Logs on the following map: " . $mapName);

                    if (count($steamIds) > 0) {
                        // prevent version mismatches by searching for ex. cp_steel instead of cp_steel_f6 (teams tend to play the wrong versions)
                        $mapToSearch = Str::substrCount($mapName, '_') >= 2 ? Str::beforeLast($mapName, '_') : $mapName;

                        $logs = $this->logsRepo->findLogsBySteamIds($steamIds, $mapToSearch, Carbon::createFromFormat('d-m-Y H:i:s', $match->match_date)->startOfDay());

                        if (count($logs) === 0) {
                            $this->consoleOutput->writeln("!!! No logs were found for " . $mapName . " !!!");
                            $this->notFound[] = (object)[
                                'match_url'  => $match->match_url,
                                'map'        => $mapName,
                                'match_date' => $matchDate
                            ];
                        } else {
                            $this->consoleOutput->writeln("Following logs were found: ");

                            foreach ($logs as $log) {
                                $this->consoleOutput->writeln("$log->url | $log->title | $log->map | " . $log->date->format('Y-m-d H:i:s'));
                            }
                        }

                        $this->waitAWhile();

                        foreach ($logs as $log) {
                            $allLogs[] = $log;
                        }
                    } else {
                        $this->consoleOutput->writeln("No Steam ID's found on this page. We assume that this match was defaulted. $match->match_url");
                    }
                }
            }
        }

        return $allLogs;
    }
}