SELECT
    users.name AS common_name,
    player_weapons.steam_id AS steam_id,
    player_weapons.name AS weapon_name,
    player_weapons.tf_class AS class_played,
    SUM(player_weapons.kills) AS kills,
    SUM(player_weapons.damage) AS damage,
    SUM(player_weapons.damage_per_minute) AS damage_per_minute,
    SUM(player_weapons.shots) AS shots,
    SUM(player_weapons.hits) AS hits,
    AVG(player_weapons.accuracy) AS accuracy
FROM
    player_weapons
        LEFT JOIN
    users ON users.id = player_weapons.steam_id
        LEFT JOIN
    logs ON logs.id = player_weapons.log_id
GROUP BY player_weapons.steam_id , player_weapons.tf_class , player_weapons.name
ORDER BY users.name;