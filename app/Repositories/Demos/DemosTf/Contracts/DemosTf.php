<?php

namespace App\Repositories\Demos\DemosTf\Contracts;

use App\Models\Log;
use Carbon\Carbon;

interface DemosTf
{
    public function searchDemosByLog(Log $log);

    public function searchDemos(string $map, Carbon $before, Carbon $after, array $players = []);
}