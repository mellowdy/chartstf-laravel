<?php

namespace App\Repositories\Demos\DemosTf;

use App\Models\Log;
use App\Repositories\Demos\DemosTf\Contracts\DemosTf;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Console\Output\ConsoleOutput;

class DemosTfRepo implements DemosTf
{
    protected ConsoleOutput $consoleOutput;

    public function __construct()
    {
        $this->consoleOutput = new ConsoleOutput();
    }

    public function searchDemosByLog(Log $log): array
    {
        $log->loadMissing(['statlines' => function ($q) {
            $q->select('id', 'steam_id', 'log_id');
        }]);

        if (blank($log->map)) {
            return [];
        }

        // we'll take a small margin on the times we registered cause
        // 1. the upload is not instant
        // 2. logs might be uploaded later

        return $this->searchDemos(
            $log->map,
            $log->started_at->subMinutes(10),
            $log->stopped_at->addMinutes(10),
            $log->statlines
                ->pluck('steam_id')
                ->unique()
                ->values()
                ->toArray()
        );
    }

    public function searchDemos(string $map, Carbon $before, Carbon $after, array $players = []): array
    {
        // Contains players who participated, the map it was played on, the time in between the games were played.
        // '&before=' . $before->timestamp . '&after=' . $after->timestamp
        // this date filter does not seem to work on the end of demos.tf unless I am doing something wrong.
        $url = config('demostf.api.base') . config('demostf.api.demos') . '?players=' . implode(',', $players) . '&map=' . $map;
        $this->consoleOutput->writeln("Looking for demos at " . $url);
        return collect(
            Http::withHeaders(
                [
                    'Content-Type' => 'application/json',
                    'Accept'       => 'application/json'
                ]
            )
                ->get($url)
                ->json()
        )->where('time', '>', $before->timestamp)
         ->where('time', '<', $after->timestamp)
         ->values()
         ->toArray();
    }
}