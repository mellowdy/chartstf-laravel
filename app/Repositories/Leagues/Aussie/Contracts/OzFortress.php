<?php

namespace App\Repositories\Leagues\Aussie\Contracts;

interface OzFortress
{
    public function getUserInfoBySteamId(int $steamId64): object;

    public function getUserInfoByOzFortressId(int $ozFortressId): object;

    public function getLeagueMatchesForSeason(int $seasonId, string $division = 'Premier');
}