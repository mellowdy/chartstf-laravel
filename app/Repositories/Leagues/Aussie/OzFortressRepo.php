<?php

namespace App\Repositories\Leagues\Aussie;

use App\Repositories\Leagues\Aussie\Contracts\OzFortress;
use Goutte\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class OzFortressRepo implements OzFortress
{
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getUserInfoBySteamId(int $steamId64): object
    {
        // We should be able to resolve the user through his Steam ID this way.
        // However it is probably good practice to keep track of the ozfortress player ID.
        // It'll help us not have to scrape as much of their pages once most top Australian players are in the system.
        $url  = config('ozfortress.web.base') . config('ozfortress.web.player') . $steamId64;
        $page = $this->client->request('GET', $url);

        return (object)[
            'steam_id'      => $steamId64,
            'name'          => $page->filter('.spaced-letters')->first()->text(),
            'url'           => $page->getUri(),
            'ozfortress_id' => Str::afterLast($page->getUri(), '/')
        ];
    }

    public function getUserInfoByOzFortressId(int $ozFortressId): object
    {
        // We should be able to resolve the user through his OzFortress ID this way.
        $url  = config('ozfortress.web.base') . config('ozfortress.web.player') . $ozFortressId;
        $page = $this->client->request('GET', $url);

        return (object)[
            'ozfortress_id' => $ozFortressId,
            'name'          => $page->filter('.spaced-letters')->first()->text(),
            'steam_id'      => $page->filter('.steam-link')->filter('span')->last()->text(),
            'url'           => $page->getUri()
        ];
    }

    public function getLeagueMatchesForSeason(int $seasonId, string $division = 'Premier')
    {
        $url  = config('ozfortress.web.base') . config('ozfortress.web.leagues') . $seasonId . config('ozfortress.web.matches');
        $page = $this->client->request('GET', $url);

        $matches = $page->filter('.table-responsive')
                        ->filter('.matches-table')
                        ->filter('tr')
                        ->each(fn (Crawler $match) => (object)[
                            //'division'  => $match->closest('tr')->closest('.matches-table')->closest('.table-responsive')->closest('h3')->text(),
                            'round'     => $match->filter('.name')->text(),
                            'home_team' => [
                                'id'   => $match->filter('.home-team')->filter('a')->count() > 0 ? Str::afterLast($match->filter('.home-team')->filter('a')->attr('href'), '/') : null,
                                'name' => $match->filter('.home-team')->filter('a')->count() > 0 ? $match->filter('.home-team')->filter('a')->text() : null
                            ],
                            'bye_week'  => $match->filter('.vs')->text() === 'BYE',
                            'match_id'  => Str::afterLast($match->filter('.match-page')->filter('a')->attr('href'), '/'),
                            'match_url' => config('ozfortress.web.base') . $match->filter('.match-page')->filter('a')->attr('href'),
                            'away_team' => [
                                'id'   => $match->filter('.away-team')->filter('a')->count() > 0 ? Str::afterLast($match->filter('.away-team')->filter('a')->attr('href'), '/') : null,
                                'name' => $match->filter('.away-team')->filter('a')->count() > 0 ? $match->filter('.away-team')->filter('a')->text() : null
                            ]
                        ]);
        return $matches;
    }
}