<?php

namespace App\Repositories\Leagues\Burger;

use App\Models\User;
use App\Repositories\Leagues\Burger\Contracts\RGL;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;
use xPaw\Steam\SteamID;

class RGLRepo implements RGL
{
    protected Client $client;
    protected ConsoleOutput $consoleOutput;
    protected int $delay = 1;
    protected bool $rateLimiting = true;
    protected LogsRepo $logsRepo;
    protected array $notFound = [];

    public function __construct()
    {
        $this->client        = new Client();
        $this->consoleOutput = new ConsoleOutput();
        $this->logsRepo      = new LogsRepo();
        $this->notFound      = [];
    }

    private function waitAWhile()
    {
        if ($this->rateLimiting) {
            sleep($this->delay);
        }
    }

    public function getNotFoundLogs(): array
    {
        return $this->notFound;
    }

    /**
     * @throws \Exception
     */
    public function scrapeMatchesPlayedBySeason(int $season, string $division = ''): array
    {
        $url = config('rgl.base_url') . config('rgl.season_page') . "?s=$season&r=24";
        $this->consoleOutput->writeln("Checking teams on following URL: " . $url);
        $teamColumn = '4';
        $teams      = collect(
            $this->client->request('GET', $url)
                         ->filter('.table-responsive')
                         ->filter('.table-striped')
                         ->filter('tr:nth-child(n + 2)')
                         ->filter('td:nth-child(' . $teamColumn . ') > a')
                         ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                             'division' => $item->closest('tr')
                                                ->closest('table')
                                                ->closest('.table-responsive')
                                                ->previousAll()
                                                ->filter('h3')
                                                ->text(),
                             'team'     => $item->text(),
                             'team_url' => 'https://rgl.gg/Public/' . $item->attr('href'),
                             'team_id'  => $this->getTeamIdFromUrl($item->attr('href'))
                         ])
        )->filter(fn ($item) => Str::contains($item->division, $division) && !Str::contains($item->team, ['Free Agent']))
         ->values()
         ->toArray();

        foreach ($teams as $team) {
            $this->consoleOutput->writeln("Team found: " . $team->team);
        }

        //$this->consoleOutput->writeln("Teams: " . json_encode($teams, JSON_PRETTY_PRINT));

        $totalMatches = [];
        $teamNames    = [];
        $teamPlayers  = [];

        foreach ($teams as $team) {
            $this->consoleOutput->writeln("Getting match info for " . $team->team);
            $teamData                    = $this->scrapeTeamInfoByTeamId($team->team_id);
            $totalMatches[]              = $teamData->matches;
            $teamNames[$team->team_id]   = $teamData->team;
            $teamPlayers[$team->team_id] = $teamData->rostered_players;
            //$this->consoleOutput->writeln("Team data: " . json_encode($teamData, JSON_PRETTY_PRINT));
            $this->waitAWhile();
        }

        $totalMatches = collect($totalMatches)
            ->flatten()
            ->unique('match_id')
            ->values()
            ->filter(fn ($item) => !Str::contains(strtolower($item->match), ['playoffs', 'finals', 'qualifier', 'third place']))
            ->values();

        $totalLogs = [];

        foreach ($totalMatches as $match) {
            $this->consoleOutput->writeln("Looking for logs for match " . $match->match . " at " . $match->match_url . " at about " . $match->match_date_text . " (" . $match->match_date->format('Y-m-d H:i:s') . ")");
            $matchData = $this->scrapeMatchById($match->match_id);

            foreach ($matchData->maps as $map) {
                $playersToPick = 1;
                $steamIds      = collect(collect($teamPlayers[$matchData->teams->home->id])->take($playersToPick)->toArray())
                    ->merge(collect($teamPlayers[$matchData->teams->away->id])->take($playersToPick)->toArray())
                    ->values()
                    ->pluck('steam_id')
                    ->filter(fn ($steamId) => !in_array($steamId, config('rgl.players.ignored'), false))
                    ->values()
                    ->toArray();
                // in order to remove mismatch of versions, use the base map
                // ex. use cp_steel instead of cp_steel_f6a whatever
                $mapToSearch = Str::substrCount($map, '_') >= 2 ? Str::beforeLast($map, '_') : $map;
                $logs        = collect($this->logsRepo->findLogsBySteamIds($steamIds, $mapToSearch, $match->match_date))->map(function ($log) use ($match) {
                    $log->match = $match;
                    return $log;
                });

                if (count($logs) === 0) {
                    if ($map !== 'Forfeit') {
                        $this->notFound[] = (object)[
                            'map'     => $map,
                            'match'   => $match,
                            'details' => $matchData
                        ];
                    }
                } else {
                    $totalLogs[] = $logs;

                    $this->consoleOutput->writeln("Following logs were found: ");

                    foreach ($logs as $log) {
                        $this->consoleOutput->writeln("$log->url | $log->title | $log->map | " . $log->date->format('Y-m-d H:i:s'));
                    }
                }

                $this->waitAWhile();
            }
        }

        return collect($totalLogs)
            ->flatten()
            ->toArray();
    }

    public function scrapeMatchById(int $matchId): object
    {
        $url  = config('rgl.base_url') . config('rgl.match_page') . "?m=$matchId&r=24";
        $page = $this->client->request('GET', $url);

        return (object)[
            'season'       => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_lblSeasonName')->text(),
            'match_date'   => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_lblScheduleDate')->text(),
            'default_date' => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_lblDefaultDate')->text(),
            'maps'         => collect($page->filter("div[class='col-md-12'] strong a")->each(fn ($item) => $item->text()))->filter(fn ($map) => !Str::contains($map, 'Forfeit'))->values()->toArray(),
            'teams'        => (object)[
                'home' => (object)[
                    'id'   => $this->getTeamIdFromUrl($page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_hlHomeTeamLinkFull')->attr('href')),
                    'name' => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_hlHomeTeamLinkFull')->text(),
                ],
                'away' => (object)[
                    'id'   => $this->getTeamIdFromUrl($page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_hlAwayTeamFull')->attr('href')),
                    'name' => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_Main_ggMatchInfo_hlAwayTeamFull')->text()
                ]
            ]
        ];
    }

    /**
     * Alias
     * @param string $steamId
     * @return User
     */
    public function scrapePlayerInfoBySteamId(string $steamId): object
    {
        $url = config('rgl.base_url') . config('rgl.player_page') . "?p=$steamId&r=24";

        $page = $this->client->request('GET', $url);

        $steamIdConversion = new SteamID($steamId);

        return (object)[
            'id'                           => $steamId,
            'name'                         => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_lblPlayerName')->text(),
            'alias'                        => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_lblPlayerName')->text(),
            'steamid64'                    => $steamId,
            'password'                     => bcrypt($steamId),
            'steamid2'                     => $steamIdConversion->RenderSteam2(),
            'steamid3'                     => $steamIdConversion->RenderSteam3(),
            'steam_profile_picture_small'  => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_imgProfileImage')->attr('src'),
            'steam_profile_picture_big'    => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_imgProfileImage')->attr('src'),
            'steam_profile_picture_medium' => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_imgProfileImage')->attr('src'),
        ];
    }

    public function scrapeTeamInfoByTeamId(int $teamId): object
    {
        $url     = config('rgl.base_url') . config('rgl.team_page') . "?t=$teamId&r=24";
        $page    = $this->client->request('GET', $url);
        $matches = $page
            ->filter('.col-lg-8')
            ->filter('.row')
            ->filter('.col-xs-12')
            ->filter('.table-responsive')
            ->filter('.table-striped')
            ->filter('tr')
            ->filter('td:first-child > a')
            ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                'match'           => $item->text(),
                'match_url'       => 'https://rgl.gg/Public/' . $item->attr('href'),
                'match_id'        => (int)Str::after($item->attr('href'), '?m='),
                'match_date_text' => $item->closest('td')->closest('tr')->filter('td:nth-child(3)')->text(),
                'match_date'      => Carbon::parse($item->closest('td')->closest('tr')->filter('td:nth-child(3)')->text())->setTimezone('Europe/Brussels')
            ]);

        $rosteredPlayers = $page
            ->filter('.col-md-12')
            ->filter('.row')
            ->filter('.col-lg-4')
            ->filter('.table-responsive')
            ->filter('.table-striped')
            ->filter('tr:not(:first-child):not(:last-child)')
            ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                'player'     => $item->filter('td:nth-child(2)')->text(),
                'player_url' => 'https://rgl.gg/Public/' . $item->filter('td:nth-child(2) > a')->last()->attr('href'),
                'steam_id'   => $this->getSteamIdFromUrl($item->filter('td:nth-child(2) > a')->last()->attr('href')),
                'joined'     => $item->filter('td:nth-child(3)')->text(),
                'left'       => Str::replace('Left On ', '', $item->attr('data-original-title')) ?? null
            ]);


        $mainSeasonMatches = collect($matches)
            ->filter(fn ($item) => !Str::contains(strtolower($item->match), ['playoffs', 'finals', 'qualifier', 'qualifer', 'third place']))
            ->values()
            ->toArray();

        return (object)[
            'team_id'          => $teamId,
            'team'             => $page->filter('#ContentPlaceHolder1_ContentPlaceHolder1_ContentPlaceHolder1_lblHeaderText')->text(),
            'matches'          => $mainSeasonMatches,
            'rostered_players' => $rosteredPlayers
        ];
    }

    public function getTeamIdFromUrl(string $url): int
    {
        return (int)Str::replace(['https://rgl.gg/Public/Team.aspx?t=', 'Team.aspx?t=', '&r=24'], ['', '', ''], $url);
    }

    public function getSteamIdFromUrl(string $url): string
    {
        return (string)Str::replace(['https://rgl.gg/Public/PlayerProfile.aspx?p=', 'PlayerProfile.aspx?p=', '&r=24'], ['', '', ''], $url);
    }

    public function getMatchIdFromUrl(string $url)
    {
        // TODO: Implement getMatchIdFromUrl() method.
    }
}