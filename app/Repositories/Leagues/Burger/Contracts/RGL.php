<?php

namespace App\Repositories\Leagues\Burger\Contracts;

interface RGL
{
    public function scrapeMatchesPlayedBySeason(int $season);
    public function scrapeMatchById(int $matchId);
    public function scrapePlayerInfoBySteamId(string $steamId);
    public function scrapeTeamInfoByTeamId(int $teamId);

    public function getTeamIdFromUrl(string $url);
    public function getSteamIdFromUrl(string $url);
    public function getMatchIdFromUrl(string $url);
}