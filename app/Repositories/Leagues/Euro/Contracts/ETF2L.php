<?php

namespace App\Repositories\Leagues\Euro\Contracts;

interface ETF2L
{
    public function getUserInfo(int $steamId64);

    public function scrapeArchive();

    public function getTeamInfo(int $teamId);
}