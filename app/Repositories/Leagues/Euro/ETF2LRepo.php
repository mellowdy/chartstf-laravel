<?php

namespace App\Repositories\Leagues\Euro;

use App\Repositories\Leagues\Euro\Contracts\ETF2L;
use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class ETF2LRepo implements ETF2L
{
    protected ConsoleOutput $consoleOutput;
    protected Client $client;
    protected int $scrapeDelay = 15;

    public function __construct()
    {
        $this->consoleOutput = new ConsoleOutput();
        $this->client        = new Client();
        $this->scrapeDelay   = 15;
    }

    /**
     * @param int $steamId64
     * @return object|null
     */
    public function getUserInfo(int $steamId64): ?object
    {
        $url = config('etf2l.api.base') . config('etf2l.api.player') . $steamId64;

        $response = Http::withHeaders(
            [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json'
            ]
        )->get($url);

        if ($response->successful()) {
            return $response->object();
        }

        return null;
    }

    /**
     * @param int $competitionId
     * @return array
     */
    public function scrapeAwardsFromCompetition(int $competitionId): array
    {
        return $this->client->request('GET', config('etf2l.web.base') . config('etf2l.web.awards') . $competitionId)
                            ->filter('.post > p')
                            ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => [
                                'classes'   => $item->extract(['class'])[0],
                                'value'     => $item->text(),
                                'placement' => count($item->filter('img')->extract(['alt'])) >= 1 ? $item->filter('img')->extract(['alt'])[0] : null,
                                'flag'      => count($item->filter('img')->extract(['alt'])) >= 2 ? $item->filter('img')->extract(['alt'])[1] : null,
                            ]);
    }

    /**
     * @param int $matchId
     * @return object
     */
    public function scrapeMatchInformation(int $matchId): object
    {
        $url = config('etf2l.web.base') . config('etf2l.web.matches') . $matchId;
        $this->consoleOutput->writeln("Fetch match information from " . $url);
        $page = $this->client->request('GET', $url);
        return (object)[
            'maps_played' => collect(
                $page->filter('.maps .map h2')
                     ->each(fn ($item) => $item->text())
            )
                ->unique()
                ->values()
                ->toArray(),
            'week'        => $page->filter('.post .c')->getNode(2)->textContent,
            'name'        => $page->filter(".result")
                                  ->text(),
            'teams'       => $page->filter('.result')
                                  ->filter('span')
                                  ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                                      'won'       => $item->attr('class') === 'winr',
                                      'team_name' => $item->text(),
                                      'team_url'  => $item->filter('a')->count() > 0 ? $item->filter('a')->attr('href') : null,
                                      'team_id'   => $item->filter('a')->count() > 0 ? (int) Str::afterLast($item->filter('a')->attr('href'), '/') : null
                                  ]),
            'players'     => $page->filter('.match-players')
                                  ->filter('tr')
                                  ->filter('td:nth-child(2)')
                                  ->filter('span')
                                  ->filter('a')
                                  ->each(fn ($item, $index) => (object)[
                                      'team_name'   => $item->closest('tr')
                                                            ->filter('td:first-child')
                                                            ->text() ?? null,
                                      'player_name' => $item->text(),
                                      'player_url'  => $item->attr('href'),
                                      'player_id'   => (int)Str::afterLast($item->attr('href'), '/')
                                  ])
        ];
    }

    public function getTeamInfo(int $teamId): ?object
    {
        $url = config('etf2l.api.base') . config('etf2l.api.team') . $teamId;

        $this->consoleOutput->writeln("Getting team info at " . $url);

        $response = Http::withHeaders(
            [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json'
            ]
        )->get($url);

        if ($response->successful()) {
            return $response->object();
        }

        return null;
    }

    /**
     * @param int $competitionId
     * @param string $division
     * @return array
     */
    public function scrapeMatchesFromCompetition(int $competitionId, string $division = 'Premiership'): array
    {
        $firstPage = $this->client->request('GET', config('etf2l.web.base') . config('etf2l.web.awards') . $competitionId . '/2');

        $weeks = collect($firstPage->filter('.post > .c > h4 > a')
                                   ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                                       'week' => $item->text(),
                                       'url'  => $item->attr('href')
                                   ]))
            ->filter(fn ($item) => Str::contains($item->week, 'Week'))
            ->map(fn ($item) => $item->url)
            ->toArray();

        $this->consoleOutput->writeln("Getting matches for " . implode(', ', $weeks));

        $firstWeek = collect(
            $firstPage->filter('.fix')
                      ->filter('tr')
                      ->filter('td:first-child')
                      ->filter('a')
                      ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                          'match_date' => trim($item->text()) . ':00',
                          'match_url'  => $item->attr('href'),
                          'division'   => $item->closest('td')->closest('tr')->closest('table')->previousAll()->text(),
                          'match_id'   => (int)Str::afterLast($item->attr('href'), '/')
                      ])
        )->filter(fn ($item) => Str::contains($item->division, $division) && !Str::contains($item->match_date, 'Not Scheduled'))
         ->values()
         ->toArray();

        $matchWeeks = [
            $firstWeek
        ];

        foreach ($weeks as $week) {
            $matchWeeks[] = collect(
                $this->client->request('GET', $week)
                             ->filter('.fix')
                             ->filter('tr')
                             ->filter('td:first-child')
                             ->filter('a')
                             ->each(fn (\Symfony\Component\DomCrawler\Crawler $item) => (object)[
                                 'match_date' => trim($item->text()) . ':00',
                                 'match_url'  => $item->attr('href'),
                                 'division'   => $item->closest('td')->closest('tr')->closest('table')->previousAll()->text(),
                                 'match_id'   => (int)Str::afterLast($item->attr('href'), '/')
                             ])
            )->filter(fn ($item) => Str::contains($item->division, $division) && !Str::contains($item->match_date, 'Not Scheduled'))
             ->values()
             ->toArray();;
        }

        return collect($matchWeeks)->flatten()->toArray();
    }

    /**
     * @return array
     */
    public function scrapeArchive(): array
    {
        return collect(
            $this->client->request('GET', config('etf2l.web.base') . config('etf2l.web.archive'))
                         ->filter('.wrap > ul > li > a')
                         ->each(fn ($item) => (object)[
                             'url'            => $item->attr('href'),
                             'title'          => $item->text(),
                             'competition_id' => (int)Str::afterLast($item->text(), '/')
                         ]))
            ->filter(fn ($item) => !Str::contains($item->title, 'Preseason Cup')
                                   && !Str::contains($item->title, 'MGE Cup')
                                   && !Str::contains($item->title, 'One Night Cup')
                                   && !Str::contains($item->title, 'Weekend Cup')
                                   && !Str::contains($item->title, 'Map Cup')
                                   && !Str::contains($item->title, 'Fun Team')
                                   && !Str::contains($item->title, 'One-Night-Cup')
                                   && !Str::contains($item->title, 'Playoffs')
                                   && !Str::contains($item->title, '1v1')
                                   && (!Str::contains($item->title, 'Experimental') && !Str::contains($item->title, 'Cup')))
            ->values()
            ->toArray();
    }
}