<?php

namespace App\Repositories\Logs\LogsTF;

use App\Events\LogAlreadyParsed;
use App\Events\LogErrorDuringParse;
use App\Events\LogParsed;
use App\Events\LogStartParse;
use App\Models\Chat;
use App\Models\ClassSwitch;
use App\Models\Disconnect;
use App\Models\Domination;
use App\Models\HealSpread;
use App\Models\Killstreak;
use App\Models\Log;
use App\Models\PlayerStatline;
use App\Models\Revenge;
use App\Models\Round;
use App\Models\RoundEvent;
use App\Models\RoundPlayer;
use App\Models\Spawn;
use App\Models\User;
use App\Repositories\Leagues\Aussie\OzFortressRepo;
use App\Repositories\Leagues\Burger\RGLRepo;
use App\Repositories\Leagues\Euro\ETF2LRepo;
use App\Repositories\Logs\LogsTF\Contracts\Logs;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Exception;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Spatie\TemporaryDirectory\Exceptions\PathAlreadyExists;
use Spatie\TemporaryDirectory\TemporaryDirectory;
use Symfony\Component\Console\Output\ConsoleOutput;
use xPaw\Steam\SteamID;
use ZipArchive;

class LogsRepo implements Logs
{
    /**
     * @var ConsoleOutput $consoleOutput
     */
    protected ConsoleOutput $consoleOutput;

    /**
     * @var object $json
     */
    protected object $json;

    /**
     * Delay in seconds in between requests.
     * @var int $delay
     */
    protected int $delay = 5;

    protected string $nameResolution = 'ETF2L';

    /**
     * @var Collection $lines
     */
    protected Collection $lines;

    public function __construct()
    {
        $this->consoleOutput = new ConsoleOutput();
    }

    public function getNameResolution(): string
    {
        return $this->nameResolution;
    }

    public function setNameResolution(string $league): void
    {
        $this->nameResolution = $league;
    }

    public function getSteamInfoBySteamId(array $steamIds): object
    {
        $url = 'https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2/?key=' . env('STEAM_API_KEY') . '&format=json&steamids=' . implode(',', $steamIds);
        $res = Http::timeout(30)
                   ->retry(3, 5000, function ($exception) {
                       return $exception instanceof ConnectionException;
                   })
                   ->withHeaders(
                       [
                           'Accept' => 'application/json'
                       ]
                   )->get($url);

        return $res->object();
    }

    public function createUsersForSteamIds(array $steamIds, string $nameResolution = 'ETF2L')
    {
        try {
            $existingUsers = User::query()
                                 ->select('id', 'name')
                                 ->whereIn('id', $steamIds)
                                 ->get()
                                 ->pluck('id')
                                 ->toArray();

            $notYetAdded = array_values(array_diff($steamIds, $existingUsers));
            if (count($notYetAdded) > 0) {
                $body    = $this->getSteamInfoBySteamId($notYetAdded);
                $players = $body->response->players;

                foreach ($players as $player) {
                    //$this->consoleOutput->writeln("Found Steam for the following user: " . $player->steamid . " with name " . $player->personaname);
                    $steamId = new SteamID($player->steamid);

                    //$this->consoleOutput->writeln("Created a user for " . $player->steamid . " with name " . $player->personaname);
                    $playerName = $player->personaname;

                    if ($this->nameResolution === 'ETF2L') {
                        // Preference for ETF2L alias if possible/found.
                        try {
                            $etf2l      = (new ETF2LRepo())->getUserInfo($player->steamid);
                            $playerName = $etf2l->player->name ?? $player->personaname;
                        } catch (Exception $ex) {
                            logger()->error("An error occurred trying to get the ETF2L alias for person with steam ID $player->steamid: " . $ex->getMessage());
                        }
                    }

                    if ($this->nameResolution === 'RGL') {
                        try {
                            $rgl        = (new RGLRepo())->scrapePlayerInfoBySteamId($player->steamid);
                            $playerName = $rgl->name ?? $player->personaname;
                        } catch (Exception $ex) {
                            logger()->error("An error occurred trying to get the RGL alias for person with steam ID $player->steamid: " . $ex->getMessage());
                        }
                    }

                    if ($this->nameResolution === 'ozfortress') {
                        try {
                            $oz         = (new OzFortressRepo())->getUserInfoBySteamId($player->steamid);
                            $playerName = $oz->name ?? $player->personaname;
                        } catch (Exception $ex) {
                            logger()->error("An error occurred trying to get the ozfortress alias for person with steam ID $player->steamid: " . $ex->getMessage());
                        }
                    }

                    User::query()
                        ->updateOrCreate(
                            [
                                'id' => $player->steamid,
                            ],
                            [
                                'id'                           => $player->steamid,
                                'steamid2'                     => $steamId->RenderSteam2(),
                                'steamid3'                     => $steamId->RenderSteam3(),
                                'steamid64'                    => $player->steamid,
                                'name'                         => Str::transliterate($playerName),
                                'password'                     => bcrypt($player->steamid),
                                'steam_profile_picture_small'  => $player->avatar,
                                'steam_profile_picture_medium' => $player->avatarmedium,
                                'steam_profile_picture_big'    => $player->avatarfull,
                            ]
                        );
                }
            }
        } catch (Exception $ex) {
            logger()->error('An error occurred fetching data from the Steam API relating to users.');
            logger()->error('The error occurred at line ' . $ex->getLine() . ' in file ' . $ex->getFile());
            logger()->error('The line of code that is causing problems is ' . $ex->getCode());
            //$this->consoleOutput->writeln("An error occurred fetching data from the Steam API relating to users.");
        }
    }

    public function deleteLog(int $logId)
    {
        $log = Log::find($logId);

        if ($log instanceof Log) {
            $log->roundPlayers()->delete();
            $log->roundEvents()->delete();
            $log->rounds()->delete();
            $log->statlines()->delete();
            $log->weapons()->delete();
            $log->disconnects()->delete();
            $log->switches()->delete();
            $log->spawns()->delete();
            $log->dominations()->delete();
            $log->revenges()->delete();
            $log->healSpreads()->delete();
            $log->killstreaks()->delete();
            $log->chats()->delete();
            $log->delete();
        }
    }

    /**
     * Checks whether the log doesn't exist yet within our database.
     * @param int $logId
     * @return bool
     */
    public function logDoesNotExist(int $logId): bool
    {
        return Log::query()
                  ->where('id', $logId)
                  ->doesntExist();
    }

    /**
     * Find logs by Steam ID's of players who participated, the map on which the game was played & the time the game was played.
     * @param array $steamIds
     * @param string $map
     * @param Carbon $matchTime
     * @return object
     * @throws Exception
     */
    public function findLogsBySteamIds(array $steamIds, string $map, Carbon $matchTime): array
    {
        if (count($steamIds) === 0) {
            throw new Exception("At least one user's Steam ID needs to be provided!");
        }

        // the logs.tf API will return an error if we search for more.
        if (count($steamIds) > 18) {
            $steamIds = collect($steamIds)->take(18)->toArray();
        }

        // Build the URL that will give me the log :)
        $url = "https://logs.tf/api/v1/log?player=" . implode(',', $steamIds) . "&map=$map";

        //$this->consoleOutput->writeln("Requesting logs on following URL: " . $url);

        $res = Http::timeout(30)
                   ->retry(3, 10000, function ($exception) {
                       return $exception instanceof ConnectionException;
                   })
                   ->withHeaders(
                       [
                           'Accept'       => 'application/json',
                           'Content-Type' => 'application/json'
                       ]
                   )
                   ->get($url);

        $body = $res->object();

        $relevantLogs = [];

        if (isset($body->logs)) {
            foreach ($body->logs as $log) {
                $logDate = Carbon::createFromTimestamp($log->date, 'Europe/Brussels');

                // Logs that are 1.5 days later should not be in. (I think that's pretty forgiving still...)
                // Logs that were before the match date should also not be in.
                // If the log was restored later (and there is some indication of that) do add it however.
                if ($logDate->isAfter($matchTime) && ($logDate->isBefore($matchTime->toImmutable()->addDay()->addHours(12)) || Str::contains(strtolower($log->title), 'restore'))) {
                    $relevantLogs[] = (object)[
                        'date'  => $logDate,
                        'map'   => $log->map,
                        'id'    => $log->id,
                        'title' => $log->title,
                        'url'   => 'https://logs.tf/' . $log->id
                    ];
                }
            }
        }

        // Repeat the same process but capitalize every letter of the map. For some reason, some of the server providers
        // used to do this especially for older logs.

        $uppercaseMapUrl = "https://logs.tf/api/v1/log?player=" . implode(',', $steamIds) . "&map=" . strtoupper($map);
        $res             = Http::timeout(30)
                               ->retry(3, 10000, function ($exception) {
                                   return $exception instanceof ConnectionException;
                               })
                               ->withHeaders(
                                   [
                                       'Accept'       => 'application/json',
                                       'Content-Type' => 'application/json'
                                   ]
                               )
                               ->get($uppercaseMapUrl);

        $uppercaseBody = $res->object();

        foreach ($uppercaseBody->logs as $log) {
            $logDate = Carbon::createFromTimestamp($log->date, 'Europe/Brussels');

            if ($logDate->isAfter($matchTime) && ($logDate->isBefore($matchTime->toImmutable()->addDay()->addHours(12)) || Str::contains(strtolower($log->title), 'restore'))) {
                $relevantLogs[] = (object)[
                    'date'  => $logDate,
                    'map'   => $log->map,
                    'id'    => $log->id,
                    'title' => $log->title,
                    'url'   => 'https://logs.tf/' . $log->id
                ];
            }
        }

        return $relevantLogs;
    }

    /**
     * Returns a list of spawn times for everyone.
     * Could be interesting to observe stuff like respawn waves (in contrast to in-game events).
     * Don't sweat it too much if for some reason we can't add this information. It's not that crucial.
     * Log the error to a log just in case we notice odd situations later and require debugging.
     * @param Log $log
     */
    public function addSpawns(Log $log): void
    {
        $spawns = $this->lines->filter(fn ($item) => Str::contains($item, [' spawned as ']))
                              ->values();

        // L 05/23/2021 - 19:36:18: Log file started (file "logs/L0523007.log") (game "/home/tf2/tf2-3/tf") (version "6394067")
        $sd = str_replace(' - ', ' ', substr($this->lines->first(), 0, 21));
        //$this->consoleOutput->writeln($sd);
        $startDateTime = Carbon::createFromFormat('m/d/Y H:i:s', $sd);

        // add start time to log
        //$log->update(['started_at' => $startDateTime]);

        // Spawn lines look something like the following:
        // L 10/24/2021 - 19:01:04: "SDCK JackyLegs<5><[U:1:38678832]><Red>" spawned as "Soldier"
        // L 11/25/2013 - 21:06:03: "Steve 8-)<25><STEAM_0:0:24676016><Red>" spawned as "Pyro"
        foreach ($spawns as $spawn) {
            try {
                $steamIdType = Str::contains($spawn, 'STEAM_0:') ? 2 : 3;
                $steamId3    = match ($steamIdType) {
                    2 => 'STEAM_0:' . Str::before(Str::after($spawn, 'STEAM_0:'), '>'),
                    3 => '[' . Str::beforeLast(Str::afterLast($spawn, '<['), ']>') . ']'
                };
                $steamId     = new SteamID($steamId3);
                $steamId64   = $steamId->ConvertToUInt64();

                $d = trim($startDateTime->format('Y-m-d') . ' ' . substr($spawn, 13, 8));

                //$this->consoleOutput->writeln("SteamID: $steamId64 Start time: $startDateTime Spawn line: $spawn Date: $d");

                $when = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $d);

                // haven't figured out the timezone stuff yet, but it's two hours too late. todo
                $time = $when->diffInSeconds($startDateTime);
                //$this->consoleOutput->writeln("Round started at $startDateTime and we spawned at $when");

                $name = match ($steamIdType) {
                    2 => Str::before(Str::before(Str::after($spawn, '"'), "><STEAM_0:"), '<'),
                    3 => Str::before(Str::before(Str::after($spawn, '"'), "><["), '<')
                };

                Spawn::query()
                     ->create(
                         [
                             'log_id'     => $log->id,
                             'steam_id'   => $steamId64,
                             'tf_class'   => Str::afterLast(Str::beforeLast($spawn, '"'), '"'),
                             'name'       => Str::transliterate($name),
                             'team'       => Str::contains($spawn, 'Red') ? 'Red' : 'Blue',
                             'time'       => $time,
                             'spawned_at' => $when
                         ]
                     );
            } catch (Exception $ex) {
                /*if (auth()->check()) {
                    LogErrorDuringParse::dispatch($log->id);
                }*/
                logger()->error("An error occurred while attempting to add the spawn time of this person to the database: ", [
                    'spawn' => $spawn,
                    'error' => $ex->getMessage(),
                    'line'  => $ex->getLine(),
                    'code'  => $ex->getCode()
                ]);
                //$this->consoleOutput->writeln("Error occurred on line $spawn : could not add spawn. " . $ex->getMessage());
            }
        }
    }

    public function addDominationsAndRevenges(Log $log)
    {
        // Dominations & revenges
        $dominations = $this->lines->filter(function ($item) {
            return Str::contains($item, [' triggered "domination" against ']);
        })->values();
        $revenges    = $this->lines->filter(function ($item) {
            return Str::contains($item, [' triggered "revenge" against ']);
        })->values();

        $startDateTime = Carbon::createFromFormat('m/d/Y H:i:s', str_replace(' - ', ' ', substr($this->lines->first(), 0, 21)));

        // ex. L 10/24/2021 - 19:00:54: "grip<35><[U:1:127936248]><Blue>" triggered "revenge" against "SDCK Clark<12><[U:1:38255057]><Red>"
        foreach ($dominations as $domination) {
            try {
                $event = explode(' triggered "domination" against ', $domination);

                $timestamp = substr($domination, 13, 8);
                //$this->consoleOutput->writeln("Domination: " . $startDateTime->format('Y-m-d') . ' ' . $timestamp);
                $when = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $startDateTime->format('Y-m-d') . ' ' . $timestamp);
                // haven't figured out the timezone stuff yet, but it's two hours too late. todo
                $time = $when->diffInSeconds($startDateTime);
                //$this->consoleOutput->writeln("$time is diff between $when and $startDateTime");

                // grip<35><[U:1:127936248]><Blue>
                $source = Str::before(Str::after($event[0], '"'), '"');

                $steamIdType = Str::contains($source, 'STEAM_0:') ? 2 : 3;
                $sourceName  = match ($steamIdType) {
                    3 => Str::beforeLast(explode('><[', $source)[0], '<'),
                    2 => Str::beforeLast(explode('><STEAM_0:', $source)[0], '<')
                };
                // grip<35><[U:1:127936248]><Blue>
                // L 11/25/2013 - 21:12:46: "furbo<30><STEAM_0:1:741224><Red>" triggered "domination" against "TLR kileR4fuNN' :P<40><STEAM_0:0:15653274><Blue>"
                $sourceSteamid3 = match ($steamIdType) {
                    3 => Str::replace(['><]'], [''], '[' . Str::after(Str::beforeLast($source, ']>') . ']', '<[')),
                    2 => 'STEAM_0:' . Str::between($source, 'STEAM_0:', '><'),
                };
                $sourceTeam     = Str::contains($source, '<Red>') ? 'Red' : 'Blue';

                // Event 1 shouldn't need any parsing anymore, should be fine after split already.
                // Just replace the double quote with an empty character.
                // SDCK Clark<12><[U:1:38255057]><Red>
                // L 06/07/2021 - 19:42:42: "Odin<40><[U:1:127534302]><Blue>" triggered "domination" against "DADDY ---<===3<38><[U:1:85387387]><Red>"
                $target = Str::replace('"', '', trim($event[1]));
                // SDCK Clark 12 [U:1:38255057] Red
                $targetName     = match ($steamIdType) {
                    3 => Str::beforeLast(explode('><[', $target)[0], '<'),
                    2 => Str::beforeLast(explode('><STEAM_0:', $target)[0], '<'),
                };
                $targetSteamid3 = match ($steamIdType) {
                    3 => Str::replace(['><]'], [''], '[' . Str::after(Str::beforeLast($target, ']>') . ']', '<[')),
                    2 => 'STEAM_0:' . Str::between($target, 'STEAM_0:', '><'),
                };
                $targetTeam     = Str::contains($target, '<Red>') ? 'Red' : 'Blue';

                //$this->consoleOutput->writeln("Source: $sourceSteamid3 Target: $targetSteamid3");
                Domination::query()
                          ->create(
                              [
                                  'log_id'          => $log->id,
                                  'steam_id'        => (new SteamID($sourceSteamid3))->ConvertToUInt64(),
                                  'team'            => Str::replace(' (assist 1)', '', $sourceTeam),
                                  'name'            => Str::transliterate(htmlspecialchars($sourceName)),
                                  'target_steam_id' => (new SteamID($targetSteamid3))->ConvertToUInt64(),
                                  'target_team'     => $targetTeam,
                                  'target_name'     => htmlspecialchars(Str::replace(' (assist 1)', '', $targetName)),
                                  'time'            => $time
                              ]
                          );
            } catch (Exception $ex) {
                /*if (auth()->check()) {
                    LogErrorDuringParse::dispatch($log->id);
                }*/
                logger()->error("An error occurred while attempting to add a domination entry to the database: ", [
                    'domination' => $domination,
                    'error'      => $ex->getMessage(),
                    'line'       => $ex->getLine(),
                    'code'       => $ex->getCode()
                ]);
                //$this->consoleOutput->writeln("Error occurred on line $domination : could not add domination. " . $ex->getMessage());
            }
        }

        // ex. L 10/24/2021 - 19:00:54: "grip<35><[U:1:127936248]><Blue>" triggered "revenge" against "SDCK Clark<12><[U:1:38255057]><Red>"
        foreach ($revenges as $revenge) {
            try {
                $event = explode(' triggered "revenge" against ', $revenge);

                $timestamp = substr($revenge, 13, 8);
                //$this->consoleOutput->writeln("Revenge: " . $startDateTime->format('Y-m-d') . ' ' . $timestamp);
                $when = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $startDateTime->format('Y-m-d') . ' ' . $timestamp);
                $time = Carbon::createFromFormat(
                    'Y-m-d H:i:s',
                    $when
                )->diffInSeconds($startDateTime);
                //$this->consoleOutput->writeln("$time is diff between $when and $startDateTime");

                // grip<35><[U:1:127936248]><Blue>
                // Part 0: name
                // Part 2: steamid3
                // Part 3: team
                // grip<35><[U:1:127936248]><Blue>

                $source      = Str::before(Str::after($event[0], '"'), '"');
                $steamIdType = Str::contains($source, 'STEAM_0:') ? 2 : 3;

                $sourceName     = match ($steamIdType) {
                    3 => Str::beforeLast(explode('><[', $source)[0], '<'),
                    2 => Str::beforeLast(explode('><STEAM_0:', $source)[0], '<')
                };
                $sourceSteamid3 = match ($steamIdType) {
                    3 => Str::replace(['><]'], [''], '[' . Str::after(Str::beforeLast($source, ']>') . ']', '<[')),
                    2 => 'STEAM_0:' . Str::between($source, 'STEAM_0:', '><'),
                };
                $sourceTeam     = Str::contains($source, '<Red>') ? 'Red' : 'Blue';

                // Event 1 shouldn't need any parsing anymore, should be fine after split already.
                // Just replace the double quote with an empty character.
                // SDCK Clark<12><[U:1:38255057]><Red>
                $target = Str::replace('"', '', trim($event[1]));
                // SDCK Clark 12 [U:1:38255057] Red
                $targetName     = match ($steamIdType) {
                    3 => Str::beforeLast(explode('><[', $target)[0], '<'),
                    2 => Str::beforeLast(explode('><STEAM_0:', $target)[0], '<'),
                };
                $targetSteamid3 = match ($steamIdType) {
                    3 => Str::replace(['><]'], [''], '[' . Str::after(Str::beforeLast($target, ']>') . ']', '<[')),
                    2 => 'STEAM_0:' . Str::between($target, 'STEAM_0:', '><'),
                };
                $targetTeam     = Str::contains($target, '<Red>') ? 'Red' : 'Blue';

                //$this->consoleOutput->writeln("Source: $sourceSteamid3 Target: $targetSteamid3");

                Revenge::query()
                       ->create(
                           [
                               'log_id'          => $log->id,
                               'steam_id'        => (new SteamID($sourceSteamid3))->ConvertToUInt64(),
                               'team'            => $sourceTeam,
                               'name'            => Str::transliterate(htmlspecialchars($sourceName)),
                               'target_steam_id' => (new SteamID($targetSteamid3))->ConvertToUInt64(),
                               'target_team'     => $targetTeam,
                               'target_name'     => htmlspecialchars($targetName),
                               'time'            => $time
                           ]
                       );
            } catch (Exception $ex) {
                /*if (auth()->check()) {
                    LogErrorDuringParse::dispatch($log->id);
                }*/
                logger()->error("An error occurred while attempting to add a revenge entry to the database: ", [
                    'revenge' => $revenge,
                    'error'   => $ex->getMessage(),
                    'line'    => $ex->getLine(),
                    'code'    => $ex->getCode()
                ]);
                //$this->consoleOutput->writeln("Error occurred on line $revenge : could not add revenge. " . $ex->getMessage());
            }
        }
    }

    /**
     * Adds player specific statistics like kills, deaths, assists etc.
     * @param Log $log
     */
    public function addPlayerStatlines(Log $log)
    {
        // Lines regarding build events for sentries, dispensers and teleporters
        // Older logs have a different way of checking for buildings built
        $sentriesBuilt = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"player_builtobject\" (object \"OBJ_SENTRYGUN\")", "triggered \"builtobject\" (object \"OBJ_SENTRYGUN\")"]);
        })->values();

        $dispensersBuilt = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"player_builtobject\" (object \"OBJ_DISPENSER\")", "triggered \"builtobject\" (object \"OBJ_DISPENSER\")"]);
        })->values();

        $teleportersBuilt = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"player_builtobject\" (object \"OBJ_TELEPORTER\")", "triggered \"builtobject\" (object \"OBJ_TELEPORTER\")"]);
        })->values();

        // Lines regarding destruction of sentries, dispensers and teleporters
        $sentriesDestroyed = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"killedobject\" (object \"OBJ_SENTRYGUN\")"]) && !Str::contains($item, ["(weapon \"pda_engineer\")"]);
        })->values();

        $dispensersDestroyed = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"killedobject\" (object \"OBJ_DISPENSER\")"]) && !Str::contains($item, ["(weapon \"pda_engineer\")"]);
        })->values();

        $teleportersDestroyed = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"killedobject\" (object \"OBJ_TELEPORTER\")"]) && !Str::contains($item, ["(weapon \"pda_engineer\")"]);
        })->values();


        // Lines regarding extinguishes (pyro)
        $extinguishes = $this->lines->filter(function ($item) {
            return Str::contains($item, ["triggered \"player_extinguished\""]);
        })->values();

        // Lines that represent Dead Ringer feigns from a Spy
        $deadRingerFeigns = $this->lines->filter(function ($item) {
            return Str::contains($item, ["customkill \"feign_death\""]);
        })->values();

        // Ammo pick-ups (small, medium, big)
        $largeAmmoPacks = $this->lines->filter(function ($item) {
            return Str::contains($item, [" picked up item \"ammopack_large\""]);
        })->values();

        $mediumAmmoPacks = $this->lines->filter(function ($item) {
            return Str::contains($item, [" picked up item \"ammopack_medium\""]);
        })->values();

        $smallAmmoPacks = $this->lines->filter(function ($item) {
            return Str::contains($item, [" picked up item \"ammopack_small\""]);
        })->values();

        // Checks whether the person uses P-REC (can't check for in-game demo system)
        // Could be useful to check whether people nicely record their demos or not (can't POSSIBLY see how this could be abused.......)
        $precEnabled = $this->lines->filter(function ($item) {
            return Str::contains($item, ' say_team "[P-REC] ');
        })->values();

        // Dominations & revenges
        $dominations = $this->lines->filter(function ($item) {
            return Str::contains($item, [' triggered "domination" against ']);
        })->values();
        $revenges    = $this->lines->filter(function ($item) {
            return Str::contains($item, [' triggered "revenge" against ']);
        })->values();

        // Duration in seconds / 60 = amount of minutes
        $minutes = $log->duration_real / 60;

        // 64-bit Steam ID's
        $steamIdList = [];

        if (isset($this->json->players)) {
            foreach ($this->json->players as $steamId3 => $player) {
                $weapons = [];

                $steamId       = (new SteamID($steamId3));
                $steamIdList[] = $steamId->ConvertToUInt64();
                $parameters    = [
                    'log_id'                             => $log->id,
                    'name'                               => Str::transliterate($this->json->names->{$steamId3}),
                    // We consider it a domination when the person in question isn't the one getting dominated.
                    'dominations'                        => $dominations->filter(fn ($item) => !Str::contains($item, 'triggered "domination" against "' . $this->json->names->{$steamId3}))->count(),
                    // We consider it a revenge when it the player himself who is getting the revenge (not the other party)
                    'revenges'                           => $revenges->filter(fn ($item) => !Str::contains($item, 'triggered "revenge" against "' . $this->json->names->{$steamId3}))->count(),
                    'team'                               => $player->team,
                    'steam_id'                           => $steamId->ConvertToUInt64(),
                    'kills'                              => $player->kills,
                    'kills_per_minute'                   => $player->kills / $minutes,
                    'assists'                            => $player->assists,
                    'assists_per_minute'                 => $player->assists / $minutes,
                    'deaths'                             => $player->deaths,
                    'deaths_per_minute'                  => $player->deaths / $minutes,
                    'suicides'                           => $player->suicides ?? null,
                    'suicides_per_minute'                => isset($player->suicides) ? $player->suicides / $minutes : null,
                    'kills_per_death'                    => $player->deaths > 0 ? $player->kills / $player->deaths : $player->kills,
                    'kills_assists_per_death'            => $player->deaths > 0 ? ($player->kills + $player->assists) / $player->deaths : ($player->kills + $player->assists),
                    'assists_per_death'                  => $player->deaths > 0 ? $player->assists / $player->deaths : $player->assists,
                    // Apparently logs without damage ACTUALLY EXIST???
                    // https://logs.tf/1558575
                    'damage'                             => $player->dmg ?? null,
                    'damage_per_minute'                  => !empty($player->dmg) ? $player->dmg / $minutes : null,
                    // Damage real comes into play for stuff like Spy where damage is inflated for backstabs normally.
                    'damage_real'                        => $player->dmg_real ?? null,
                    'damage_real_per_minute'             => isset($player->dmg_real) ? $player->dmg_real / $minutes : null,
                    // Logs from 2013 era and earlier do not always contain Damage Taken
                    'damage_taken'                       => $player->dt ?? null,
                    'damage_taken_per_minute'            => isset($player->dt) ? $player->dt / $minutes : null,
                    'heals_received'                     => $player->hr ?? null,
                    'heals_received_per_minute'          => isset($player->hr) ? $player->hr / $minutes : null,
                    'longest_killstreak'                 => $player->lks,
                    // Logs from 2013 era and earlier do not contain Airshots
                    'airshots'                           => $player->as ?? null,
                    'airshots_per_minute'                => isset($player->as) ? $player->as / $minutes : null,
                    'medkit_pickup'                      => $player->medkits,
                    'medkit_pickup_per_minute'           => $player->medkits / $minutes,
                    'medkit_health'                      => $player->medkits_hp ?? null,
                    'medkit_health_per_minute'           => isset($player->medkits_hp) ? $player->medkits_hp / $minutes : null,
                    'backstabs'                          => $player->backstabs,
                    'backstabs_per_minute'               => $player->backstabs / $minutes,
                    'headshot_kills'                     => $player->headshots,
                    'headshot_kills_per_minute'          => $player->headshots / $minutes,
                    // This is not present everywhere: older logs only counted headshot kills & not total headshots.
                    'headshots'                          => $player->headshots_hit ?? null,
                    'headshots_per_minute'               => isset($player->headshots_hit) ? $player->headshots_hit / $minutes : null,
                    // Data we got from raw log file
                    'sentries_built'                     => $sentriesBuilt->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'dispensers_built'                   => $dispensersBuilt->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'teleporters_built'                  => $teleportersBuilt->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'sentries_destroyed'                 => $sentriesDestroyed->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'dispensers_destroyed'               => $dispensersDestroyed->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'teleporters_destroyed'              => $teleportersDestroyed->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'extinguishes'                       => $extinguishes->filter(fn ($item) => Str::contains($item, $steamId3) && Str::contains(explode("triggered \"player_extinguished\"", $item)[0], $this->json->names->{$steamId3}))->count(),
                    'feigns'                             => $deadRingerFeigns->filter(fn ($item) => Str::contains($item, $steamId3) && Str::contains($item, "killed \"" . $this->json->names->{$steamId3}))->count(),
                    'sentries_built_per_minute'          => $sentriesBuilt->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'dispensers_built_per_minute'        => $dispensersBuilt->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'teleporters_built_per_minute'       => $teleportersBuilt->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'sentries_destroyed_per_minute'      => $sentriesDestroyed->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'dispensers_destroyed_per_minute'    => $dispensersDestroyed->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'teleporters_destroyed_per_minute'   => $teleportersDestroyed->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'extinguishes_per_minute'            => $extinguishes->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'feigns_per_minute'                  => $deadRingerFeigns->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'point_captures'                     => $player->cpc,
                    'point_captures_per_minute'          => $player->cpc / $minutes,
                    'intel_captures'                     => $player->ic,
                    'intel_captures_per_minute'          => $player->ic / $minutes,
                    'charges'                            => $player->ubers,
                    'charges_uber'                       => $player->ubertypes->medigun ?? null,
                    'charges_kritzkrieg'                 => $player->ubertypes->kritzkrieg ?? null,
                    'charges_vaccinator'                 => $player->ubertypes->vaccinator ?? null,
                    'charges_quickfix'                   => $player->ubertypes->quickfix ?? null,
                    'charges_per_minute'                 => $player->ubers / $minutes,
                    'charges_uber_per_minute'            => ($player->ubertypes->medigun ?? 0) / $minutes,
                    'charges_kritzkrieg_per_minute'      => ($player->ubertypes->kritzkrieg ?? 0) / $minutes,
                    'charges_vaccinator_per_minute'      => ($player->ubertypes->vaccinator ?? 0) / $minutes,
                    'charges_quickfix_per_minute'        => ($player->ubertypes->quickfix ?? 0) / $minutes,
                    'drops'                              => $player->drops,
                    'drops_per_minute'                   => $player->drops / $minutes,
                    // Medic stats were not available until somewhere around 2014-2015
                    'advantages_lost'                    => $player->medicstats->advantages_lost ?? null,
                    'biggest_advantage_lost'             => $player->medicstats->biggest_advantage_lost ?? null,
                    'deaths_with_95_uber'                => $player->medicstats->deaths_with_95_99_uber ?? null,
                    'deaths_within_20s_after_uber'       => $player->medicstats->deaths_within_20s_after_uber ?? null,
                    'average_time_before_healing'        => $player->medicstats->avg_time_before_healing ?? null,
                    'average_time_to_build'              => $player->medicstats->avg_time_to_build ?? null,
                    'average_time_before_using'          => $player->medicstats->avg_time_before_using ?? null,
                    // In seconds (does not account for pauses)
                    'average_charge_length'              => $player->medicstats->avg_uber_length ?? null,
                    'ammo_pack_small_pickup'             => $smallAmmoPacks->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'ammo_pack_medium_pickup'            => $mediumAmmoPacks->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'ammo_pack_large_pickup'             => $largeAmmoPacks->filter(fn ($item) => Str::contains($item, $steamId3))->count(),
                    'ammo_pack_small_pickup_per_minute'  => $smallAmmoPacks->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'ammo_pack_medium_pickup_per_minute' => $mediumAmmoPacks->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'ammo_pack_large_pickup_per_minute'  => $largeAmmoPacks->filter(fn ($item) => Str::contains($item, $steamId3))->count() / $minutes,
                    'prec_enabled'                       => $precEnabled->filter(fn ($item) => Str::contains($item, $steamId3))->count() > 0
                ];

                $classesPlayed = [];

                // Class specific statistics
                foreach ($player->class_stats as $classStat) {
                    $classesPlayed[]                              = $classStat->type;
                    $parameters["time_as_" . $classStat->type]    = $classStat->total_time;
                    $parameters["kills_as_" . $classStat->type]   = $classStat->kills;
                    $parameters["assists_as_" . $classStat->type] = $classStat->assists;
                    $parameters["deaths_as_" . $classStat->type]  = $classStat->deaths;
                    $parameters["damage_as_" . $classStat->type]  = $classStat->dmg;

                    foreach ($classStat->weapon as $weaponName => $weaponStats) {
                        if (!is_integer($weaponStats)) {
                            $weapons[] = [
                                'log_id'            => $log->id,
                                'name'              => $weaponName,
                                'steam_id'          => $steamId->ConvertToUInt64(),
                                'tf_class'          => $classStat->type,
                                'kills'             => $weaponStats->kills,
                                'damage'            => $weaponStats->dmg,
                                'damage_per_minute' => $weaponStats->dmg / $minutes,
                                'shots'             => $weaponStats->shots,
                                'hits'              => $weaponStats->hits,
                                'accuracy'          => $weaponStats->shots > 0 && $log->has_accuracy ? $weaponStats->hits / $weaponStats->shots * 100 : null,
                            ];
                        } else {
                            // In really ancient logs, weapons were formatted differently and didn't track accuracy either.
                            $weapons[] = [
                                'log_id'   => $log->id,
                                'name'     => $weaponName,
                                'steam_id' => $steamId->ConvertToUInt64(),
                                'tf_class' => $classStat->type,
                                'kills'    => $weaponStats,
                            ];
                        }
                    }
                }

                $mostPlayedClass = $classesPlayed[0];

                // Most played class (differentiate between off-classes & class that was played primarily during the round)
                $parameters['class_played']   = $mostPlayedClass;
                $parameters['classes_played'] = $classesPlayed;

                $plurals = [
                    'scout'        => 'scouts',
                    'soldier'      => 'soldiers',
                    'pyro'         => 'pyros',
                    'demoman'      => 'demomen',
                    'heavyweapons' => 'heavies',
                    'engineer'     => 'engineers',
                    'medic'        => 'medics',
                    'sniper'       => 'snipers',
                    'spy'          => 'spies'
                ];

                // Could potentially be empty if no kills were gotten (ex. medics)

                $classKills = $this->json->classkills->{$steamId3} ?? (object)[];

                foreach ($classKills as $class => $amount) {
                    if ($class !== 'unknown') {
                        $parameters[$plurals[$class] . '_killed']            = $amount;
                        $parameters[$plurals[$class] . '_killed_per_minute'] = $amount / $minutes;
                    }
                }

                $classDeaths = $this->json->classdeaths->{$steamId3} ?? (object)[];

                foreach ($classDeaths as $class => $amount) {
                    if ($class !== 'unknown') {
                        $parameters['deaths_to_' . $plurals[$class]]                 = $amount;
                        $parameters['deaths_to_' . $plurals[$class] . '_per_minute'] = $amount / $minutes;
                    }
                }

                $classAssists = $this->json->classkillassists->{$steamId3} ?? (object)[];

                foreach ($classAssists as $class => $amount) {
                    // Edge case #ILostCount: some logs don't know which class was actually the one that got the kill.
                    // ex. https://logs.tf/1558575
                    if ($class !== 'unknown') {
                        $parameters[$plurals[$class] . '_assisted']            = $amount;
                        $parameters[$plurals[$class] . '_assisted_per_minute'] = $amount / $minutes;
                    }
                }

                if ($mostPlayedClass === 'sniper') {
                    $snipersKilled           = $parameters['snipers_killed'] ?? 0;
                    $deathsToSnipers         = $parameters['deaths_to_snipers'] ?? 0;
                    $parameters['svs_ratio'] = $deathsToSnipers > 0 ? $snipersKilled / $deathsToSnipers : $snipersKilled;
                }

                // Ensure user exists.
                $this->createUsersForSteamIds([$steamId->ConvertToUInt64()]);

                /**
                 * @var PlayerStatline $statline
                 */
                $statline = PlayerStatline::create($parameters);

                foreach ($weapons as $weapon) {
                    $statline->weapons()->create($weapon);
                }
            }
        }

        // Add the people from this log to the database of users in the system.
        $this->createUsersForSteamIds($steamIdList);
    }

    /**
     * Adds the different rounds to the log.
     * @param Log $log
     */
    public function addRounds(Log $log)
    {
        if (isset($this->json->rounds)) {
            foreach ($this->json->rounds as $round) {
                $start      = Carbon::createFromTimestamp($round->start_time);
                $matchRound = Round::create(
                    [
                        'log_id'      => $log->id,
                        'started_at'  => $start->toDateTimeString(),
                        'stopped_at'  => $start->toImmutable()->addSeconds($round->length),
                        'winner'      => $round->winner,
                        'duration'    => $round->length,
                        'first_cap'   => $round->firstcap,
                        'red_score'   => $round->team->Red->score,
                        'red_kills'   => $round->team->Red->kills,
                        'red_damage'  => $round->team->Red->dmg,
                        'red_charges' => $round->team->Red->ubers,
                        'blu_score'   => $round->team->Blue->score,
                        'blu_kills'   => $round->team->Blue->kills,
                        'blu_damage'  => $round->team->Blue->dmg,
                        'blu_charges' => $round->team->Blue->ubers,
                    ]
                );

                $this->createUsersForSteamIds(
                    collect($round->players)
                        ->keys()
                        ->filter(fn ($item) => Str::containsAll($item, ['[', ']']) || Str::contains($item, 'STEAM_0:'))
                        ->values()
                        ->map(fn ($item) => (new SteamID($item))->ConvertToUInt64())
                        ->values()
                        ->toArray()
                );

                // Add round players (performance on a player per player basis for each round)
                foreach ($round->players as $steamId3 => $roundPlayer) {
                    if ((Str::containsAll($steamId3, ['[', ']']) || Str::contains($steamId3, 'STEAM_0:')) && $steamId3 !== '[U:0:0]') {
                        $steamId = new SteamID($steamId3);

                        RoundPlayer::query()
                                   ->create(
                                       [
                                           'log_id'   => $log->id,
                                           'round_id' => $matchRound->id,
                                           'steam_id' => $steamId->ConvertToUInt64(),
                                           'kills'    => $roundPlayer->kills,
                                           'damage'   => $roundPlayer->dmg,
                                           'team'     => $roundPlayer->team ?? null
                                       ]
                                   );
                    }
                }

                // Add round events (occurrences like drops, point captures, medic deaths etc)
                foreach ($round->events as $event) {
                    $parameters = [
                        'log_id'   => $log->id,
                        'round_id' => $matchRound->id,
                        'type'     => $event->type,
                        'tick'     => $event->time,
                        'team'     => $event->team
                    ];

                    $steamIdsToAdd = [];

                    if (isset($event->steamid)) {
                        $parameters['steam_id'] = (new SteamID($event->steamid))->ConvertToUInt64();
                        $steamIdsToAdd[]        = (new SteamID($event->steamid))->ConvertToUInt64();
                    }

                    if (isset($event->killer)) {
                        $parameters['killer_steam_id'] = (new SteamID($event->killer))->ConvertToUInt64();
                        $steamIdsToAdd[]               = (new SteamID($event->killer))->ConvertToUInt64();
                    }

                    $this->createUsersForSteamIds($steamIdsToAdd);

                    if (isset($event->point)) {
                        $parameters['point'] = $event->point;
                    }

                    if (isset($event->medigun)) {
                        $parameters['medigun'] = $event->medigun;
                    }

                    RoundEvent::query()
                              ->create($parameters);
                }
            }
        }
    }

    /**
     * Adds heal distribution from the log to the database.
     * @param Log $log
     */
    public function addHealSpread(Log $log)
    {
        if (isset($this->json->healspread)) {
            foreach ($this->json->healspread as $healer => $healSpreads) {
                foreach ($healSpreads as $target => $healAmount) {
                    try {
                        HealSpread::query()
                                  ->create(
                                      [
                                          'log_id'          => $log->id,
                                          'healer_steam_id' => (new SteamID($healer))->ConvertToUInt64(),
                                          'target_steam_id' => (new SteamID($target))->ConvertToUInt64(),
                                          'heal_amount'     => $healAmount
                                      ]
                                  );
                    } catch (\Exception $ex) {
                        logger()->error('An error occurred: cannot insert heal spread entry into database for log ' . $log->id);
                    }
                }
            }
        }
    }

    /**
     * Adds chat logs from a log to the database.
     * @param Log $log
     */
    public function addChat(Log $log)
    {
        if (isset($this->json->chat)) {
            $steamIds = collect($this->json->chat)
                ->pluck('steamid')
                ->filter(fn ($item) => !Str::contains($item, 'Console'))
                ->map(fn ($item) => (new SteamID($item))->ConvertToUInt64())
                ->values()
                ->toArray();
            $this->createUsersForSteamIds($steamIds);

            foreach ($this->json->chat as $chatMessage) {
                if ($chatMessage->steamid !== 'Console') {
                    //$this->consoleOutput->writeln("Trying to create a chat record for " . $chatMessage->steamid . " by " . $chatMessage->name . " who wrote " . $chatMessage->msg);
                    $steamId = new SteamID($chatMessage->steamid);

                    Chat::query()->create(
                        [
                            'log_id'   => $log->id,
                            'name'     => Str::transliterate($chatMessage->name),
                            'steam_id' => $steamId->ConvertToUInt64(),
                            'message'  => $chatMessage->msg
                        ]
                    );
                }
            }
        }
    }

    /**
     * Adds killstreak events for a log to the database.
     * @param Log $log
     */
    public function addKillstreaks(Log $log)
    {
        if (isset($this->json->killstreaks)) {
            $steamIds = collect($this->json->killstreaks)
                ->pluck('steamid')
                ->map(fn ($item) => (new SteamID($item))->ConvertToUInt64())
                ->values()
                ->toArray();
            $this->createUsersForSteamIds($steamIds);

            foreach ($this->json->killstreaks as $killstreak) {
                $steamId = new SteamID($killstreak->steamid);
                Killstreak::query()
                          ->create(
                              [
                                  'log_id'   => $log->id,
                                  'steam_id' => $steamId->ConvertToUInt64(),
                                  'name'     => Str::transliterate($this->json->names->{$killstreak->steamid}),
                                  'streak'   => $killstreak->streak,
                                  'tick'     => $killstreak->time
                              ]
                          );
            }
        }
    }

    /**
     * Takes note of whenever a player got disconnected from the server (for any reason).
     * @param Log $log
     */
    public function addDisconnects(Log $log)
    {
        $disconnects = $this->lines->filter(fn ($item) => Str::contains($item, [' disconnected (reason ']))
                                   ->values();

        // L 05/23/2021 - 19:36:18: Log file started (file "logs/L0523007.log") (game "/home/tf2/tf2-3/tf") (version "6394067")
        $startDateTime = Carbon::createFromFormat('m/d/Y H:i:s', str_replace(' - ', ' ', substr($this->lines->first(), 0, 21)));

        // Spawn lines look something like the following:
        // L 10/24/2021 - 19:01:04: "SDCK JackyLegs<5><[U:1:38678832]><Red>" spawned as "Soldier"
        // L 11/25/2013 - 21:06:03: "Steve 8-)<25><STEAM_0:0:24676016><Red>" spawned as "Pyro"
        foreach ($disconnects as $disconnect) {
            try {
                $steamIdType = Str::contains($disconnect, 'STEAM_0:') ? 2 : 3;
                $steamId3    = match ($steamIdType) {
                    2 => 'STEAM_0:' . Str::before(Str::after($disconnect, 'STEAM_0:'), '>'),
                    3 => '[' . Str::beforeLast(Str::afterLast($disconnect, '<['), ']>') . ']'
                };
                $steamId     = new SteamID($steamId3);
                $steamId64   = $steamId->ConvertToUInt64();

                $d = trim($startDateTime->format('Y-m-d') . ' ' . substr($disconnect, 13, 8));

                $when = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $d);

                $time = $when->diffInSeconds($startDateTime);
                //$this->consoleOutput->writeln("Round started at $startDateTime and we spawned at $when");

                $name = match ($steamIdType) {
                    2 => Str::before(Str::before(Str::after($disconnect, '"'), "><STEAM_0:"), '<'),
                    3 => Str::before(Str::before(Str::after($disconnect, '"'), "><["), '<')
                };

                $reason = trim(Str::replace(')', '', Str::replace('"', '', Str::after($disconnect, ' disconnected (reason '))));

                Disconnect::query()
                          ->create(
                              [
                                  'log_id'   => $log->id,
                                  'steam_id' => $steamId64,
                                  'reason'   => $reason,
                                  'name'     => Str::transliterate($name),
                                  'team'     => Str::contains($disconnect, 'Red') ? 'Red' : 'Blue',
                                  'time'     => $time,
                                  'when'     => $when
                              ]
                          );
            } catch (Exception $ex) {
                /*if (auth()->check()) {
                    LogErrorDuringParse::dispatch($log->id);
                }*/
                logger()->error("An error occurred while attempting to add a class switch of this person to the database: ", [
                    'disconnect' => $disconnect,
                    'error'      => $ex->getMessage(),
                    'line'       => $ex->getLine(),
                    'code'       => $ex->getCode()
                ]);
                //$this->consoleOutput->writeln("Error occurred on line $disconnect : could not add disconnect. " . $ex->getMessage());
            }
        }
    }

    /**
     * Takes note of when a player switched to a different class.
     * @param Log $log
     */
    public function addClassSwitches(Log $log)
    {
        $switches = $this->lines->filter(fn ($item) => Str::contains($item, ['" changed role to "']))
                                ->values();

        // L 05/23/2021 - 19:36:18: Log file started (file "logs/L0523007.log") (game "/home/tf2/tf2-3/tf") (version "6394067")
        $startDateTime = Carbon::createFromFormat('m/d/Y H:i:s', str_replace(' - ', ' ', substr($this->lines->first(), 0, 21)));

        // Spawn lines look something like the following:
        // L 10/24/2021 - 19:01:04: "SDCK JackyLegs<5><[U:1:38678832]><Red>" spawned as "Soldier"
        // L 11/25/2013 - 21:06:03: "Steve 8-)<25><STEAM_0:0:24676016><Red>" spawned as "Pyro"
        foreach ($switches as $switch) {
            try {
                $steamIdType = Str::contains($switch, 'STEAM_0:') ? 2 : 3;
                $steamId3    = match ($steamIdType) {
                    2 => 'STEAM_0:' . Str::before(Str::after($switch, 'STEAM_0:'), '>'),
                    3 => '[' . Str::beforeLast(Str::afterLast($switch, '<['), ']>') . ']'
                };
                $steamId     = new SteamID($steamId3);
                $steamId64   = $steamId->ConvertToUInt64();

                $d = trim($startDateTime->format('Y-m-d') . ' ' . substr($switch, 13, 8));

                $when = Carbon::createFromFormat(
                    'Y-m-d H:i:s', $d);

                $time = $when->diffInSeconds($startDateTime);
                //$this->consoleOutput->writeln("Round started at $startDateTime and we spawned at $when");

                $name = match ($steamIdType) {
                    2 => Str::before(Str::before(Str::after($switch, '"'), "><STEAM_0:"), '<'),
                    3 => Str::before(Str::before(Str::after($switch, '"'), "><["), '<')
                };

                $tfClass = trim(Str::replace('"', '', Str::after($switch, 'changed role to "')));

                ClassSwitch::query()
                           ->create(
                               [
                                   'log_id'   => $log->id,
                                   'steam_id' => $steamId64,
                                   'tf_class' => $tfClass,
                                   'name'     => Str::transliterate($name),
                                   'team'     => Str::contains($switch, 'Red') ? 'Red' : 'Blue',
                                   'time'     => $time,
                                   'when'     => $when
                               ]
                           );
            } catch (Exception $ex) {
                /*if (auth()->check()) {
                    LogErrorDuringParse::dispatch($log->id);
                }*/
                logger()->error("An error occurred while attempting to add a class switch of this person to the database: ", [
                    'switch' => $switch,
                    'error'  => $ex->getMessage(),
                    'line'   => $ex->getLine(),
                    'code'   => $ex->getCode()
                ]);
                //$this->consoleOutput->writeln("Error occurred on line $switch : could not add switch. " . $ex->getMessage());
            }
        }
    }

    /**
     * We'll do the actual parsing here. The URL one is just a helper for ease of use.
     * @param int $logId
     * @return Log
     * @throws PathAlreadyExists
     */
    public function parseLogById(int $logId): Log
    {
        $log = Log::select('id', 'title', 'map', 'date')
                  ->where('id', $logId)
                  ->first();
        // Only parse the log if it has not been parsed yet previously.
        if (!$log instanceof Log) {
            if (auth()->check()) {
                LogStartParse::dispatch($logId);
            }

            // Get ZIP file as a bunch of lines in a Laravel Collection.
            $this->lines = $this->downloadLogFile($logId);

            // Lines containing information regarding round length.
            $roundLines = $this->lines->filter(function ($item) {
                return Str::contains($item, ["World triggered \"Round_Length\"", "World triggered \"Mini_Round_Length\""]);
            })->values();

            // Calculate the total time of the match (corrected time due to pauses occurring in between competitive matches)
            $totalTime = 0.0;
            //$this->consoleOutput->writeln("Adjusting round time to account for pauses: determining real game time.");
            foreach ($roundLines as $line) {
                // Split based on " and pick the 3rd element of the array to obtain the value
                $roundTime = explode('"', $line);
                // Convert string into double
                $timeInSeconds = (double)$roundTime[3];
                // Add round time to total time
                $totalTime += $timeInSeconds;
            }

            // Convert amount of seconds into a readable timestamp value (Hours:minutes:seconds)
            //$totalTimeForHumans = CarbonInterval::seconds($totalTime)->cascade()->format('%H:%i:%s');

            // Gets the JSON data inside of the log.
            $this->json = $this->getLogJson($logId);

            // A check for whether we have a ZIP file.
            $zipPresent = $totalTime > 0.0 && $this->lines->count() > 0;

            $redTeamName = 'Red';
            $bluTeamName = 'Blue';

            // If the title contains 'versus', 'vs', 'vs.' we can try our luck at getting the team names.
            // It's not a perfect way of doing this but hopefully good enough :)
            // See serveme.tf logic below
            if (Str::contains($this->json->info->title, ['vs ', 'vs. ', 'versus '])) {
                $splitTeams = match (true) {
                    Str::contains($this->json->info->title, 'vs') => explode('vs', $this->json->info->title),
                    Str::contains($this->json->info->title, 'vs.') => explode('vs.', $this->json->info->title),
                    Str::contains($this->json->info->title, 'versus') => explode('versus', $this->json->info->title),
                };

                if (count($splitTeams) === 2) {
                    $bluSection  = explode(' ', trim($splitTeams[0]));
                    $redSection  = explode(' ', trim($splitTeams[1]));
                    $bluTeamName = end($bluSection);
                    $redTeamName = $redSection[0];
                }
            }

            // If the title from the log is from serveme.tf, the title should have the team names.
            // ex. serveme.tf #1206764 - sd!ck vs BLU
            if (Str::contains($this->json->info->title, 'serveme.tf')) {
                $teams = explode(' vs ', Str::after($this->json->info->title, ' - '));

                // problem here, is that I can't really differentiate which team is blu or red based on the title alone.
                // unless one of them keeps the original name of course :) or if they have the same order every time.
                // I think it is always first Blu, then Red.
                if (count($teams) === 2) {
                    $bluTeamName = $teams[0];
                    $redTeamName = $teams[1];
                }
            }

            // Check for any popular pug services (+ some leagues).
            $pugService = match (true) {
                Str::contains(strtolower($this->json->info->title), 'tf2center') => 'TF2Center',
                Str::contains(strtolower($this->json->info->title), 'tf2pickup') => 'TF2Pickup',
                Str::contains(strtolower($this->json->info->title), 'pugchamp') => 'PugChamp',
                Str::contains(strtolower($this->json->info->title), 'hlpugs.tf') => 'HLPugs.tf',
                Str::contains(strtolower($this->json->info->title), 'tf2lobby.eu') => 'TF2Lobby',
                default => null
            };

            $league = match (true) {
                !empty($this->nameResolution) => $this->nameResolution,
                Str::containsAll($this->json->info->title, ['UGC', 'Match']) => 'United Gaming Clans',
                Str::contains($this->json->info->title, ['RGL']) => 'Recharge Gaming League',
                Str::contains($this->json->info->title, ['ozfortress']) => 'OzFortress',
                Str::contains($this->json->info->title, ['AsiaFortress.com']) => 'AsiaFortress',
                Str::contains($this->json->info->title, ['CEVO TF2 Match Server']) => 'CEVO',
                default => null
            };

            $serverProvider = match (true) {
                Str::containsAll($this->json->info->title, ['serveme.tf']) => 'serveme.tf',
                Str::contains($this->json->info->title, ['tragicservers']) => 'TragicServers',
                Str::contains($this->json->info->title, ['hiperz']) => 'Hiperz',
                default => null
            };

            // Add uploader to list of users.
            if (isset($this->json->info->uploader->id)) {
                $this->createUsersForSteamIds([$this->json->info->uploader->id]);
            }

            // Create the log (everything will start from here on out)
            $log = Log::create(
                [
                    'id'                     => $logId,
                    'server_provider'        => $serverProvider,
                    'league'                 => $league,
                    'pug_service'            => $pugService,
                    'red_score'              => $this->json->teams->Red->score,
                    'red_kills'              => $this->json->teams->Red->kills,
                    'red_deaths'             => $this->json->teams->Red->deaths,
                    'red_damage'             => $this->json->teams->Red->dmg,
                    'red_first_caps'         => $this->json->teams->Red->firstcaps,
                    'red_caps'               => $this->json->teams->Red->caps,
                    'red_charges'            => $this->json->teams->Red->charges,
                    'red_drops'              => $this->json->teams->Red->drops ?? null,
                    'blu_score'              => $this->json->teams->Blue->score,
                    'blu_kills'              => $this->json->teams->Blue->kills,
                    'blu_deaths'             => $this->json->teams->Blue->deaths,
                    'blu_damage'             => $this->json->teams->Blue->dmg,
                    'blu_first_caps'         => $this->json->teams->Blue->firstcaps,
                    'blu_caps'               => $this->json->teams->Blue->caps,
                    'blu_charges'            => $this->json->teams->Blue->charges,
                    'blu_drops'              => $this->json->teams->Blue->drops ?? null,
                    'map'                    => $this->json->info->map,
                    'supplemental'           => $this->json->info->supplemental ?? false,
                    'has_real_damage'        => $this->json->info->hasRealDamage ?? false,
                    'has_weapon_damage'      => $this->json->info->hasWeaponDamage ?? false,
                    'has_accuracy'           => $this->json->info->hasAccuracy ?? false,
                    'has_medkit_pickups'     => $this->json->info->hasHP ?? false,
                    'has_medkit_health'      => $this->json->info->hasHP_real ?? false,
                    'has_headshot_kills'     => $this->json->info->hasHS ?? false,
                    'has_headshot_hits'      => $this->json->info->hasHS_hit ?? false,
                    'has_backstabs'          => $this->json->info->hasBS ?? false,
                    'has_point_captures'     => $this->json->info->hasCP ?? false,
                    'has_sentries_built'     => $this->json->info->hasSB ?? false,
                    'has_damage_taken'       => $this->json->info->hasDT ?? false,
                    'has_airshots'           => $this->json->info->hasAS ?? false,
                    'has_heals_received'     => $this->json->info->hasHR ?? false,
                    'has_intel_captures'     => $this->json->info->hasIntel ?? false,
                    'scoring_attack_defense' => $this->json->info->AD_scoring ?? false,
                    'red_team_name'          => $redTeamName,
                    'blu_team_name'          => $bluTeamName,
                    'title'                  => $this->json->info->title,
                    'date'                   => Carbon::createFromTimestamp($this->json->info->date)->toDateTimeString(),
                    'stopped_at'             => Carbon::createFromTimestamp($this->json->info->date)->toDateTimeString(),
                    'started_at'             => Carbon::createFromTimestamp($this->json->info->date)->subSeconds($this->json->info->total_length)->toDateTimeString(),
                    'uploader_steam_id'      => $this->json->info->uploader->id,
                    'uploader_name'          => $this->json->info->uploader->name,
                    'uploader_info'          => $this->json->info->uploader->info,
                    'duration'               => $this->json->info->total_length,
                    'has_zip_data'           => $zipPresent,
                    // Depending on whether we were able to retrieve the ZIP for the log: use the corrected time.
                    'duration_real'          => $zipPresent ? $totalTime : $this->json->info->total_length,
                    // We'll have to implement different strategies for finding the team names, not in JSON explicitly.
                    //'red_team_name',
                    //'blu_team_name'
                ]
            );

            //$this->consoleOutput->writeln("======== https://logs.tf/$logId");

            // Adds chat logs
            //$this->consoleOutput->writeln("Adding chat logs");
            $this->addChat($log);

            // Adds killstreak events & ticks
            //$this->consoleOutput->writeln("Adding killstreaks");
            $this->addKillstreaks($log);

            // Adds rounds & relevant events + player kills per round
            //$this->consoleOutput->writeln("Adding rounds");
            $this->addRounds($log);

            // Heal spread (mainly for Medic, maybe for Dispenser/Conch if over a certain threshold?)
            //$this->consoleOutput->writeln("Adding heal spread");
            $this->addHealSpread($log);

            // Handle player stats
            //$this->consoleOutput->writeln("Adding player performance stats");
            $this->addPlayerStatlines($log);

            if ($zipPresent) {
                // Handle spawn times
                //$this->consoleOutput->writeln("Adding spawn times");
                $this->addSpawns($log);

                // Add dominations & revenges (who dominated who)
                //$this->consoleOutput->writeln("Adding dominations & revenges");
                $this->addDominationsAndRevenges($log);

                // Add disconnects
                //$this->consoleOutput->writeln("Adding disconnects from the server");
                $this->addDisconnects($log);

                // Add class switches
                //$this->consoleOutput->writeln("Adding class switches");
                $this->addClassSwitches($log);
            }

            if (auth()->check()) {
                LogParsed::dispatch($logId);
            }
        } else {
            //$this->consoleOutput->writeln("A log is already registered for https://logs.tf/$logId");
            if (auth()->check()) {
                LogAlreadyParsed::dispatch($logId);
            }
        }

        return $log;
    }

    /**
     * Returns an array of lines of text in the logged ZIP file.
     * @param int $logId
     * @return Collection
     * @throws PathAlreadyExists
     */
    public function downloadLogFile(int $logId): Collection
    {
        // Construct the URL where the ZIP should be located.
        $url = config('logs.api.base') . "/logs/log_$logId.log.zip";

        //$this->consoleOutput->writeln("Downloading ZIP file from " . $url);

        // Create a temporary folder to write the log to.
        $tmp = (new TemporaryDirectory())->create();

        // Create path to ZIP folder
        $filePath = $tmp->path() . DIRECTORY_SEPARATOR . $logId . '.zip';

        $this->lines = collect();

        // This is wrapped in a try catch because some logs will not have ZIP archives anymore due to archiving.
        // In this case, not much we can do, it is what it is.
        try {
            // Write ZIP file after downloading it from Logs.TF.
            File::put($filePath, file_get_contents($url));

            $logFilePath = $tmp->path() . DIRECTORY_SEPARATOR . "log_" . $logId . '.log';

            // Extract ZIP file
            $zipArchive = new ZipArchive();
            $zipArchive->open($filePath);
            $zipArchive->extractTo($tmp->path());

            // Read log file
            $handle = fopen($logFilePath, "rb");

            while (!feof($handle)) {
                // The log line starts out with 'L '. This part is redundant.
                // Some logs also have special characters here, for some reason.
                // This messes with encoding a lot.
                $content = trim(Str::after(fgets($handle), 'L '));
                // Remove blank lines as they will mess up the parsing and provide no useful information whatsoever.
                // For the check: remove the special characters as well just in case they might be causing issues.
                if (!blank(preg_replace('/[^A-Za-z0-9\-]/', '', $content))) {
                    //$this->consoleOutput->writeln("Adding the following line: " . utf8_decode($content));
                    // For compatibility with special names (like Kylähullu) we change the encoding.

                    //$this->lines->push(mb_convert_encoding($content, 'ISO 8859-1', 'UTF-8'));
                    //$this->lines->push($content);
                    $this->lines->push(utf8_decode($content));
                }
            }

            fclose($handle);
        } catch (Exception $ex) {
            //$this->consoleOutput->writeln("No ZIP archive was found for this log." . $ex->getMessage());
            logger()->error('An error occurred downloading the ZIP file for log ' . $logId);
            logger()->error('Error: ' . $ex->getMessage());
            logger()->error('Occurred in file ' . $ex->getFile() . ' at line ' . $ex->getLine());
            if (auth()->check()) {
                LogErrorDuringParse::dispatch($logId);
            }
        }

        // Once we are done, remove the directory and the files within it. (cleanup of remains)
        File::cleanDirectory($tmp->path());
        File::deleteDirectory($tmp->path());

        return $this->lines;
    }

    public function getLogJson(int $logId): object
    {
        $url = config('logs.api.base') . config('logs.api.json') . $logId;
        $res = Http::timeout(30)
                   ->retry(3, 10000, function ($exception) {
                       return $exception instanceof ConnectionException;
                   })
                   ->withHeaders(
                       [
                           'Accept'       => 'application/json',
                           'Content-Type' => 'application/json'
                       ]
                   )->get($url);

        return $res->object();
    }

    /**
     * Returns the ID of the log based on the URL.
     * Needs to be numeric.
     * @throws Exception
     */
    public function getLogIdFromUrl(string $logUrl): int
    {
        $logId = Str::afterLast($logUrl, '/');

        if (!is_numeric($logId)) {
            throw new Exception("The log ID needs to be numeric.");
        }

        return (int)$logId;
    }

    /**
     * @throws Exception
     */
    public function parseLogByUrl(string $logUrl): Log
    {
        // Get the Log ID.
        $logId = $this->getLogIdFromUrl($logUrl);

        // Parse by log ID.
        return $this->parseLogById($logId);
    }
}