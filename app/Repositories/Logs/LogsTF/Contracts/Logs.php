<?php

namespace App\Repositories\Logs\LogsTF\Contracts;

interface Logs
{
    public function parseLogByUrl(string $logUrl);

    public function parseLogById(int $logId);

    public function downloadLogFile(int $logId);

    public function getLogJson(int $logId);

    public function getLogIdFromUrl(string $logUrl);

    public function logDoesNotExist(int $logId);

    public function deleteLog(int $logId);
}