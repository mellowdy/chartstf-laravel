<?php

namespace App\Console\Commands\Kritzkast;

use App\Imports\KritzkastImport;
use Illuminate\Console\Command;

class ReadVodsExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kritzkast:vods-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Links VODs of KritzKast to charts.tf matches (Excel courtesy of Wiethoofd)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // ps give me the kritzkast hat
        // haha jk... unless?

        $excelPath = storage_path('app' . DIRECTORY_SEPARATOR . 'vods' . DIRECTORY_SEPARATOR . 'kritzkast' . DIRECTORY_SEPARATOR . 'vods.xlsx');
        (new KritzkastImport())->import($excelPath);
    }
}
