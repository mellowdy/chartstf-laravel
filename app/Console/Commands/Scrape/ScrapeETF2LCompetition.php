<?php

namespace App\Console\Commands\Scrape;

use App\Repositories\Leagues\Euro\ETF2LRepo;
use App\Repositories\Logs\LogsTF\LogsRepo;
use App\Services\LogRetrieval\ETF2LLogRetrievalService;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class ScrapeETF2LCompetition extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:etf2l-competition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes a competition & attempts to get logs';

    protected ETF2LRepo $etf2lRepo;

    protected LogsRepo $logsRepo;

    protected ConsoleOutput $consoleOutput;

    protected ETF2LLogRetrievalService $etf2lLogRetrievalService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ETF2LRepo $etf2lRepo, LogsRepo $logsRepo, ConsoleOutput $consoleOutput, ETF2LLogRetrievalService $etf2lLogRetrievalService)
    {
        parent::__construct();

        $this->etf2lRepo                = $etf2lRepo;
        $this->logsRepo                 = $logsRepo;
        $this->consoleOutput            = $consoleOutput;
        $this->etf2lLogRetrievalService = $etf2lLogRetrievalService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $competitionId = $this->ask('What is the competition you want to fetch the logs from? (numeric) Use the ETF2L archives as a reference.');
        $competitionId = (int)$competitionId;

        $division = $this->ask('For which division are you looking to compile the data? (case sensitive)', 'Premiership');

        $logs = $this->etf2lLogRetrievalService->getLogsFromCompetition($competitionId, $division);

        $this->consoleOutput->writeln("All the logs we think are relevant are below: ");

        foreach ($logs as $log) {
            // make a guess whether the log is a combined log or not.
            $combined = (!Str::contains($log->title, 'serveme.tf') || Str::contains(strtolower($log->title), 'combined')) ? '(combined)' : '';
            $this->consoleOutput->writeln("$log->url " . $combined);
        }

        $notFound = $this->etf2lLogRetrievalService->getNotFoundLogs();

        $this->consoleOutput->writeln("!!! No logs were found for: ");
        foreach ($notFound as $item) {
            $this->consoleOutput->writeln("$item->match_url - $item->map - $item->match_date");
        }
        $this->consoleOutput->writeln("Please review these matches manually.");
    }
}
