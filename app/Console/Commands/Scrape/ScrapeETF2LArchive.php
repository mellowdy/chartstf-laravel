<?php

namespace App\Console\Commands\Scrape;

use App\Models\User;
use App\Repositories\Leagues\Euro\ETF2LRepo;
use App\Repositories\Logs\LogsTF\LogsRepo;
use App\Services\LogRetrieval\ETF2LLogRetrievalService;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class ScrapeETF2LArchive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:etf2l-archive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes competitions from the ETF2L archive & attempts to get logs';

    protected ETF2LRepo $etf2lRepo;

    protected LogsRepo $logsRepo;

    protected ConsoleOutput $consoleOutput;

    protected ETF2LLogRetrievalService $etf2lLogRetrievalService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ETF2LRepo $etf2lRepo, LogsRepo $logsRepo, ConsoleOutput $consoleOutput, ETF2LLogRetrievalService $etf2lLogRetrievalService)
    {
        parent::__construct();

        $this->etf2lRepo                = $etf2lRepo;
        $this->logsRepo                 = $logsRepo;
        $this->consoleOutput            = $consoleOutput;
        $this->etf2lLogRetrievalService = $etf2lLogRetrievalService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        // this will get the competitions from the archive that we are interested in

        //$data = $this->etf2lRepo->scrapeArchive();
        //$this->consoleOutput->writeln("Archive has the following data: " . json_encode($data));

        // this will get the awards from that competition (aka who won & who lost)
        //$awards = $this->etf2lRepo->scrapeAwardsFromCompetition(743);
        //$this->consoleOutput->writeln("Archive has the following awards: " . json_encode($awards));

        // this will get all the relevant matches for the competitions
        $matches = $this->etf2lRepo->scrapeMatchesFromCompetition(691);
        $this->consoleOutput->writeln("Archive has the following matches: " . json_encode($matches));

        // this will get all the relevant data from the match itself
        //$match = $this->etf2lRepo->scrapeMatchInformation(81311);
        //$this->consoleOutput->writeln("Archive has the following match: " . json_encode($match));

        // Get a list of all competitions.
        // A lot of them will be filtered out.

        /*$archivedCompetitions = $this->etf2lRepo->scrapeArchive();

        foreach ($archivedCompetitions as $archivedCompetition) {
            $this->consoleOutput->writeln("Competition: " . $archivedCompetition->title);

            $confirmed = $this->confirm("Do you want to create a collection for the competition " . $archivedCompetition->title . "?");

            if ($confirmed) {
                $this->consoleOutput->writeln("Fetching matches for " . $archivedCompetition->title . ", please wait a second.");

                $this->etf2lLogRetrievalService->getLogsFromCompetition($archivedCompetition->competition_id);
            }
        }*/
    }
}
