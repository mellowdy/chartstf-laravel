<?php

namespace App\Console\Commands\Collections;

use App\Models\Collection;
use App\Models\CompetitionParticipation;
use App\Models\LeagueMatch;
use App\Models\LogMatch;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class LinkLogsToMatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collections:link-logs-to-teams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tries to reason to which team & match a log belongs to.';

    protected ConsoleOutput $consoleOutput;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->consoleOutput = new ConsoleOutput();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $peopleWithLogs = DB::table('player_statlines')->select('steam_id as steam_id_user')->distinct()->get();
        $collections    = Collection::with(['logs', 'logs.statlines'])->has('competitionParticipations')->get();
        /*$collections = Collection::with(['logs', 'logs.statlines' => function ($q) {
            $q->where(function ($query) {
                $query->select('steam_id')
                      ->from('logs')
                      ->whereColumn('player_statlines.steam_id', 'competition_participations.steam_id');
            });
        }])->has('competitionParticipations')->get();*/

        foreach ($collections as $collection) {
            foreach ($collection->logs as $log) {
                /*$redPlayers  = $log->statlines
                    ->where('team', 'Red')
                    ->pluck('steam_id')
                    ->toArray();
                $bluePlayers = $log->statlines
                    ->where('team', 'Blue')
                    ->pluck('steam_id')
                    ->toArray();*/

                $mapToSearch = $log->map;

                if (Str::substrCount($log->map, '_') >= 2) {
                    $mapToSearch = Str::beforeLast($log->map, '_');
                };

                $query = LeagueMatch::whereHas('competitionParticipations', function (Builder $q) use ($log, $peopleWithLogs) {
                    $q->whereIn('steam_id', array_intersect($log->statlines->pluck('steam_id')->toArray(), collect($peopleWithLogs)->pluck('steam_id_user')->toArray()));
                })
                                    ->where('maps_played', 'like', '%' . $mapToSearch . '%')
                                    ->where(DB::raw("CONVERT_TZ(`scheduled_date`, 'Europe/Brussels','UTC')"), '<', $log->date)
                                    ->where('scheduled_date', '>', $log->date->toImmutable()->subHours(12)->format('Y-m-d H:i:s'));

                $match = $query->first();

                //$this->consoleOutput->writeln(Str::replaceArray('?', collect($query->getBindings())->map(fn ($item) => "'" . $item . "'")->toArray(), $query->toSql()));

                if ($match instanceof LeagueMatch) {
                    $this->consoleOutput->writeln("The log matcher thinks that log " . $log->id . " on map " . $log->map . " was for match " . $match->name);
                    try {
                        $match->logs()->attach($log);
                    } catch (\Exception $ex) {
                        $this->consoleOutput->writeln("An error occurred: " . $ex->getMessage());
                    }
                } else {
                    $this->consoleOutput->writeln("No matching league match game was found for https://logs.tf/" . $log->id . ". Please link these manually.");
                }
                /*$aRedPlayer = CompetitionParticipation::with('team')
                                                      ->whereIn('steam_id', $redPlayers)
                                                      ->first();

                $aBluePlayer = CompetitionParticipation::with('team')
                                                       ->whereIn('steam_id', $bluePlayers)
                                                       ->first();

                $redTeam  = $aRedPlayer->team->name;
                $blueTeam = $aBluePlayer->team->name;

                $this->consoleOutput->writeln()*/
            }
        }
    }
}
