<?php

namespace App\Console\Commands\RGL;

use App\Repositories\Leagues\Burger\RGLRepo;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class ScrapeRGLMatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rgl:scrape-season';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes a season of RGL';

    protected RGLRepo $rglRepo;

    protected ConsoleOutput $consoleOutput;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RGLRepo $rglRepo, ConsoleOutput $consoleOutput)
    {
        parent::__construct();
        $this->rglRepo       = $rglRepo;
        $this->consoleOutput = new ConsoleOutput();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Exception
     */
    public function handle()
    {
        $season = (int)$this->ask('What season of RGL do you want to scrape?');

        $division = $this->ask('For which division?', 'Invite');

        $logs = $this->rglRepo->scrapeMatchesPlayedBySeason($season, $division);

        $this->consoleOutput->writeln("All the logs we think are relevant are below: ");

        foreach ($logs as $log) {
            // make a guess whether the log is a combined log or not.
            $combined = (!Str::contains($log->title, 'serveme.tf') || Str::contains(strtolower($log->title), 'combined')) ? '(combined)' : '';
            $this->consoleOutput->writeln("$log->url " . $combined . " | " . $log->title . " | ". $log->match->match . " | "  . $log->match->match_date_text);
        }

        $notFound = $this->rglRepo->getNotFoundLogs();

        $this->consoleOutput->writeln("!!! No logs were found for: ");
        foreach ($notFound as $item) {
            $this->consoleOutput->writeln($item->match->match_url . " - $item->map - " . $item->match->match_date_text);
        }
        $this->consoleOutput->writeln("Please review these matches manually.");

        return self::SUCCESS;
    }
}
