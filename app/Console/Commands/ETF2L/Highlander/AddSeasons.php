<?php

namespace App\Console\Commands\ETF2L\Highlander;

use App\Models\Collection;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Illuminate\Console\Command;
use Spatie\TemporaryDirectory\Exceptions\PathAlreadyExists;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddSeasons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etf2l:hl-seasons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed Highlander seasons based on list of logs.';

    /**
     * @var ConsoleOutput $consoleOutput
     */
    protected ConsoleOutput $consoleOutput;

    /**
     * @var LogsRepo $logsRepo
     */
    protected LogsRepo $logsRepo;

    protected int $delay = 10;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(LogsRepo $logsRepo, ConsoleOutput $consoleOutput)
    {
        parent::__construct();

        $this->logsRepo      = $logsRepo;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws PathAlreadyExists
     */
    public function handle(): int
    {
        // These are the S24 logs.
        $s24LogsPrem = [2924667, 2924692, 2929176, 2929142, 2929182, 2929155, 2935664, 2935692, 2935632, 2935659, 2935654, 2935674, 2943030, 2942993, 2942104, 2942083, 2943000, 2943039, 2943066, 2948264, 2948306, 2945648, 2945667, 2948271, 2948299, 2954451, 2954430, 2954412, 2954427, 2954409, 2951804, 2951775];
        $s23LogsPrem = [2819276, 2819317, 2821328, 2821309, 2820222, 2820247, 2826549, 2826608, 2826525, 2826584, 2828640, 2828684, 2833867, 2833837, 2834872, 2834848, 2833808, 2833781, 2841473, 2841428, 2841409, 2839236, 2839226, 2841444, 2841416, 2848862, 2848807, 2851874, 2851911, 2848830, 2848891];
        $s22LogsPrem = [2676195, 2676179, 2680207, 2680179, 2679150, 2679122, 2682987, 2682957, 2682972, 2683021, 2682956, 2683001, 2689931, 2689893, 2689876, 2689957, 2689934, 2689895, 2692897, 2692867, 2696918, 2696955, 2695925, 2695963, 2694980, 2694925, 2703813, 2703794, 2703808, 2703783, 2703817, 2703800, 2703777];
        $s21LogsPrem = [2514764, 2514804, 2514774, 2514821, 2514851, 2510581, 2510606, 2522291, 2522319, 2518910, 2518958, 2522294, 2522307, 2522362, 2529599, 2529632, 2526553, 2526598, 2526559, 2526545, 2532677, 2532708, 2532729, 2535743, 2535707, 2532715, 2532669, 2539853, 2539820, 2539830, 2539814, 2543055, 2543035];
        $s20LogsPrem = [2417985, 2417968, 2420081, 2420064, 2461088, 2421834, 2421809, 2422350, 2422365, 2421819, 2421838, 2428687, 2428703, 2426699, 2426715, 2427230, 2427254, 2430828, 2430808, 2430859, 2430833, 2432062, 2432085, 2434015, 2434029, 2434042, 2434534, 2434550, 2435126, 2435139];
        $s19LogsPrem = [2343306, 2374534, 2343324, 2340486, 2346384, 2346353, 2346331, 2346357, 2361104, 2352031, 2352026, 2364089, 2364112, 2361830, 2361852, 2361834, 2361810, 2366742, 2366245];
        $s18LogsPrem = [2240825, 2240862, 2240838, 2240859, 2240860, 2240841, 2240809, 2242854, 2242807, 2245849, 2245886, 2245854, 2245892, 2248571, 2248549, 2248530, 2248516, 2248538, 2250928, 2250936, 2253578, 2253542, 2255980, 2256006, 2255991, 2256027, 2258851, 2258833, 2261255, 2261224, 2261203, 2261223, 2261194];
        $s17LogsPrem = [2158906, 2158930, 2161072, 2161105, 2161128, 2161080, 2161099, 2162936, 2162900, 2165469, 2165504, 2165476, 2165450, 2167713, 2167733, 2167692, 2167716, 2167734, 2167688, 2167714, 2172106, 2172133, 2174096, 2174125, 2174094, 2174117, 2175197, 2175217, 2175238, 2176276, 2176300];
        $s16LogsPrem = [2080357, 2082727, 2082761, 2087833, 2087803, 2087806, 2087832, 2092980, 2092986, 2092919, 2095789, 2095822, 2098210, 2098242, 2098246, 2098208, 2099590, 2100263];
        $s15LogsPrem = [2002106, 1989038, 1988372, 2001996, 2002027, 2001407, 2001444, 1998618, 1998646, 1999251, 2002107, 2001325, 2005682, 2005705, 2005706, 2005662, 2005654, 2005701, 2010328, 2009939, 2009957];
        $s14LogsPrem = [1882128, 1884716, 1884757, 1886848, 1886813, 1890148, 1890180, 1890172, 1890202, 1896096, 1895590, 1895567, 1900912, 1900962, 1897625, 1897634, 1897598, 1898209, 1898233, 1906212, 1906175, 1906189];
        $s13LogsPrem = [1782620, 1787221, 1787158, 1799768, 1793664, 1793613, 1800331, 1799905, 1805908, 1805858, 1800763, 1800802, 1805907, 1805851, 1811654, 1811683, 1811672];
        $s12LogsPrem = [1690439, 1687333, 1689916, 1689891, 1689879, 1689917, 1690748, 1690726, 1693986, 1697743, 1694052, 1710173, 1706310, 1707405, 1710801, 1712914, 1712866];
        $s11LogsPrem = [1562099, 1558575, 1562046, 1562080, 1564676, 1564718, 1564026, 1564056, 1567538, 1567472, 1572711, 1570241, 1572727, 1577053, 1577093, 1577062, 1577098, 1574357, 1574400, 1581803, 1582174, 1578817, 1583653, 1583680, 1583661, 1583702, 1586858, 1586893, 1588762, 1591642, 1592040];
        $s10LogsPrem = [1331243, 1333719, 1332537, 1338464, 1343956, 1344033, 1344001, 1343927, 1348473, 1348566, 1348467, 1348556, 1356924, 1352524, 1352603, 1356863, 1364549, 1364564, 1359431, 1359460, 1368951, 1368931, 1366639, 1373445, 1374595, 1377928, 1377973, 1382028, 1386362, 1386446, 1385439, 1385394, 1389687, 1389726, 1385397, 1385528];
        $s09LogsPrem = [1094313, 1103284, 1095780, 1096321, 1105784, 1105740, 1113070, 1113151, 1112903, 1112996, 1113019, 1112945, 1116479, 1118814, 1118141, 1116956, 1134321, 1134333, 1134284, 1127333, 1144230, 1137313, 1143413, 1138451, 1153658, 1153530, 1147462, 1154363, 1165214, 1165300, 1158962, 1158911, 1157565, 1157629, 1158872, 1158942];
        $s08LogsPrem = [742023, 742080, 742137, 742078, 736401, 742029, 742097, 745515, 754054, 754217, 748606, 748552, 754060, 754172, 766731, 766694, 767877, 759362, 759294, 766736, 766690, 776755, 776707, 779603, 779545, 776730, 776688, 776845, 786936, 782040, 781988, 786912, 786945, 786964, 786933, 786940, 797876, 797742, 792370, 792318, 792437, 792351, 797789, 797863, 804080, 804019, 810645, 810562, 810555, 810606, 810655, 810612, 810565, 810531];

        $this->logsRepo->createUsersForSteamIds([76561198072250111]);

        $this->s05Prem();
        $this->s06Prem();
        $this->s07Prem();


        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S08 Main Season");

        /**
         * @var Collection $collection
         */
        $s08Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S08 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s08LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s08Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S09 Main Season");

        /**
         * @var Collection $collection
         */
        $s09Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S09 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s09LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s09Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S10 Main Season");

        /**
         * @var Collection $collection
         */
        $s10Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S10 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s10LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s10Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S11 Main Season");

        /**
         * @var Collection $collection
         */
        $s11Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S11 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s11LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s11Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S12 Main Season");

        /**
         * @var Collection $collection
         */
        $s12Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S12 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s12LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s12Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S13 Main Season");

        /**
         * @var Collection $collection
         */
        $s13Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S13 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s13LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s13Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S14 Main Season");

        /**
         * @var Collection $collection
         */
        $s14Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S14 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s14LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s14Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S15 Main Season");

        /**
         * @var Collection $collection
         */
        $s15Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S15 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s15LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s15Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S16 Main Season");

        /**
         * @var Collection $collection
         */
        $s16Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S16 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s16LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s16Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S17 Main Season");

        /**
         * @var Collection $collection
         */
        $s17Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S17 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s17LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s17Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S18 Main Season");

        /**
         * @var Collection $collection
         */
        $s18Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S18 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s18LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s18Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S19 Main Season");

        /**
         * @var Collection $collection
         */
        $s19Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S19 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s19LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s19Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S20 Main Season");

        /**
         * @var Collection $collection
         */
        $s20Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S20 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s20LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s20Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S21 Main Season");

        /**
         * @var Collection $collection
         */
        $s21Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S21 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s21LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s21Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S22 Main Season");

        /**
         * @var Collection $collection
         */
        $s22Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S22 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s22LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s22Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S23 Main Season");

        /**
         * @var Collection $collection
         */
        $s23Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S23 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s23LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s23Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S24 Main Season");

        /**
         * @var Collection $collection
         */
        $s24Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S24 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s24LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s24Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }

        return self::SUCCESS;
    }

    private function s07Prem()
    {
        $s07LogsPrem = [536233, 536294, 530240, 526777, 526560, 526466, 564232, 564319, 541029, 540951, 545788, 545878, 558857, 558974, 555048, 555121, 550340, 550371, 550433, 555123, 555176, 555256, 550464, 560067, 560096, 576511, 576520, 568332, 576619, 576654, 572427, 572469, 585851, 585817, 572494, 574739, 574777, 580849, 580917, 580798, 580884, 599562, 599632, 584528, 584549, 590494];

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S07 Main Season");

        /**
         * @var Collection $collection
         */
        $s07Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S07 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s07LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s07Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }
    }

    private function s06Prem()
    {
        $s06LogsPrem = [210466, 210547, 216535, 216573, 208758, 212157, 212200, 220621, 220739, 220646, 220774, 220652, 220753, 221046, 228616, 228569, 228580, 228631, 228661, 241818, 254606, 235326, 246697, 246732, 246747, 235740, 255835, 243234, 243268, 243497, 243315, 243376, 267542, 267420, 267305, 267399, 267436, 267389, 251549, 251633, 259384, 259314, 259370, 260239];

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S06 Main Season");
        /**
         * @var Collection $collection
         */
        $s06Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S06 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s06LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s06Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }
    }

    private function s05Prem()
    {
        $s05LogsPrem = [129648, 157055, 157083, 133486, 133512, 123036, 123109, 123032, 122991, 122966, 129647, 129649, 127920, 127957, 126928, 126990, 131169, 131210, 131245, 131136, 131171, 131005, 131013, 131026, 136697, 136739, 136768, 155653, 155694, 136538, 136513, 140569, 140547, 140403, 140381, 138821, 138808, 144475, 144633, 144653, 153432, 153461, 155096, 155066, 155027, 156959, 156976, 157049, 157035];

        $this->consoleOutput->writeln("Parsing ETF2L Premiership Highlander S05 Main Season");
        /**
         * @var Collection $collection
         */
        $s05Collection = Collection::create(
            [
                'steam_id' => 76561198072250111,
                'name'     => 'ETF2L Premiership Highlander S05 Main Season',
                'gamemode' => 'Highlander'
            ]
        );

        foreach ($s05LogsPrem as $logId) {
            $log = $this->logsRepo->parseLogById($logId);
            $s05Collection->logs()->attach($log->id);
            //sleep($this->delay);
        }
    }
}
