<?php

namespace App\Console\Commands\ETF2L;

use App\Models\Collection;
use App\Models\CompetitionParticipation;
use App\Models\LeagueMatch;
use App\Models\Team;
use App\Models\User;
use App\Repositories\Leagues\Euro\ETF2LRepo;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddTeamsToCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etf2l:add-teams-to-collection';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adds teams to collections';

    protected LogsRepo $logsRepo;

    protected ETF2LRepo $etf2lRepo;

    protected ConsoleOutput $consoleOutput;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(LogsRepo $logsRepo, ETF2LRepo $etf2lRepo, ConsoleOutput $consoleOutput)
    {
        parent::__construct();
        $this->logsRepo      = $logsRepo;
        $this->etf2lRepo     = $etf2lRepo;
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collections = Collection::where('league', 'ETF2L')
                                 ->whereNotNull('competition_id')
                                 ->doesntHave('competitionParticipations')
                                 ->whereNotNull('division')
                                 ->get();

        foreach ($collections as $collection) {
            // get the matches, players & teams and add them to the db
            $this->consoleOutput->writeln("Checking matches for collection " . $collection->name);
            $matches = $this->etf2lRepo->scrapeMatchesFromCompetition($collection->competition_id, $collection->division);

            // loop through matches
            foreach ($matches as $match) {
                $matchInfo = $this->etf2lRepo->scrapeMatchInformation($match->match_id);

                if (count($matchInfo->players) > 0) {
                    $score = trim(Str::after($matchInfo->name, $matchInfo->teams[1]->team_name));

                    [$homeScore, $awayScore] = explode('-', $score);

                    $leagueMatch = LeagueMatch::create(
                        [
                            'name'             => 'Match #' . $match->match_id . ": " . $match->division . " (" . $matchInfo->name . ")",
                            'home_team_name'   => $matchInfo->teams[0]->team_name,
                            'home_team_score'  => (int)$homeScore ?? null,
                            'away_team_name'   => $matchInfo->teams[1]->team_name,
                            'away_team_score'  => (int)$awayScore ?? null,
                            'league_match_id'  => $match->match_id,
                            'league_match_url' => $match->match_url,
                            'week'             => $matchInfo->week,
                            'maps_played'      => $matchInfo->maps_played,
                            'division'         => $collection->division,
                            'scheduled_date'   => Carbon::createFromFormat('d-m-Y H:i:s', $match->match_date)->format('Y-m-d H:i:s'),
                            'collection_id'    => $collection->id
                        ]
                    );

                    foreach ($matchInfo->teams as $i => $teamInfo) {
                        $this->waitAWhile(2);

                        // Check if the team ID is valid.
                        if (is_integer($teamInfo->team_id)) {
                            $teamDetails = $this->etf2lRepo->getTeamInfo($teamInfo->team_id);

                            $team = Team::updateOrCreate(
                                [
                                    'league_team_id' => $teamInfo->team_id,
                                    'league'         => 'ETF2L',
                                    'gamemode'       => $teamDetails->team->type
                                ],
                                [
                                    'name'         => $teamInfo->team_name,
                                    'team_url'     => $teamInfo->team_url,
                                    'tag'          => $teamDetails->team->tag,
                                    'country'      => $teamDetails->team->country,
                                    'team_picture' => Str::replace('http://etf2l.org/images/avatars/', 'https://etf2l.org/wp-content/uploads/avatars/', $teamDetails->team->steam->avatar)
                                ]
                            );

                            $playersOnTeam = collect($matchInfo->players)->where('team_name', $teamInfo->team_name)->values();

                            foreach ($playersOnTeam as $playerOnTeam) {
                                $user = User::where('etf2l_id', $playerOnTeam->player_id)->first();

                                if ($user instanceof User) {
                                    CompetitionParticipation::create(
                                        [
                                            'team_id'       => $team->id,
                                            'steam_id'      => $user->id,
                                            'collection_id' => $collection->id,
                                            'match_id'      => $leagueMatch->id
                                        ]
                                    );

                                    if ($i === 0) {
                                        $leagueMatch->home_team_id = $team->id;
                                    } else {
                                        $leagueMatch->away_team_id = $team->id;
                                    }
                                }
                            }
                        }
                    }

                    $unrostered = collect($matchInfo->players)->where('team_name', 'Unrostered')->values();

                    foreach ($unrostered as $unrosteredPlayer) {
                        $user = User::where('etf2l_id', $unrosteredPlayer->player_id)->first();

                        if ($user instanceof User) {
                            CompetitionParticipation::create(
                                [
                                    'steam_id'      => $user->id,
                                    'collection_id' => $collection->id,
                                    'match_id'      => $leagueMatch->id,
                                    'unrostered'    => 1
                                ]
                            );
                        }
                    }

                    $leagueMatch->save();

                    $this->waitAWhile(4);
                }
            }
            $this->consoleOutput->writeln("Finishing collection $collection->name ...");
            $this->waitAWhile(15);
        }
    }

    private function waitAWhile(int $sleep = 3)
    {
        sleep($sleep);
    }
}
