<?php

namespace App\Console\Commands;

use App\Models\Collection;
use App\Models\PlayerStatline;
use App\Models\User;
use App\Repositories\Leagues\Aussie\OzFortressRepo;
use App\Repositories\Leagues\Burger\RGLRepo;
use App\Repositories\Leagues\Euro\ETF2LRepo;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class FixAliases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aliases:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attempts to correct aliases that were previously fetched incorrectly.';

    /**
     * @var ConsoleOutput $consoleOutput
     */
    protected ConsoleOutput $consoleOutput;

    protected ETF2LRepo $etf2lRepo;

    protected RGLRepo $rglRepo;

    protected OzFortressRepo $ozFortressRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->consoleOutput  = new ConsoleOutput();
        $this->etf2lRepo      = new ETF2LRepo();
        $this->rglRepo        = new RGLRepo();
        $this->ozFortressRepo = new OzFortressRepo();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->consoleOutput->writeln("Updating aliases for ozfortress players");

        $ozPlayers = User::whereIn('id', PlayerStatline::select('steam_id')->whereHas('log.collections', function (Builder $q) {
            $q->where('league', 'ozfortress');
        })
                                                       ->distinct()
                                                       ->get()
                                                       ->pluck('steam_id')
                                                       ->toArray()
        )
                         ->where('updated_at', '<=', now()->subHours(6)->format('Y-m-d H:i:s'))
                         ->get();

        foreach ($ozPlayers as $ozPlayer) {
            $info = $this->ozFortressRepo->getUserInfoBySteamId($ozPlayer->id);

            if (isset($info->name)) {
                $this->consoleOutput->writeln("Updating alias to " . $info->name . " for " . $ozPlayer->id);
                $ozPlayer->update(['name' => Str::transliterate($info->name), 'ozfortress_id' => $info->ozfortress_id]);
                sleep(1);
            }
        }

        $this->consoleOutput->writeln("Updating aliases for RGL players");

        $rglPlayers = User::whereIn('id', PlayerStatline::select('steam_id')->whereHas('log.collections', function (Builder $q) {
            $q->where('league', 'RGL');
        })
                                                        ->distinct()
                                                        ->get()
                                                        ->pluck('steam_id')
                                                        ->toArray()
        )
                          ->where('updated_at', '<=', now()->subHours(6)->format('Y-m-d H:i:s'))
                          ->get();

        foreach ($rglPlayers as $rglPlayer) {
            $info = $this->rglRepo->scrapePlayerInfoBySteamId($rglPlayer->id);

            if (isset($info->name)) {
                $this->consoleOutput->writeln("Updating alias to " . $info->name . " for " . $rglPlayer->id);
                $rglPlayer->update(['name' => Str::transliterate($info->name)]);
                sleep(1);
            }
        }

        $this->consoleOutput->writeln("Updating aliases for ETF2L players");

        $etf2lPlayers = User::whereIn('id', PlayerStatline::select('steam_id')->whereHas('log.collections', function (Builder $q) {
            $q->where('league', 'ETF2L');
        })
                                                          ->distinct()
                                                          ->get()
                                                          ->pluck('steam_id')
                                                          ->toArray()
        )
                            ->where('updated_at', '<=', now()->subHours(6)->format('Y-m-d H:i:s'))
                            ->get();

        foreach ($etf2lPlayers as $etf2lPlayer) {
            $info = $this->etf2lRepo->getUserInfo($etf2lPlayer->id);
            if (isset($info->player->name)) {
                $this->consoleOutput->writeln("Updating alias to " . $info->player->name . " for " . $etf2lPlayer->id);
                $etf2lPlayer->update(['name' => Str::transliterate($info->player->name)]);
                sleep(1);
            }
        }
    }
}
