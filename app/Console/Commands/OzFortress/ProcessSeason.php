<?php

namespace App\Console\Commands\OzFortress;

use App\Repositories\Leagues\Aussie\OzFortressRepo;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class ProcessSeason extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ozfortress:process-season';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Looks for logs for ozfortress season (internally they refer to these as leagues)';

    protected ConsoleOutput $consoleOutput;

    protected OzFortressRepo $ozFortressService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->consoleOutput     = new ConsoleOutput();
        $this->ozFortressService = new OzFortressRepo();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $seasonId = (int)$this->ask('Which ozfortress season do you want to process? (numeric) Please use https://ozfortress.com/leagues as a reference.');

        $matches = $this->ozFortressService->getLeagueMatchesForSeason($seasonId);
        $this->consoleOutput->writeln(json_encode($matches));
    }
}
