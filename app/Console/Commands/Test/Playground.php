<?php

namespace App\Console\Commands\Test;

use App\Models\Collection;
use App\Repositories\Leagues\Euro\ETF2LRepo;
use App\Repositories\Logs\LogsTF\LogsRepo;
use Illuminate\Console\Command;
use Spatie\TemporaryDirectory\Exceptions\PathAlreadyExists;

class Playground extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:playground';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing random stuff';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Test a log in isolation.
     *
     * @return int
     */
    public function handle()
    {
        $logId = (int) $this->ask('Which log do you want to re-do?');
        (new LogsRepo())->deleteLog($logId);
        (new LogsRepo())->parseLogById($logId);
        return 0;
    }
}
