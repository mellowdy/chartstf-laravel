<?php

namespace App\Console\Commands\DemosTf;

use App\Models\Demo;
use App\Models\Log;
use App\Repositories\Demos\DemosTf\DemosTfRepo;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Output\ConsoleOutput;

class GetDemosForLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demostf:get-demos-from-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attempts to retrieve demos from demos.tf based on the logs within the system (if no demos are linked yet)';

    protected ConsoleOutput $consoleOutput;

    protected DemosTfRepo $demosTfRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->consoleOutput = new ConsoleOutput();
        $this->demosTfRepo   = new DemosTfRepo();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $logs = Log::with(['statlines' => function ($q) {
            $q->select('id', 'steam_id', 'log_id');
        }])
                   ->whereNotNull('map')
                   ->doesntHave('demos')
                   ->orderByDesc('started_at')
                   ->get();

        foreach ($logs as $log) {
            $this->consoleOutput->writeln("Looking for demo of log https://logs.tf/" . $log->id . " on map " . $log->map);

            $demos = $this->demosTfRepo->searchDemosByLog($log);

            foreach ($demos as $demoInfo) {
                $this->consoleOutput->writeln("Demo was found!");
                $identifiers = [
                    'demostf_id' => $demoInfo['id']
                ];

                /**
                 * @var Demo $demo
                 */
                $demo = Demo::updateorCreate(
                    $identifiers,
                    [
                        'download_url'  => $demoInfo['url'],
                        'server'        => $demoInfo['server'],
                        'duration'      => $demoInfo['duration'],
                        'demo_provider' => 'demos.tf',
                        'demo_type'     => 'SourceTV Demo',
                        'map'           => $demoInfo['map'],
                        'date'          => Carbon::parse($demoInfo['time']),
                        'red_name'      => $demoInfo['red'],
                        'blu_name'      => $demoInfo['blue'],
                        'player_count'  => $demoInfo['playerCount'],
                    ]
                );
                $demo->logs()->attach($log);
            }
            sleep(2);
        }
    }
}
