<?php

namespace App\Console\Commands\Logs;

use App\Enums\Formats;
use App\Enums\GameModes;
use App\Models\Log;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class DetermineWinners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:determine-winners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Based on the data we have: determine who won the game (Blue or Red)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $logs = Log::with(['rounds', 'statlines'])
                   ->get();
        foreach ($logs as $log) {
            // The winner depends on the game type & map.
            // We have King of the Kill, CP, payload & attack/defend.

            // Honestly I don't know how I'm going to differentiate between atk/defend maps and control points,
            // so I'm making a small list of known atk/defend maps and just checking against that.
            $atkDefendMaps = [
                'cp_altitude',
                'cp_dustbowl',
                'cp_egypt',
                'cp_erebus',
                'cp_gorge',
                'cp_gorge_event',
                'cp_gravelpit',
                'cp_junction',
                'cp_hadal',
                'cp_hadal_b13a',
                'cp_hadal_b2',
                'cp_hadal_b8',
                'cp_hadal_b9',
                'cp_hadal_b5a',
                'cp_hadal_b9a',
                'cp_manor_event',
                'cp_mountainlab',
                'cp_mossrock',
                'cp_mercenarypark',
                'cp_steel',
                'cp_steel_f1',
                'cp_steel_f2',
                'cp_steel_f3',
                'cp_steel_f4',
                'cp_steel_f5',
                'cp_steel_f6',
                'cp_steel_f5a',
                'cp_steel_f7',
                'cp_steel_f8'
            ];

            $gamemode = match (true) {
                in_array($log->map, $atkDefendMaps, true) => GameModes::ATK_DEF,
                Str::startsWith($log->map, 'ctf_') => GameModes::CTF,
                Str::startsWith($log->map, 'pass_') => GameModes::PASS_TIME,
                Str::startsWith($log->map, 'tc_') => GameModes::TERRITORIAL_CONTROL,
                Str::startsWith($log->map, 'pd_') => GameModes::PLAYER_DESTRUCTION,
                Str::startsWith($log->map, 'plr_') => GameModes::PAYLOAD_RACE,
                Str::startsWith($log->map, 'pl_') => GameModes::PAYLOAD,
                Str::startsWith($log->map, 'koth_') => GameModes::KING_OF_THE_HILL,
                Str::startsWith($log->map, 'cp_') => GameModes::CONTROL_POINTS,
                default => null
            };

            $format = match ($log->statlines->count()) {
                18 => Formats::HIGHLANDER,
                12 => Formats::SIXES,
                8 => Formats::FOURS,
                4 => Formats::ULTIDUO,
                2 => Formats::MGE,
                14 => Formats::PROLANDER,
                default => Formats::CUSTOM
            };

            $winner = match ($gamemode) {
                // winner of the last round
                GameModes::PAYLOAD, GameModes::ATK_DEF => optional($log->rounds->last())->winner,
                // whoever has the highest score (most rounds won)
                GameModes::CONTROL_POINTS, GameModes::KING_OF_THE_HILL, GameModes::CTF => $log->red_score > $log->blu_score ? 'Red' : 'Blue',
                // ties/troll gamemodes (that were never played) don't count
                default => null
            };

            $log->update(
                [
                    'gamemode_id'  => $gamemode,
                    'format_id'    => $format,
                    'winning_team' => $winner
                ]
            );
        }
    }
}
