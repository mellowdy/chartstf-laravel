<?php

//use Illuminate\Foundation\Application;
//use Illuminate\Support\Facades\Route;
//use Inertia\Inertia;

// FAQ/OTHER
//Route::get('/', 'HomeController@index')->name('home');

// PLAYERS
//Route::get('/players/{user}', 'PlayerController@show')->name('player.detail');

// LOGS
//Route::get('/logs', 'LogController@index')->name('logs');
//Route::get('/logs/{log}', 'LogController@show')->name('log.detail');

// PLAYERS
//Route::get('/players', 'PlayerController@index')->name('players');

// COLLECTIONS
//Route::get('/collections', 'CollectionController@index')->name('collections');
//Route::get('/collections/{collection:slug}', 'CollectionController@show')->name('collection.detail');
//Route::get('/collections/export/{collection:slug}', 'CollectionController@export')->name('export.collection');

// TEAMS
//Route::get('/teams', 'TeamController@index')->name('teams');
//Route::get('/teams/{team}', 'TeamController@show')->name('team.detail');

/*Route::middleware(['auth:sanctum', \App\Http\Middleware\IsAdmin::class])->group(function () {
    Route::get('/collection/new', 'CollectionController@create')->name('create.collection');
    Route::post('/collections', 'CollectionController@store')->name('store.collection');
});*/

// HANDLE STEAM LOG IN
//Route::get('auth/steam', 'AuthController@redirectToSteam')->name('auth.steam');
//Route::get('auth/steam/handle', 'AuthController@handle')->name('auth.steam.handle');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('{any}', 'HomeController@index')->name('home')->where('any', '.*');