<table>
    <thead>
    <tr>
        <th>Player</th>
        <th>SteamID</th>
        <th>Time played</th>
        <th>Kills</th>
        <th>Assists</th>
        <th>Deaths</th>
        <th>K/m</th>
        <th>A/m</th>
        <th>D/m</th>
        <th>K/d</th>
        <th>KA/d</th>
        <th>DMG</th>
        <th>DPM</th>
        <th>DT</th>
        <th>DT/m</th>
        <th>HS</th>
        <th>HS/m</th>
        <th>Longest killstreak</th>
        <th>Heals received</th>
        <th>Avg heals received/m</th>
        <th>Medkit health</th>
        <th>Medkit pickup</th>
        <th>Point captures</th>
        <th>Sentries destroyed</th>
        <th>Sentries destroyed per minute</th>
        <th>Dispensers destroyed</th>
        <th>Dispensers destroyed per minute</th>
        <th>Teleporters destroyed</th>
        <th>Teleporters destroyed per minute</th>
        <th>Killed scouts</th>
        <th>Killed soldiers</th>
        <th>Killed pyros</th>
        <th>Killed demomen</th>
        <th>Killed heavies</th>
        <th>Killed engineers</th>
        <th>Killed medics</th>
        <th>Killed snipers</th>
        <th>Killed spies</th>
        <th>Deaths to scouts</th>
        <th>Deaths to soldiers</th>
        <th>Deaths to pyros</th>
        <th>Deaths to demomen</th>
        <th>Deaths to heavies</th>
        <th>Deaths to engineers</th>
        <th>Deaths to medics</th>
        <th>Deaths to snipers</th>
        <th>Deaths to spies</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $player)
        <tr>
            <td>{{ $player->common_name }}</td>
            <td>{{ $player->steam_id }}</td>
            <td>{{ $player->time_played }}</td>
            <td>{{ $player->total_kills }}</td>
            <td>{{ $player->total_assists }}</td>
            <td>{{ $player->total_deaths }}</td>
            <td>{{ $player->average_kills_per_minute }}</td>
            <td>{{ $player->average_assists_per_minute }}</td>
            <td>{{ $player->average_deaths_per_minute }}</td>
            <td>{{ $player->average_kills_per_death }}</td>
            <td>{{ $player->average_kills_assists_per_death }}</td>
            <td>{{ $player->total_damage }}</td>
            <td>{{ $player->average_damage_per_minute }}</td>
            <td>{{ $player->total_damage_taken }}</td>
            <td>{{ $player->average_damage_taken_per_minute }}</td>
            <td>{{ $player->total_headshots }}</td>
            <td>{{ $player->average_headshots_per_minute }}</td>
            <td>{{ $player->longest_killstreak }}</td>
            <td>{{ $player->total_heals_received }}</td>
            <td>{{ $player->average_heals_received_per_minute }}</td>
            <td>{{ $player->total_medkit_health }}</td>
            <td>{{ $player->total_medkit_pickup }}</td>
            <td>{{ $player->total_point_captures }}</td>
            <td>{{ $player->total_sentries_destroyed }}</td>
            <td>{{ $player->average_sentries_destroyed_per_minute }}</td>
            <td>{{ $player->total_dispensers_destroyed }}</td>
            <td>{{ $player->average_dispensers_destroyed_per_minute }}</td>
            <td>{{ $player->total_teleporters_destroyed }}</td>
            <td>{{ $player->average_teleporters_destroyed_per_minute }}</td>
            <td>{{ $player->total_scouts_killed }}</td>
            <td>{{ $player->total_soldiers_killed }}</td>
            <td>{{ $player->total_pyros_killed }}</td>
            <td>{{ $player->total_demomen_killed }}</td>
            <td>{{ $player->total_heavies_killed }}</td>
            <td>{{ $player->total_engineers_killed }}</td>
            <td>{{ $player->total_medics_killed }}</td>
            <td>{{ $player->total_snipers_killed }}</td>
            <td>{{ $player->total_spies_killed }}</td>
            <td>{{ $player->total_deaths_to_scouts }}</td>
            <td>{{ $player->total_deaths_to_soldiers }}</td>
            <td>{{ $player->total_deaths_to_pyros }}</td>
            <td>{{ $player->total_deaths_to_demomen }}</td>
            <td>{{ $player->total_deaths_to_heavies }}</td>
            <td>{{ $player->total_deaths_to_engineers }}</td>
            <td>{{ $player->total_deaths_to_medics }}</td>
            <td>{{ $player->total_deaths_to_snipers }}</td>
            <td>{{ $player->total_deaths_to_spies }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
