require('./bootstrap');

import {createApp, h} from 'vue';
import {createInertiaApp} from '@inertiajs/inertia-vue3';
import {InertiaProgress} from '@inertiajs/progress';

import VueApexCharts from "vue3-apexcharts";

import Echo from "laravel-echo"

/*window.Pusher = require('pusher-js');

window.Echo = new Echo({
                           broadcaster : 'pusher',
                           key         : 'myKey',
                           wsHost      : window.location.hostname,
                           wsPort      : 6001,
                           forceTLS    : false,
                           disableStats: true,
                       });*/

// icons font awesome
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faSkull,
    faArchive,
    faHome,
    faArrowDown,
    faArrowLeft,
    faArrowUp,
    faBan,
    faBold,
    faCalendarAlt,
    faCalendarDay,
    faCheck,
    faChevronCircleRight,
    faChevronDown,
    faChevronRight,
    faChevronUp,
    faCircleNotch,
    faComment,
    faCopy,
    faDownload,
    faEdit,
    faEllipsisV,
    faEnvelope,
    faExclamationTriangle,
    faEye,
    faEyeSlash,
    faFile,
    faFileAlt,
    faFileImage,
    faFolder,
    faFont,
    faGripLines,
    faGripLinesVertical,
    faHeading,
    faHourglass,
    faHourglassHalf,
    faHourglassStart,
    faInfoCircle,
    faItalic,
    faList,
    faListOl,
    faListUl,
    faParagraph,
    faPeopleArrows,
    faPlus,
    faSave,
    faSearch,
    faSortNumericUp,
    faSpinner,
    faStickyNote,
    faStrikethrough,
    faSync,
    faTheaterMasks,
    faThLarge,
    faTimes,
    faTrash,
    faUnderline,
    faUser,
    faUserCheck,
    faUserEdit,
    faUserSecret,
    faFlag,
    faBriefcaseMedical,
    faTrophy,
    faCarCrash
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText} from '@fortawesome/vue-fontawesome'

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

library.add(faFont, faCarCrash, faTrophy, faFlag, faBriefcaseMedical, faSkull, faHome, faArchive, faCircleNotch, faCopy, faSortNumericUp, faCheck, faCalendarDay, faCalendarAlt, faComment, faUser, faPlus, faPeopleArrows, faInfoCircle, faChevronCircleRight, faHourglassStart, faSync, faHourglassHalf, faBan, faEnvelope, faUserSecret, faUserCheck, faGripLines, faArrowUp, faEllipsisV, faFileAlt, faFolder, faSearch, faThLarge, faList, faDownload, faSave, faFileImage, faArrowDown, faUserEdit, faChevronUp, faSpinner, faEye, faEdit, faChevronDown, faChevronRight, faArrowLeft, faTimes, faEyeSlash, faHourglass, faFile, faStickyNote, faTrash, faTheaterMasks, faGripLinesVertical, faBold, faItalic, faStrikethrough, faUnderline, faParagraph, faListUl, faListOl, faHeading, faExclamationTriangle);

import {VueCookieNext} from 'vue-cookie-next'

import {ObserveVisibility} from 'vue-observe-visibility';

import VueTippy from 'vue-tippy'
import 'tippy.js/dist/tippy.css'

createInertiaApp({
                     title  : (title) => `${title} - ${appName}`,
                     resolve: (name) => require(`./Pages/${name}.vue`),
                     setup({el, app, props, plugin}) {
                         return createApp({render: () => h(app, props)})
                             .use(plugin)
                             .use(
                                 VueTippy,
                                 // optional
                                 {
                                     directive         : 'tippy', // => v-tippy
                                     component         : 'tippy', // => <tippy/>
                                     componentSingleton: 'tippy-singleton', // => <tippy-singleton/>,
                                     defaultProps      : {
                                         placement: 'auto-end',
                                         allowHTML: true,
                                     }, // => Global default options * see all props
                                 }
                             )
                             .use(VueApexCharts)
                             .use(VueCookieNext)

                             .component("font-awesome-icon", FontAwesomeIcon)
                             .component("font-awesome-layers", FontAwesomeLayers)
                             .component("font-awesome-layers-text", FontAwesomeLayersText)
                             .directive('observe-visibility', {
                                 beforeMount: (el, binding, vnode) => {
                                     vnode.context = binding.instance;
                                     ObserveVisibility.bind(el, binding, vnode);
                                 },
                                 update     : ObserveVisibility.update,
                                 unmounted  : ObserveVisibility.unbind,
                             })

                             .mixin({methods: {route}})
                             .mount(el);
                     },
                 });


InertiaProgress.init({color: '#8B5CF6'});
