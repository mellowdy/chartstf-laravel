export const sumPropertyValue = (items, prop) => items.reduce((a, b) => a + b[prop], 0);

export const roundNumber = (num) => Math.round((num + Number.EPSILON) * 100) / 100
