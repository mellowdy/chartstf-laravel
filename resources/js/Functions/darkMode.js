export function enableDarkMode() {
    // Whenever the user explicitly chooses dark mode
    localStorage.theme = 'dark'
    document.documentElement.classList.add('dark')
}

export function enableLightMode() {
    // Whenever the user explicitly chooses light mode
    localStorage.theme = 'light'
    document.documentElement.classList.remove('dark')
}

export function setInitialColorPreference() {
    // On page load or when changing themes, best to add inline in `head` to avoid FOUC
    if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
        document.documentElement.classList.add('dark')
    } else {
        document.documentElement.classList.remove('dark')
    }
}