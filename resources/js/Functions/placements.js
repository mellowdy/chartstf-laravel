export function getMax(prop, array) {
    /*if (prop === 'average_kills_per_death') {
        console.log({msg: 'max', prop, array})
    }*/
    return Math.max.apply(
        Math,
        [...new Set(array.map(function (o) {
            return o[prop]
        }))]
    )
}

export function getMin(prop, array) {
    return Math.min.apply(
        Math,
        [...new Set(array.map(function (o) {
            return o[prop]
        }))]
    )
}

export function getSecondMin(prop, array) {
    let tmp = [...new Set(array.map(e => {
        return e[prop]
    }))]
    tmp.sort((a, b) => {
        return a - b
    })
    if (typeof tmp[1] != 'undefined') {
        return tmp[1]
    } else {
        return false
    }
}

export function getThirdMin(prop, array) {
    let tmp = [...new Set(array.map(e => {
        return e[prop]
    }))]
    tmp.sort((a, b) => {
        return a - b
    })
    if (typeof tmp[2] != 'undefined') {
        return tmp[2]
    } else {
        return false
    }
}

export function getSecond(prop, array) {
    /*if (prop === 'average_kills_per_death') {
        console.log({msg: 'second', prop, array})
    }*/
    let tmp = [...new Set(array.map(e => {
        return e[prop]
    }))]
    tmp.sort((a, b) => {
        return b - a
    })
    if (typeof tmp[1] != 'undefined') {
        return tmp[1]
    } else {
        return false
    }
}

export function getThird(prop, array) {
    let tmp = [...new Set(array.map(e => {
        return e[prop]
    }))]
    tmp.sort((a, b) => {
        return b - a
    })
    if (typeof tmp[2] != 'undefined') {
        return tmp[2]
    } else {
        return false
    }
}
