export function getClassName(className) {
    const map = new Map(
        [
            ['scout', 'Scout'],
            ['soldier', 'Soldier'],
            ['pyro', 'Pyro'],
            ['demoman', 'Demoman'],
            ['heavy', 'Heavy'],
            ['heavyweapons', 'Heavy'],
            ['engineer', 'Engineer'],
            ['medic', 'Medic'],
            ['sniper', 'Sniper'],
            ['spy', 'Spy'],
        ]
    )
    return map.get(className);
}